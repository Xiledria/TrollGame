#ifndef STANDALONE_SERVER

#include "GraphicsInitialize.h"
#include "VKGraphics.h"
#include "GLGraphics.h"
#include "ImmediateGLGraphics.h"
#include "Config.h"

Graphics* GraphicsInitialize::CreateGraphicsContext()
{
    GraphicsInitializeErrors error;
    graphics = CreateGraphicsContextInternal(error);
    if (graphics)
        error = graphics->InitializeFonts();

    if (error != GraphicsInitializeErrors::NONE)
    {
        switch (error)
        {
        case GraphicsInitializeErrors::FAILED_SDL_INIT:
            SDL_ShowSimpleMessageBox(0, PROJECT_NAME, "Failed to initialize SDL Video!", nullptr);
            break;
        case GraphicsInitializeErrors::UNSUPPORTED_GL_VERSION:
            SDL_ShowSimpleMessageBox(0, PROJECT_NAME, "Your graphic card does not support minimal OpenGL Version 2.1!", nullptr);
            break;
        case GraphicsInitializeErrors::FAILED_LOAD_FONT_LIB:
            SDL_ShowSimpleMessageBox(0, PROJECT_NAME, "Failed to initialize fonts library!", nullptr);
            break;
        case GraphicsInitializeErrors::FAILED_LOAD_FONT:
            SDL_ShowSimpleMessageBox(0, PROJECT_NAME, "Failed to open font.ttf!", nullptr);
            break;
        default:
            break;
        }
        delete graphics; // it is legal to delete nullptr & if graphics init is successfull & fonts failed, we have to delete it
        return nullptr;
    }
    return graphics;
}

//@TODO: add VK renderer detection after VK renderer is workings
// make it working with SDL so we do not need to use GLFW & SDL (or maybe move to GLFW from SDL?)
Graphics* GraphicsInitialize::CreateGraphicsContextInternal(GraphicsInitializeErrors& error)
{
    error = GraphicsInitializeErrors::NONE;
#ifdef BUILD_VULKAN_GRAPHICS
    bool canUseVKGraphics = false;
    glfwInit();
    if (glfwVulkanSupported())
    {
        // @TODO: check for content & window creation first
        // canUseVKGraphics = true;
    }

    glfwTerminate();
    if (canUseVKGraphics)
        return new VKGraphics(game);
#endif
    // failed to create video
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        error = GraphicsInitializeErrors::FAILED_SDL_INIT;
        return nullptr;
    }

    SDL_Window* window = SDL_CreateWindow(nullptr, 0, 0,
        0, 0, SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN);
    if (!window) /* Die if creation failed */
    {
        error = GraphicsInitializeErrors::FAILED_SDL_INIT;
        SDL_Quit();
        return nullptr;
    }
    SDL_GLContext context = SDL_GL_CreateContext(window);
    const GLubyte* version = glGetString(GL_VERSION);
    GLubyte version_main = version[0];
    GLubyte version_minor = version[2];
    DEBUG(std::cout << "Supported OpenGL Version: " << version_main << "." << version_minor << std::endl);
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    if (Config::IsEnabled(Config::opengl_4_3) && (version_main > '4' || (version_main == '4' && version_minor >= '3')))
        return new GLGraphics();

    if (version_main > '2' || (version_main == '2' && version_minor >= '1'))
        return new ImmediateGLGraphics();

    error = GraphicsInitializeErrors::UNSUPPORTED_GL_VERSION;
    return nullptr;
}

#endif
