#ifndef STANDALONE_SERVER
#ifndef __IMMEDIATE_GL_GRAPHICS_H
#define __IMMEDIATE_GL_GRAPHICS_H

#include "SharedDefs.h"
#include "Graphics.h"
#include <GL/glew.h>
#include SDL_INCLUDE

#define GLERROR() DEBUG({GLenum err = glGetError(); if (err != GL_NO_ERROR) std::cout << "GL ERROR: " << err << " " << __LINE__ << std::endl;})
class Fonts;
class UIObject;

class ImmediateGLGraphics : public Graphics
{
public:
    ImmediateGLGraphics();
    ~ImmediateGLGraphics() override;
    void Run() override;
    void UpdateCamera() override;
    void Resize() override;
    void HandleWorldResize() override;
    void RecreateMinimap() override;
    GraphicsInitializeErrors InitializeFonts() override;
    void UpdateVSync() override;
    void UpdateFullscreen() override;
    void DrawUIObject(UIObject* object) override;
    void DrawMinimap(UIObject* object) override;
private:
    void DrawCircle(int32 posx, int32 posy, int32 radius, const float* color4f) override;
    void DrawDisc(int32 posx, int32 posy, int32 radius, const float* color4f) override;
    void DrawTile(uint32 x, uint32 y, const float* color3f) override;
    void DrawSquare(int32 x, int32 y, int32 radius, const float* color4f) override;
    void DrawUnitHealth(int32 x, int32 y, int32 radius, float pctHp) override;
    void DrawUnitSelection(int32 x, int32 y, int32 radius) override;
    void DrawActualSelecting() override;
    void DrawTeamColor(Unit* unit, Vec2<int32>& offset) override;
    void DrawUI() override;
    void DrawWorld(uint32 startX, uint32 endX, uint32 startY, uint32 endY, UIObject*) override;
    void DrawEditor() override;
    void SDLDie(char* msg);
    SDL_Window* window;
    SDL_GLContext context;
    GLuint mapTexture;
    Fonts* fonts;
};

#endif
#endif
