#ifndef STANDALONE_SERVER
#ifndef __CONTROLLS_H
#define __CONTROLLS_H

#include "SharedDefs.h"
#include <vector>

union SDL_Event;
typedef int32 Sint32;
typedef Sint32 SDL_Keycode;
class Position;
template<typename TYPE>
class Vec2;
class UIObject;
class LevelMenu;
class MainMenu;
class NewWorldMenu;
class EditBox;
class ConfigMenu;

class Controlls
{
public:
    Controlls();
    ~Controlls();
    bool Update(float diff);
    Position* GetSelectionArea();
    void Quit(bool isQuitting = true); // terminates progam
    void SetClickStartedObject(UIObject* obj);

    void HandleWorldClick(UIObject* obj);
    void HandleWorldDrag(UIObject* obj);
    void HandleSendSelected(UIObject* obj);
private:
    enum ClickLocation
    {
        LOCATION_WORLD,
        LOCATION_UI,
        LOCATION_MINIMAP,
    };

    Vec2<int32> GetWorldPosition(Vec2<int32> pos);
    ClickLocation GetMouseLocation(uint32 x, uint32 y);
    void HandleBuildTerrain(uint32 x, uint32 y);
    void SetAreaSelectionBegin(uint32 x, uint32 y);
    void SetAreaSelectionEnd(uint32 x, uint32 y);
    void HandleSelectArea();
    void HandleMenuBuilderClick(uint32 x, uint32 y);
    void HandleMouseClick(SDL_Event& e, MouseType button);
    //void HandleMouseRightClick(SDL_Event& e);
    void HandleMouseDrag(SDL_Event& e, MouseType button);
    bool HandleKeyboardEvent(SDL_Keycode& e);

    enum Selection
    {
        SELECTION_BEGIN,
        SELECTION_END,
        SELECTION_OFFSET,
        SELECTION_ARRAY_SIZE,
    };
    Position* selection;
    uint32 ScrollSpeed;
    bool quit;
    bool isSelecting;
    UIObject* clickStartObj;
};

extern Controlls* controlls;

#endif
#endif
