#include "MoveMaps.h"
#include "World.h"
#include "Object.h"
#include "Terrain.h"

MoveMaps::MoveMaps(MoveFlags flags, Object* owner) : _terrain(world->GetTerrain()), _objects(world->GetObjects()),
_maxX(world->GetGridMaxX()), _maxY(world->GetGridMaxY()), _moveFlags(flags), _owner(owner)
{
    int32 size = std::max(1, static_cast<int32>(2 * owner->GetSize() / world->GetGridSize()));
    _sizeplus = size / 2 + size % 2;
    _sizeminus = size / 2;
}

MoveMaps::~MoveMaps()
{

}

bool MoveMaps::CheckTile(unsigned xitr, unsigned yitr) const
{
    if ((_moveFlags & _terrain[xitr + _maxX * yitr]->GetMoveFlags()) == 0)
        return false;

    if (Object* obj = _objects[xitr + _maxX * yitr])
        if (obj != _owner && !obj->IsMoving())
            return false;
    return true;
}

bool MoveMaps::operator()(unsigned x, unsigned y) const
{
    if (x + _sizeplus - 1 >= _maxX || y + _sizeplus - 1 >= _maxY)
        return false;

    if (x < _sizeminus || y < _sizeminus)
        return false;

    for (uint32 xitr = x - _sizeminus; xitr < _sizeplus + x; ++xitr)
        for (uint32 yitr = y - _sizeminus; yitr < _sizeplus + y; ++yitr)
            if (!CheckTile(xitr, yitr))
                return false;
    return true;
}

MoveMapsFollow::MoveMapsFollow(MoveFlags flags, Object* owner, Position* target) : MoveMaps(flags, owner), _target(target)
{
}

bool MoveMapsFollow::CheckTile(unsigned xitr, unsigned yitr) const
{
    if ((_moveFlags & _terrain[xitr + _maxX * yitr]->GetMoveFlags()) == 0)
        return false;

    if (Object* obj = _objects[xitr + _maxX * yitr])
    {
        if (obj != _owner && obj != _target && !obj->IsMoving())
            return false;
    }
    return true;
}

MoveMapsIgnoringMobs::MoveMapsIgnoringMobs(MoveFlags flags, Object* owner) : MoveMaps(flags, owner)
{
}

bool MoveMapsIgnoringMobs::CheckTile(unsigned xitr, unsigned yitr) const
{
    return (_moveFlags & _terrain[xitr + _maxX * yitr]->GetMoveFlags()) != 0;
}
