#ifndef STANDALONE_SERVER
#ifndef __GRAPHICS_H
#define __GRAPHICS_H

#include "SharedDefs.h"
#include "Vec2.h"
#include <chrono>

class Unit;
class UIObject;

class Graphics
{
public:
    Graphics();
    virtual ~Graphics();
    virtual void Run();
    Vec2<uint32>& GetResolution();
    uint32 GetScreenX();
    uint32 GetScreenY();
    uint32 GetMinimapSize();
    uint32 GetUIHeight();
    Vec2<int32>* GetCamera();
    virtual void UpdateCamera();
    virtual void Resize();
    virtual void HandleWorldResize();
    virtual void RecreateMinimap();
    virtual GraphicsInitializeErrors InitializeFonts();
    virtual void UpdateVSync();
    virtual void UpdateFullscreen();
    virtual void DrawUIObject(UIObject* obj);
    virtual void DrawMinimap(UIObject* object);
    void DrawWorld(UIObject* object);
protected:
    char* FileToBuffer(char *file);
    virtual void DrawCircle(int32 posx, int32 posy, int32 radius, const float* color4f);
    virtual void DrawDisc(int32 posx, int32 posy, int32 radius, const float* color4f);
    virtual void DrawTile(uint32 x, uint32 y, const float* color3f);
    virtual void DrawSquare(int32 x, int32 y, int32 radius, const float* color4f);
    virtual void DrawUnitHealth(int32 x, int32 y, int32 radius, float pctHp);
    virtual void DrawUnitSelection(int32 x, int32 y, int32 radius);
    virtual void DrawActualSelecting();
    virtual void DrawTeamColor(Unit* unit, Vec2<int32>& offset);
    virtual void DrawVisibleObjects(UIObject* object);
    virtual void DrawWorld(uint32 startX, uint32 endX, uint32 startY, uint32 endY, UIObject* object);
    virtual void DrawEditor();
    virtual void DrawUI();
    std::chrono::high_resolution_clock::time_point lastUpdateTime;
    Vec2<uint32> resolution;
    uint32 uiHeight;
    uint32 minimapSize;
};

extern Graphics* graphics;
extern Vec2<int32>*camera;

#endif
#endif
