#ifndef STANDALONE_SERVER
#ifndef __FUNCTION_PTR_H
#define __FUNCTION_PTR_H

#include "SharedDefs.h"
#include <functional>

class UIObject;

struct FunctionPtr
{
    virtual ~FunctionPtr() { }
    virtual void Execute() { }
};

struct NoParamFunction : FunctionPtr
{
    NoParamFunction(std::function<void()> fn);
    ~NoParamFunction() { }
    void Execute() override;
private:
    std::function<void()> _fn;
};

struct OneParamFunction : FunctionPtr
{
    OneParamFunction(std::function<void(int32)> fn, int32 param1);
    ~OneParamFunction() { }
    void Execute() override;
private:
    std::function<void(int32)> _fn;
    int32 _param1;
};

struct MemberNoParamFunction : FunctionPtr
{
    MemberNoParamFunction(std::function<void(UIObject*)> fn, UIObject* obj);
    ~MemberNoParamFunction() { }
    void Execute() override;
private:
    std::function<void(UIObject*)> _fn;
    UIObject* _obj;
};

struct MemberOneParamFunction : FunctionPtr
{
    MemberOneParamFunction(std::function<void(UIObject*, int32)> fn, UIObject* obj, int32 param1);
    ~MemberOneParamFunction() { }
    void Execute() override;
private:
    std::function<void(UIObject*, int32)> _fn;
    UIObject* _obj;
    int32 _param1;
};

struct MemberTwoParamFunction : FunctionPtr
{
    MemberTwoParamFunction(std::function<void(UIObject*, int32, std::string)> fn, UIObject* obj, int32 param1, std::string param2);
    ~MemberTwoParamFunction() { }
    void Execute() override;
private:
    std::function<void(UIObject*, int32, std::string)> _fn;
    UIObject* _obj;
    int32 _param1;
    std::string _param2;
};

struct MemberThreeParamFunction : FunctionPtr
{
    MemberThreeParamFunction(std::function<void(UIObject*, int32, std::string, std::string)> fn, UIObject* obj, int32 param1, std::string param2, std::string param3);
    ~MemberThreeParamFunction() { }
    void Execute() override;
private:
    std::function<void(UIObject*, int32, std::string, std::string)> _fn;
    UIObject* _obj;
    int32 _param1;
    std::string _param2;
    std::string _param3;
};

#endif
#endif
