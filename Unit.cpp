#include "Unit.h"
#include "ObjectStore.h"
#include "World.h"
#include "Pathfinder.h"
#include "Terrain.h"
#include "Missile.h"
#include "Searcher.h"

//@TODO: optimize me somehow, lol, this is realy REALY bad :D, maybe pass ref and init "this" from it instead of this bad initialisaation
Unit::Unit(uint32 entry)
{
    Unit* unit = objectStore->GetUnit(entry);
    *this = *unit;
    _objectId = world->GenerateObjectId();
    _moving = false;
    _pathfinder = new Pathfinder(this);
    if (_maximalAttack < _minimalAttack)
        std::swap(_maximalAttack, _minimalAttack);
}

//@TODO: initialiser list
Unit::Unit()
{
    _minimalAttack = 0;
    _maximalAttack = 0;
    _attackSpeed = 0.0f;
    _attackRange = 0;
    _searchRadius = 0;
    _attackTimer = 0.0f;
    _healthRegen = 0.0f;
    _maxHealth = 0.0f;
    _health = 1.0f;
    _armor = 0;
    _moveSpeed = 0;
    _ownerId = 0;
    _missileModelId = 0;
    _missileSpeed = 0;
    _pathfinder = nullptr;
    _moveFlags = MOVEFLAG_NONE;
    _type = TYPEID_UNIT;
    _model = nullptr;
    _moving = false;
    _alive = true;
    _target = nullptr;
}

Unit::~Unit()
{
    delete _pathfinder;
}

float Unit::GetHealth()
{
    return _health;
}

void Unit::SetHealth(float health)
{
    if (_maxHealth > health)
        this->_health = _maxHealth;
    else
        this->_health = health;
}

float Unit::GetMaxHealth()
{
    return _maxHealth;
}

float Unit::GetHealthPct()
{
    return _health / _maxHealth;
}

uint8 Unit::GetOwnerId()
{
    return _ownerId;
}

void Unit::SetOwnerId(uint8 id)
{
    _ownerId = id;
}

void Unit::MoveTo(int32 X, int32 Y)
{
    _pathfinder->SetMoveDest(X, Y);
}

void Unit::MoveTo(Position* pos)
{
    _pathfinder->SetMoveDest(pos);
}

void Unit::MoveTo(Position& pos)
{
    _pathfinder->SetMoveDest(pos.GetX(), pos.GetY());
}

bool Unit::GetMoveDest(Position& pos)
{
    return _pathfinder->GetMoveDest(pos);
}

bool Unit::IsMoving()
{
    return _moving;
}

bool Unit::IsAlive()
{
    return _alive;
}

MoveFlags Unit::GetMoveFlags()
{
    return _moveFlags;
}

void Unit::HandleDamageTaken(int32 damage)
{
    if (!_alive) // do not allow to die twice
        return;
    _health -= damage;
    if (_health <= 0)
    {
        world->RemoveObject(this);
        _alive = false;
    }
}

void Unit::UpdatePathfinder(float diff)
{
    _pathfinder->Update(diff);
}

void Unit::UpdateMovement(float diff)
{
    _moving = false;
    Position next;
    // get move destination
    bool hasNext = GetMoveDest(next);
    if (!hasNext) // no destination, return
        return;
    // check if we have reached our final destination
    if (GetX() == next.GetX() && GetY() == next.GetY())
    {
        _pathfinder->SetMoveDest(nullptr);
        _pathfinder->SetCanMove(false);
        return;
    }
    // get next waypoint to move on
    hasNext = _pathfinder->GetNextPath(next);
    if (!hasNext)
        return;

    if (GetX() == next.GetX() && GetY() == next.GetY())
    {
        _pathfinder->OnWaypointReached();
        hasNext = _pathfinder->GetNextPath(next);
        if (!hasNext)
            return;
    }

    int32 distDestX, distDestY;
    int32 distance = GetDistance(&next, distDestX, distDestY);
    bool negativeX = distDestX < 0;
    bool negativeY = distDestY < 0;
    int32 directionX = negativeX ? -1 : 1;
    int32 directionY = negativeY ? -1 : 1;
    const float speedDiff = static_cast<float>(_moveSpeed) * diff;
    int32 walkableDist[MOVE_MAX_LINES] = { distance, distance };
    std::vector<Object*> objects;
    world->GetObjectsInArea(this, GetSize(), objects);
    for (auto obj : objects)
    {
        if (obj == this)
            continue;

        int32 distX, distY;
        GetDistance(obj, distX, distY);
        int32 size = (obj->GetSize() + this->GetSize());
        int32 diffX = distDestX ? static_cast<int32>(speedDiff * directionX) : 0;
        int32 diffY = distDestY ? static_cast<int32>(speedDiff * directionY) : 0;
        int32 futureDistX = std::abs(distX - diffX);
        int32 futureDistY = std::abs(distY - diffY);
        distX = std::abs(distX);
        distY = std::abs(distY);
        if (distX < size && futureDistY < distY)
            walkableDist[MOVE_VERTICAL] = std::min(walkableDist[MOVE_VERTICAL], std::max(distY - size, 0));

        if (distY < size && futureDistX < distX)
            walkableDist[MOVE_HORIZONTAL] = std::min(walkableDist[MOVE_HORIZONTAL], std::max(distX - size, 0));
    }

    // @TODO: create functions for these kind of checks it is kind of duplicated here
    if (walkableDist[MOVE_HORIZONTAL] != 0)
    {
        uint32 indexX = world->CalculateGridIndex(GetX() + static_cast<int32>((speedDiff + GetSize())) * directionX);
        int32 gridX = indexX * world->GetGridSize();
        uint32 indexYMin = world->CalculateGridIndex(GetY() - GetSize());
        uint32 indexYMax = world->CalculateGridIndex(GetY() + GetSize() - 1); // 1 px correction

        if (indexYMin != indexYMax)
        {
            if (!world->IsTerrainWalkableBy(indexX, indexYMin, _moveFlags))
                walkableDist[MOVE_HORIZONTAL] = GetX() < gridX ? std::min(walkableDist[MOVE_HORIZONTAL], std::abs(gridX - GetX() - GetSize())) : 0;
        }

        if (!world->IsTerrainWalkableBy(indexX, indexYMax, _moveFlags))
            walkableDist[MOVE_HORIZONTAL] = GetX() < gridX ? std::min(walkableDist[MOVE_HORIZONTAL], std::abs(gridX - GetX() - GetSize())) : 0;
    }

    if (walkableDist[MOVE_VERTICAL] != 0)
    {
        uint32 indexXMin = world->CalculateGridIndex(GetX() - GetSize());
        uint32 indexXMax = world->CalculateGridIndex(GetX() + GetSize() - 1); // 1 px correction
        uint32 indexY = world->CalculateGridIndex(GetY() + static_cast<int32>((speedDiff + GetSize()) * directionY));
        int32 gridY = indexY * world->GetGridSize();

        if (indexXMin != indexXMax)
        {
            if (!world->IsTerrainWalkableBy(indexXMin, indexY, _moveFlags))
                walkableDist[MOVE_VERTICAL] = GetY() < gridY ? std::min(walkableDist[MOVE_VERTICAL], std::abs(gridY - GetY() - GetSize())) : 0;
        }

        if (!world->IsTerrainWalkableBy(indexXMax, indexY, _moveFlags))
            walkableDist[MOVE_VERTICAL] = GetY() < gridY ? std::min(walkableDist[MOVE_VERTICAL], std::abs(gridY - GetY() - GetSize())) : 0;
    }

    // limits
    walkableDist[MOVE_HORIZONTAL] = std::min(walkableDist[MOVE_HORIZONTAL], std::abs(distDestX));
    walkableDist[MOVE_VERTICAL] = std::min(walkableDist[MOVE_VERTICAL], std::abs(distDestY));

    distance = Position().GetDistance(walkableDist[MOVE_HORIZONTAL], walkableDist[MOVE_VERTICAL]);
    if (!distance)
    {
        _pathfinder->SetCanMove(false);
        return;
    }

    int32 diffX = (negativeX ? -1 : 1) * std::min(std::min(static_cast<int32>(walkableDist[MOVE_HORIZONTAL] * speedDiff / distance), walkableDist[MOVE_HORIZONTAL]), std::abs(distDestX));
    int32 diffY = (negativeY ? -1 : 1) * std::min(std::min(static_cast<int32>(walkableDist[MOVE_VERTICAL] * speedDiff / distance), walkableDist[MOVE_VERTICAL]), std::abs(distDestY));
    _movementOffset.SetCoords(diffX, diffY);
}

void Unit::UpdateAttack(float diff)
{
    if (_attackTimer > 0)
    {
        _attackTimer -= diff;
        return;
    }

    std::vector<Object*> hostile;
    auto searcher = HostileUnitSearcher(this, _attackRange);
    world->GetObjectsInArea(hostile, searcher);
    if (hostile.empty())
    {
        // check again after 200ms
        _attackTimer = frand(0.15f, 0.25f);
        if (!IsMoving())
        {
            auto searcher = HostileUnitSearcher(this, _searchRadius);
            world->GetObjectsInArea(hostile, searcher);
            if (hostile.empty())
                return;
            MoveTo(static_cast<Unit*>(GetRandomItem<Object*>(hostile)));
        }
        return;
    }

    _target = static_cast<Unit*>(GetRandomItem<Object*>(hostile));
    _attackTimer += _attackSpeed;
    if (!IsMoving())
        MoveTo(_target);
}

void Unit::AttackTarget(Unit* target)
{
    int32 damage = _minimalAttack == _maximalAttack ? _minimalAttack : irand(_minimalAttack, _maximalAttack);

    Missile* missile = new Missile(_missileSpeed, damage, _missileModelId);
    missile->SetTarget(target, true);
    missile->SetCoords(*this);
}

void Unit::UpdateParallel(float diff)
{
    float regenHp = _healthRegen * diff;
    if (_health + regenHp < _maxHealth)
        _health += regenHp;
    else _health = _maxHealth;

    UpdateAttack(diff);

    UpdateMovement(diff);
}


void Unit::Update(float /*diff*/)
{
    //@TODO: make function like FinalizeMovement
    if (_movementOffset.GetX() || _movementOffset.GetY())
    {
        _moving = world->RelocateObject(this, GetX() + _movementOffset.GetX(), GetY() + _movementOffset.GetY());
        _pathfinder->SetCanMove(_moving);
        _movementOffset.SetCoords(0, 0);
    }
    else
        _pathfinder->SetCanMove(false);

    //@TODO: make function like FinalizeAttack
    if (_target)
    {
        AttackTarget(_target);
        _target = nullptr;
    }
}
