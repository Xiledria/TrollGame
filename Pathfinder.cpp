#include "Pathfinder.h"
#include "MoveMaps.h"
#include "World.h"
#include "Unit.h"
#include "Terrain.h"
#include "JPS.h"

// @TODO: initialiser list
Pathfinder::Pathfinder(Unit* owner)
{
    _owner = owner;
    _moveDest = nullptr;
    Reset();
    _canMove = false;
    _isReferenceDest = false;
}

Pathfinder::~Pathfinder()
{
    for (auto itr : _waypoints)
        delete itr;
    _waypoints.clear();
    _waypoints.swap(_waypoints);

    if (!_isReferenceDest)
        delete _moveDest;
}

void Pathfinder::Reset()
{
    _updateTimer = 0.2f;
    _canMove = true;
    _cannotMoveTimer = 0.0f;
}

void Pathfinder::SetMoveDest(Position* dest)
{
    if (!dest)
    {
        std::lock_guard<std::mutex> lock(_destLock);
        if (!_isReferenceDest)
            delete _moveDest;
        _moveDest = nullptr;
        return;
    }

    if (_owner != dest)
    {
        std::lock_guard<std::mutex> lock(_destLock);
        if (!_isReferenceDest)
            delete _moveDest;

        _moveDest = dest;
        _isReferenceDest = true;
    }
    else
    {
        // do not stop between grids
        SetMoveDest(_owner->GetX(), _owner->GetY());
    }
    Reset();
}

void Pathfinder::SetMoveDest(int32 x, int32 y)
{
    int32 maxX = static_cast<int32>(world->GetMaxX());
    int32 maxY = static_cast<int32>(world->GetMaxY());
    int32 size = _owner->GetSize();
    if (x + size >= maxX)
        x = maxX - size;
    else if (x <= 0.0f + size)
        x = size;

    if (y + size >= maxY)
        y = maxY - size;
    else if (y <= 0.0f + size)
        y = size;

    x = world->NormalizeCoord(x);
    y = world->NormalizeCoord(y);
    
    if (_owner->GetX() == x && _owner->GetY() == y)
        return SetMoveDest(nullptr);

    x += size;
    y += size;

    _destLock.lock();
    if (_isReferenceDest || !_moveDest)
        _moveDest = new Position(x, y);
    else
        _moveDest->SetCoords(x, y);
    _isReferenceDest = false;
    _destLock.unlock();
    Reset();
}

bool Pathfinder::GetMoveDest(Position& pos)
{
    std::lock_guard<std::mutex> lock(_destLock);
    if (!_moveDest)
        return false;

    pos = *_moveDest;
    return true;
}

void Pathfinder::GeneratePath()
{
    _destLock.lock();
    if (!_moveDest)
    {
        _destLock.unlock();
        return;
    }
    // copy moveDest to allow unlocking mutex immediatedly
    Position dest = Position(*_moveDest);
    // copy isReferenceDest bool variable
    bool isRef = _isReferenceDest;
    _destLock.unlock();
    MoveFlags movementType = _owner->GetMoveFlags();
    int32 gridSize = world->GetGridSize();
    uint32 startX = world->CalculateGridIndex(_owner->GetX());
    uint32 startY = world->CalculateGridIndex(_owner->GetY());
    uint32 endX = world->CalculateGridIndex(dest.GetX());
    uint32 endY = world->CalculateGridIndex(dest.GetY());
    JPS::PathVector path;
    path.reserve(10);
    bool found;

    if (isRef)
        found = JPS::findPath(path, MoveMapsFollow(movementType, _owner, &dest), startX, startY, endX, endY);
    else
        found = JPS::findPath(path, MoveMaps(movementType, _owner), startX, startY, endX, endY);
    
    // @TODO: change me, remove me or something
    if (!found)
        found = JPS::findPath(path, MoveMapsIgnoringMobs(movementType, _owner), startX, startY, endX, endY);

    if (!found)
    {
       DEBUG(std::cout << "no path found!" << std::endl);
       if (_canMove || isRef) // probably collided with another unit, try to find another path, do not stop moving (move stop is handled elsewhere for this case)
           _owner->MoveTo(*_owner);
        return;
    }
    int32 size = _owner->GetSize() % world->GetGridSize();
    std::vector<Position*> newWaypoints;
    newWaypoints.reserve(3);
    int32 pathCount = std::min(2, static_cast<int32>(path.size() - 1));
    if (!path.empty())
    {
        for (auto i = 0; i < pathCount; ++i)
        {
            JPS::Position& itr = path[i];
            newWaypoints.push_back(new Position(itr.x * gridSize + size, itr.y * gridSize + size));
        }
    }

    newWaypoints.push_back(new Position(dest));
    _wpLock.lock();
    std::swap(_waypoints, newWaypoints);
    _wpLock.unlock();
    // now newWaypoints are old ones, so delete them
    if (!newWaypoints.empty())
    {
        for (auto itr : newWaypoints)
            delete itr;
        newWaypoints.clear();
    }
    path.clear();
    path.swap(path);
}

void Pathfinder::OnWaypointReached()
{
    std::lock_guard<std::mutex> lock(_wpLock);
    if (!_waypoints.empty())
    {
        // @TODO: check for better container for delete from front
        delete *_waypoints.begin();
        _waypoints.erase(_waypoints.begin());
    }
}

bool Pathfinder::GetNextPath(Position& pos)
{
    std::lock_guard<std::mutex> lock(_wpLock);
    if (_waypoints.empty())
        return false;

    // copy position
    pos = *_waypoints[0];
    return true;
}

void Pathfinder::SetCanMove(bool canMove)
{
    if (canMove)
        _cannotMoveTimer = 0.0f;
    _canMove = canMove;
}

void Pathfinder::Update(float diff)
{
    if (_updateTimer >= 0.3f)
    {
        GeneratePath();
        _updateTimer = frand(0.05f, 0.15f);
    }
    else
        _updateTimer += diff;

    if (!_isReferenceDest)
    {
        if (!_canMove)
        {
            _cannotMoveTimer += diff;
            if (_cannotMoveTimer > 2.0f)
            {
                _owner->MoveTo(*_owner);
                _cannotMoveTimer = 0.0f;
            }
        }
        else _cannotMoveTimer = 0.0f;
    }
}
