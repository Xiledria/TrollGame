#ifndef STANDALONE_SERVER
#include "GraphicsInitialize.h"
#include "UIObject.h"
#include "UIFunctions.h"
#include "World.h"
#include "Graphics.h"
#include "Controlls.h"
#include "UIHandler.h"
#include "Player.h"
#include "Config.h"
#include <filesystem>

void Unpause()
{
    Config::Clear(Config::paused);
    ui->MoveToMenu(UIHandler::INGAME_MENU);
}

// @TODO: remove me
extern void GetFilesInDirectory(std::vector<std::string> &out, const std::string &directory);

UIObject::UIObject(Vec2<int32> pos, Vec2<float> pctPos, Vec2<int32> size, float color3f[3], std::string newText, Vec2<float> pctSize)
{
    Init(pos, pctPos, size, pctSize, color3f, newText);
}

void UIObject::Init(Vec2<int32> pos, Vec2<float> pctPos, Vec2<int32> size, Vec2<float> pctSize, float color3f[3], std::string newText)
{
    // @TODO: finish size things
    this->pos = pos;
    this->pctPos = pctPos;
    this->size = size;
    this->pctSize = pctSize;
    this->clickStarted = false;
    for (auto i = 0; i < 3; ++i)
        m_color3f[i] = color3f[i];
    text = newText;
    parent = nullptr;
    visible = true;
    OnWindowResized();
    onDrawFn = new MemberNoParamFunction(UIFunctions::GetMemberNoParamFnByName("DefaultRender"), this);
    for (uint8 i = 0; i < TOTAL_BUTTONS; ++i)
        onDragFn[i] = new FunctionPtr();
    for (uint8 i = 0; i < TOTAL_BUTTONS; ++i)
        onClickFn[i] = new FunctionPtr();
    onVisibilityCheckFn = new FunctionPtr();
}

UIObject::~UIObject()
{
    for (auto child : childs)
        delete child;

    for (auto child : nonClickableChilds)
        delete child;
}

void UIObject::ClearChilds()
{
    for (auto child : childs)
        delete child;
    childs.clear();

    for (auto child : nonClickableChilds)
        delete child;
    nonClickableChilds.clear();
}

void UIObject::SetOnDrawFn(FunctionPtr* fn)
{
    delete onDrawFn;
    onDrawFn = fn;
}

void UIObject::SetOnDragFn(FunctionPtr* fn, MouseType button)
{
    delete onDragFn[button];
    onDragFn[button] = fn;
}

void UIObject::SetOnClickFn(FunctionPtr* fn, MouseType button)
{
    delete onClickFn[button];
    onClickFn[button] = fn;
}

void UIObject::SetOnVisibilityCheckFn(FunctionPtr* fn)
{
    delete onVisibilityCheckFn;
    onVisibilityCheckFn = fn;
}

void UIObject::SetText(std::string& newText)
{
    text = newText;
}

void UIObject::SetVisible(bool vis)
{
    visible = vis;
}

bool UIObject::IsVisible()
{
    return visible;
}

void UIObject::OnWindowResized()
{
    currentSize = size + (pctSize * graphics->GetResolution());
    center = graphics->GetResolution() * pctPos + pos;

    start.SetCoords(center.GetX() - currentSize.GetX() / 2, center.GetY() - currentSize.GetY() / 2);
    end.SetCoords(center.GetX() + currentSize.GetX() / 2, center.GetY() + currentSize.GetY() / 2);
    for (auto child : childs)
        child->OnPosChanged();
    
    for (auto child : nonClickableChilds)
        child->OnPosChanged();
}

void UIObject::OnPosChanged() // called from parent to all childs
{
    currentSize = size + (pctSize * parent->GetSize());
    center = parent->start + parent->currentSize * pctPos + pos;
    start = center - (currentSize * 0.5f);
    end = center + (currentSize * 0.5f);

    for (auto child : childs)
        child->OnPosChanged();

    for (auto child : nonClickableChilds)
        child->OnPosChanged();
}

void UIObject::OnDrag(Vec2<int32>& pos, MouseType button)
{
    if (!clickStarted)
        return;

    clickPos = pos;
    clickType = button;
    onDragFn[clickType]->Execute();
}


void UIObject::OnClick(Vec2<int32>& pos, MouseType button)
{
    if (!clickStarted)
        return;

    clickPos = pos;
    clickType = button;
    onClickFn[clickType]->Execute();
}

void UIObject::SetClickStarted(bool started)
{
    clickStarted = started;
}

void UIObject::OnVisibilityCheck()
{
    onVisibilityCheckFn->Execute();
    for (UIObject* child : GetChilds(true))
        child->OnVisibilityCheck();

    for (UIObject* child : GetChilds(false))
        child->OnVisibilityCheck();

    OnWindowResized();
}

void UIObject::Draw()
{
    onDrawFn->Execute();
    for (UIObject* child : GetChilds(true))
        child->Draw();

    for (UIObject* child : GetChilds(false))
        child->Draw();
}

UIObject* UIObject::GetTopObjectAtPos(Vec2<int32>& pos)
{
    if (!IsInside(pos))
        return nullptr;
    UIObject* child = this;
    for (auto i : childs)
        if (i->IsVisible())
            if (UIObject* obj = i->GetTopObjectAtPos(pos))
                child = obj;
    return child;
}

bool UIObject::IsInside(Vec2<int32>& pos)
{
    return end >= pos && pos >= start;
}

void UIObject::HandleDrag(Vec2<int32>& pos, MouseType button)
{
    if (UIObject* obj = GetTopObjectAtPos(pos))
        obj->OnDrag(pos, button);
}

void UIObject::HandleClick(Vec2<int32>& pos, MouseType button)
{
    if (UIObject* obj = GetTopObjectAtPos(pos))
        obj->OnClick(pos, button);
}

// for sorting vector
bool UIObject::operator<(UIObject& obj)
{
    if (start.GetX() < obj.start.GetX())
        return true;
    
    if (start.GetX() == obj.start.GetX())
    {
        if (end.GetX() < obj.end.GetX())
            return true;

        if (end.GetX() == obj.end.GetX())
        {
            if (start.GetY() < obj.start.GetY())
                return true;
        }
    }
    return end.GetY() < obj.end.GetY();
}

Vec2<int32>& UIObject::GetStart()
{
    return start;
}

Vec2<int32>& UIObject::GetSize()
{
    return currentSize;
}

Vec2<int32>& UIObject::GetCenter()
{
    return center;
}

Vec2<int32> UIObject::GetClickPos()
{
    return clickPos;
}

Vec2<int32> UIObject::GetRelativeClickPos()
{
    return clickPos - start;
}

float* UIObject::GetColor3f()
{
    return m_color3f;
}

std::string UIObject::GetText()
{
    return text;
}

void UIObject::SetParent(UIObject* obj)
{
    parent = obj;
    OnPosChanged();
}

void UIObject::AddChild(UIObject* obj, bool clickable /* = true */)
{
    if (clickable)
        childs.push_back(obj);
    else
        nonClickableChilds.push_back(obj);

    obj->SetParent(this);
}

std::vector<UIObject*>& UIObject::GetChilds(bool clickable /* = true */)
{
    return clickable ? childs : nonClickableChilds;
}

UIObject* UIObject::GetParent()
{
    return parent;
}

EditBox::EditBox(Vec2<int32> pos, Vec2<float> pctPos, Vec2<int32> size, float color3f[], float activeColor3f[], std::string newText, uint8 editMode, Vec2<float> pctSize)
:UIObject(pos, pctPos, size, color3f, newText, pctSize)
{
    Init(activeColor3f, editMode);
}


EditBox::~EditBox()
{
    if (active)
        ui->SetFocusedUIObject(nullptr);
}

void EditBox::Init(float activeColor3f[], uint8 editMode)
{
    SetActiveColor(activeColor3f);
    active = false;
    this->editMode = editMode;
    onChangeFn = new FunctionPtr();
}

void EditBox::SetOnChangeFunction(FunctionPtr* function)
{
    delete onChangeFn;
    onChangeFn = function;
}

void EditBox::OnClick(Vec2<int32>& pos, MouseType button)
{
    // @TODO: clear focused object in destructor with some specific Fn
    ui->SetFocusedUIObject(this);
    UIObject::OnClick(pos, button);
}

void EditBox::OnChange()
{
    onChangeFn->Execute();
}

float* EditBox::GetColor3f()
{
    if (active)
        return activeColor3f;
    return m_color3f;
}

void EditBox::SetActive(bool act)
{
    active = act;
}

void EditBox::SetActiveColor(float color3f[3])
{
    for (uint8 i = 0; i < 3; ++i)
        activeColor3f[i] = color3f[i];
}

uint8 EditBox::GetEditMode()
{
    return editMode;
}

#endif
