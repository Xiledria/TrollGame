#include <chrono>
#include "Game.h"
#include "World.h"
#include "Config.h"
#include "Initialiser.h"

#ifndef STANDALONE_SERVER
class Graphics;
extern Graphics* graphics;
#endif

using namespace std::chrono_literals;
Game::Game()
{

}

Game::~Game()
{

}
#include <iostream>
uint32 Game::Run()
{
    auto last = std::chrono::high_resolution_clock::now();
    auto now = std::chrono::high_resolution_clock::now();
    auto diff = last - now;
#ifdef STANDALONE_SERVER
    // @TOOO: make it possible to chose which level to start & to launch game
    world->LoadWorld("level.dat");
    // @TODO: add some logic to stop server
    while (true)
#else
    while (graphics || Config::IsEnabled(Config::reloading_graphics)) // when graphics is deleted, stop updating
#endif
    {
#define UPDATE_TIME 20ms
        now = std::chrono::high_resolution_clock::now();
        diff = now - last;
        if (diff < 2 * UPDATE_TIME)
            std::this_thread::sleep_for(UPDATE_TIME - std::max(0ns, diff - UPDATE_TIME));
        std::chrono::duration<float> fdiff = now - last;
        world->Update(fdiff.count());
        last = now;
#ifndef STANDALONE_SERVER
        if (Config::IsEnabled(Config::reloading_graphics))
        {
            // wait for graphics to release its context
            while (graphics)
            {
                std::this_thread::sleep_for(1us);
                continue;
            }

            Config::Toggle(Config::reloading_graphics);

            if (!Initialiser::InitializeGraphics(false))
                break;
        }
#endif
    }
    return EXIT_SUCCESS;
}
