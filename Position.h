#ifndef __POSITION_H
#define __POSITION_H

#include "Vec2.h"

class Object;

class Position : public Vec2<int32>
{
public:
    Position();
    Position(int32 x, int32 y);
    virtual ~Position();
    
    // coords
    virtual int32 GetDistance(int32 x, int32 y);
    // positions
    virtual int32 GetDistance(Position* target);
    virtual int32 GetDistance(Position* target, int32& distX, int32& distY);
    // objects
    virtual int32 GetDistance(Object* target, bool size);
    virtual int32 GetDistance(Object* target, int32& distX, int32& distY, bool size);
};

#endif
