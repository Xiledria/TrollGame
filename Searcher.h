#ifndef __SEARCHER_H
#define __SEARCHER_H

#include "SharedDefs.h"

class Object;
class Position;
class Unit;

struct Searcher
{
    //@TODO: initialiser list, possibly also _names and GetBlablaFns()
    Searcher(Position* searcher, int32 dist)
    {
        owner = searcher;
        distance = dist;
    }
    virtual bool operator()(Object*);
    int32 distance;
    Position* owner;
};

struct UnitSearcher : Searcher
{
    UnitSearcher(Position* searcher, int32 dist) : Searcher(searcher, dist)
    {
    }
    bool operator()(Object*) override;
};

struct HostileUnitSearcher : Searcher
{
    HostileUnitSearcher(Unit* searcher, int32 dist) : Searcher(reinterpret_cast<Position*>(searcher), dist)
    {
    }
    bool operator()(Object*) override;
};

#endif
