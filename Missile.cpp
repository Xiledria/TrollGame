#include "Missile.h"
#include "World.h"
#include "Unit.h"

Missile::Missile(int32 speed, int32 damage, uint32 modelId) : Object(modelId), _speed(speed), _damage(damage), _impacted(false)
{
    world->AddMissile(this);
}

Missile::~Missile()
{

}

void Missile::SetTarget(Position* target, bool ref)
{
    _dest = target;
    _ref = ref;
    
}

void Missile::HandleImpact()
{
    if (!_ref)
        delete _dest;

    if (_ref && static_cast<Object*>(_dest)->GetObjectType() == TYPEID_UNIT)
        static_cast<Unit*>(_dest)->HandleDamageTaken(_damage);

    _dest = nullptr;
    world->RemoveMissile(this);
}

void Missile::UpdateParallel(float diff)
{
    if (!_dest)
        return;

    if (_ref)
    {
        if (!static_cast<Unit*>(_dest)->IsAlive())
        {
            Position* pos = new Position(_dest->GetX(), _dest->GetY());
            _dest = pos;
            _ref = false;
            return;
        }
    }

    int32 distDestX, distDestY, distance;
    distance = _ref ? GetDistance(static_cast<Object*>(_dest), distDestX, distDestY, true) : GetDistance(_dest, distDestX, distDestY);
    if (distance <= 1)
    {
        _impacted = true;
        return;
    }

    int32 diffX = static_cast<int32>((distDestX * diff * _speed) / distance);
    int32 diffY = static_cast<int32>((distDestY * diff * _speed) / distance);

    if (std::abs(distDestX) < std::abs(diffX))
        diffX = distDestX;

    if (std::abs(distDestY) < std::abs(diffY))
        diffY = distDestY;

    SetCoords(GetX() + diffX, GetY() + diffY);
}

void Missile::Update(float /*diff*/)
{
    if (!_dest)
        return world->RemoveMissile(this);

    if (_impacted)
        return HandleImpact();
}
