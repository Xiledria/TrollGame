#include "Initialiser.h"
#include "Config.h"
#include "Network.h"
#ifndef STANDALONE_SERVER
#include "GraphicsInitialize.h"
#include "Graphics.h"
#include "Vec2.h"
#include "Controlls.h"
#include "UIHandler.h"
#endif
#include "World.h"
#include "Game.h"
#include "Player.h"
#include "ObjectStore.h"
#include <thread>

using namespace std::chrono_literals;

Network* network;
Game* game;
World* world;
#ifndef STANDALONE_SERVER
Graphics* graphics;
Vec2<int32>*camera;
Player* player;
Controlls* controlls;
UIHandler* ui;
#endif
ObjectStore* objectStore;
std::thread* renderThread;

namespace Initialiser
{
#ifndef STANDALONE_SERVER
    bool InitializeGraphics(bool initial)
    {
        if (initial)
            camera = new Vec2<int32>();

        graphics = reinterpret_cast<Graphics*>(1);
        renderThread = new std::thread([initial]
        {
            graphics = GraphicsInitialize::CreateGraphicsContext();
            if (!graphics)
                return;

            if (initial)
            {
                controlls = new Controlls();
                ui = new UIHandler();
                camera->SetY(-static_cast<int32>(graphics->GetUIHeight()));
            }
            else
                controlls->Quit(false);
            graphics->Run();
            delete graphics;
            graphics = nullptr;
        });

        while (graphics == reinterpret_cast<Graphics*>(1))
            std::this_thread::sleep_for(5ms);

        return graphics != nullptr;
    }
#endif
    bool Init()
    {
        network = new Network();
        Config::Init();
        world = new World();
#ifndef STANDALONE_SERVER
        player = new Player();
#endif
        objectStore = new ObjectStore();
#ifndef STANDALONE_SERVER
        InitializeGraphics();
#endif
        game = new Game();
        return true;
    }

    void Exit()
    {
        Config::Save();
#ifndef STANDALONE_SERVER
        renderThread->join();
        delete controlls;
#endif
        delete game;
#ifndef STANDALONE_SERVER
        delete renderThread;
        delete player;
#endif
        delete world;
        delete objectStore;
    }
}
