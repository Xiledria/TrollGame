#ifndef __WORLD_H
#define __WORLD_H

#include "SharedDefs.h"
#include <vector>
#include <list>
#include <thread>
#include <string>
//@TODO: _names, initialisere list
class Object;
class Terrain;
class Position;
class Missile;
struct Searcher;

struct RemovedObject
{
    RemovedObject(Object* obj)
    {
        this->obj = obj;
    }
    Object* obj;
    float timer = 1.0f;
};

class World
{
public:
    World();
    void CreateNewWorld(std::string name, uint32 x, uint32 y);
    void LoadWorld(std::string worldMap);
    ~World();
    void AddObject(Object* obj);
    void RemoveObject(Object* obj);
    void AddTerrain(Terrain* terrain, uint32 x, uint32 y);
    void AddMissile(Missile* missile);
    void RemoveMissile(Missile* missile);
    uint32 GetGridSize();
    uint32 GetGridMaxX();
    uint32 GetGridMaxY();
    uint32 GetMaxX();
    uint32 GetMaxY();

    int32 NormalizeCoord(int32 pos);
    int32 CalculateGridIndex(int32 pos);
    void IncreaseWorldSize();
    void SaveToFile();
    void Update(float diff);
    std::vector<Missile*>& GetMissiles();
    std::vector<Terrain*>& GetTerrain();
    bool IsTerrainWalkableBy(uint32 x, uint32 y, MoveFlags flags);
    uint32 GenerateObjectId();
    uint8 GeneratePlayerId();
    void GetObjectsInArea(Position* start, Position* end, std::vector<Object*>& objects);
    void GetObjectsInArea(Position* pos, int32 distance, std::vector<Object*>& objects);
    void GetObjectsInArea(std::vector<Object*>& objects, Searcher& searcher);
    void GetObjectsInArea(int32 posX, int32 posY, int32 distance, std::vector<Object*>& objects);
    std::vector<Object*>& GetObjects();
    bool RelocateObject(Object* obj, int32 newX, int32 newY);
    // @TODO: do function for this, lol
    std::vector<Object*> _objects;
private:
    void Init();
    void GeneratePaths();
    uint32 _gridSize;
    uint32 _gridMaxX;
    uint32 _gridMaxY;
    uint32 _objectIdCounter;
    uint8 _playerIdCounter;
    uint32 _missilesCount;
    uint32 _objectsCount;
    // @TODO: fix me, move me to objectsMap or something else
    std::vector<Missile*> _missiles;
    std::vector<Object*> _objectsMap;
    std::vector<Terrain*> _terrainVec;
    std::list<RemovedObject*> _removedObjects;
    std::thread* _pathfinderThread;
    std::string _worldName;
    bool _deleting;
};

extern World* world;

#endif
