#ifndef __MAP2D_H
#define __MAP2D_H

#include <map>
#include <list>
#include "SharedDefs.h"
template <typename T>
using map2d = std::map<uint32, std::map<uint32, T>>;
template <typename T>
using map2dlist = map2d<std::list<T>>;

#endif
