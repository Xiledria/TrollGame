#ifdef BUILD_VULKAN_GRAPHICS
#ifndef __VK_GRAPHICS_H
#define __VK_GRAPHICS_H

#include "Graphics.h"
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
class VKGraphics : public Graphics
{
public:
    VKGraphics(Game* game);
    ~VKGraphics() override;
    void Run() override;
private:
    VkApplicationInfo applicationInfo;
    VkInstanceCreateInfo instanceInfo;
    VkInstance instance;
};

#endif
#endif
