#ifndef __MISSILE_H
#define __MISSILE_H

#include "Object.h"

class Missile : public Object
{
public:
    Missile(int32 speed, int32 damage, uint32 modelId);
    void SetTarget(Position* dest, bool ref);
    ~Missile();
    void UpdateParallel(float diff) override;
    void Update(float diff) override;
private:
    void HandleImpact();
private:
    int32 _speed;
    int32 _damage;
    bool _impacted;
    Position* _dest;
    bool _ref;
};

#endif
