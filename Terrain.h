#ifndef __TERRAIN_H
#define __TERRAIN_H

#include "SharedDefs.h"

class Terrain
{
public:
    Terrain(TerrainType type = TERRAIN_PLACEHOLDER);
    Terrain(Terrain* copy);
    ~Terrain();
    void SetType(TerrainType type);
    TerrainType GetType();
    MoveFlags GetMoveFlags();
    const float* GetColor3f();
private:
    void SetColor(float r, float g, float b);
    // colors of terrain @TODO: use textures / bitmaps later (maybe model or custom GLProgram?)
    float _color3f[3];
    TerrainType _type;
    MoveFlags _moveFlags;
};

#endif
