#ifndef STANDALONE_SERVER
#include <list>

#include "Controlls.h"
#include SDL_INCLUDE
#include "Graphics.h"
#include "Player.h"
#include "World.h"
#include "Unit.h"
#include "Terrain.h"
#include "Config.h"
#include "UIObject.h"
#include "UIHandler.h"

Controlls::Controlls() : selection(new Position[SELECTION_ARRAY_SIZE]), ScrollSpeed(world->GetGridSize()), quit(false), isSelecting(false), clickStartObj(nullptr)
{
}

Controlls::~Controlls()
{
    delete[] selection;
}

void Controlls::Quit(bool isQuitting) // terminates progam
{
    quit = isQuitting;
}

Controlls::ClickLocation Controlls::GetMouseLocation(uint32 x, uint32 y)
{
    if (y >= graphics->GetUIHeight())
        return LOCATION_WORLD;

    if (x >= graphics->GetScreenX() - graphics->GetMinimapSize() && y <= graphics->GetMinimapSize())
        return LOCATION_MINIMAP;

    return LOCATION_UI;
}

void Controlls::HandleMenuBuilderClick(uint32 x, uint32 y)
{
    if (Config::CanEdit())
    {
        uint32 gridHalf = world->GetGridSize() / 2;
        if (y > gridHalf && y < gridHalf * 3)
        {
            if (x > gridHalf && y < gridHalf * 2 * (TERRAIN_MAX - 1))
            {
                TerrainType type = TerrainType(world->CalculateGridIndex(x - gridHalf));
                player->UpdateBuilder(type);
            }
        }
    }
}

void Controlls::HandleBuildTerrain(uint32 x, uint32 y)
{
    uint32 gridX = world->CalculateGridIndex(x + camera->GetX());
    uint32 gridY = world->CalculateGridIndex(y + camera->GetY());

    if (gridX < world->GetGridMaxX() && gridY < world->GetGridMaxY())
    {
        world->AddTerrain(new Terrain(player->GetBuilder()), gridX, gridY);
        graphics->RecreateMinimap();
    }
}

void Controlls::HandleSendSelected(UIObject* obj)
{
    auto pos = GetWorldPosition(obj->GetRelativeClickPos());
    bool found = false;

    std::vector<Object*> objects;
    world->GetObjectsInArea(pos.GetX(), pos.GetY(), 0, objects);
    for (auto& itr : objects)
    {
        if (itr->GetObjectType() != TYPEID_UNIT)
            continue;

        Unit* u = static_cast<Unit*>(itr);
        found = true;

        for (auto* selected : player->GetSelected())
            selected->MoveTo(u);
        break;
    }

    if (!found)
    {
        //@TODO: check boundary of world
        int32 maxX = static_cast<int32>(world->GetMaxX());
        int32 maxY = static_cast<int32>(world->GetMaxY());
        int32 gridSize = static_cast<int32>(world->GetGridSize());
        int32 index = 0;
        int32 indexMax = player->GetSelected().size();
        auto& units = player->GetSelected();
        int32 dist = std::pow(units.size(), 0.5);
        dist /= 2;
        std::list<Vec2<int32>> destinations;
        auto itr = player->GetSelected().begin();
        int processedCount = 0;

        Position worldPos = Position(pos.GetX(), pos.GetY());
		int32 maxDist = gridSize * dist;
		for (int32 x = pos.GetX() - maxDist; x <= pos.GetX() + maxDist; x += gridSize)
		{
			for (int32 y = pos.GetY() - maxDist; y <= pos.GetY() + maxDist; y += gridSize)
			{
				if (processedCount < units.size())
				{
					++processedCount;
					destinations.emplace_back(x, y);
				}
				else break;
			}
			if (processedCount == units.size())
				break;
        }

        units.sort([](Unit* a, Unit* b) { return *a < *b; });
        destinations.sort();

        if (destinations.size() != units.size())
        {
            DEBUG(std::cout << "Invalid destinations count" << std::endl);
        }

        int32 maxCount = std::min(destinations.size(), units.size());
        auto unitsItr = units.begin();
        auto destItr = destinations.begin();
        while (unitsItr != units.end() && destItr != destinations.end())
        {
            auto dest = *destItr;
            (*unitsItr)->MoveTo(dest.GetX(), dest.GetY());
            ++unitsItr;
            ++destItr;
        }
	}

}

Vec2<int32> Controlls::GetWorldPosition(Vec2<int32> pos)
{
    return pos + *camera;
}

void Controlls::SetAreaSelectionBegin(uint32 x, uint32 y)
{
    if (Config::IsEnabled(Config::paused))
        return;

    DEBUG(std::cout << "Setting start selection to " << x << ":" << y << std::endl);
    selection[SELECTION_BEGIN].SetCoords(x, y);
    selection[SELECTION_END].SetCoords(x, y);
    isSelecting = true;
}

void Controlls::SetAreaSelectionEnd(uint32 x, uint32 y)
{
    if (Config::IsEnabled(Config::paused))
        return;

    DEBUG(std::cout << "Setting end selection to " << x << ":" << y << std::endl);
    selection[SELECTION_END].SetCoords(x, y);
}

void Controlls::HandleSelectArea()
{
    auto kbd = SDL_GetKeyboardState(nullptr);
    bool add = kbd[SDL_SCANCODE_LSHIFT] || kbd[SDL_SCANCODE_RSHIFT];
    if (!add)
        player->ClearSelected();

    std::vector<Object*> objects;
    Position startPos, endPos;
    startPos.SetCoords(GetWorldPosition(selection[SELECTION_BEGIN]));
    endPos.SetCoords(GetWorldPosition(selection[SELECTION_END]));
    world->GetObjectsInArea(&startPos, &endPos, objects);
    for (auto& itr : objects)
    {
        if (itr->GetObjectType() != TYPEID_UNIT)
            continue;

        Unit* u = static_cast<Unit*>(itr);
        if (u->GetOwnerId() == player->GetId())
        {
            DEBUG(std::cout << "Selecting unit ID " << u->GetId() << std::endl);
            player->AddSelected(u);
        }
    }
    selection[0].SetCoords(0, 0);
    selection[1].SetCoords(0, 0);
    isSelecting = false;
}

void Controlls::HandleMouseDrag(SDL_Event& e, MouseType button)
{
    if (UIObject* parent = ui->GetVisibleParentObject())
    {
        Vec2<int32> clickPos(e.button.x, graphics->GetScreenY() - e.button.y);
        parent->HandleDrag(clickPos, button);
    }
}

void Controlls::HandleWorldClick(UIObject* obj)
{
    auto pos = obj->GetRelativeClickPos();
    auto x = pos.GetX();
    auto y = pos.GetY();
    if (Config::CanEdit())
    {
        HandleBuildTerrain(x, y);
        return;
    }

    if (!isSelecting)
        SetAreaSelectionBegin(x, y);

    selection[SELECTION_OFFSET].SetCoords(obj->GetStart());
    HandleSelectArea();
}

void Controlls::HandleWorldDrag(UIObject* obj)
{
    auto pos = obj->GetRelativeClickPos();
    auto x = pos.GetX();
    auto y = pos.GetY();
    selection[SELECTION_OFFSET].SetCoords(obj->GetStart());
	if (Config::CanEdit())
	{
        HandleBuildTerrain(x, y);
        return;
	}

    if (isSelecting)
        SetAreaSelectionEnd(x, y);
    else
        SetAreaSelectionBegin(x, y);
}

void Controlls::HandleMouseClick(SDL_Event& e, MouseType button)
{
    if (UIObject* parent = ui->GetVisibleParentObject())
    {
        Vec2<int32> clickPos(e.button.x, graphics->GetScreenY() - e.button.y);
        parent->HandleClick(clickPos, button);
    }
  /*  uint32 x = e.button.x;
    uint32 y = graphics->GetScreenY() - e.button.y;

    switch (GetMouseLocation(x, y))
    {
        case LOCATION_WORLD:
        {
            if (Config::CanEdit())
            {
                HandleBuildTerrain(x, y);
                break;
            }

            SetAreaSelectionBegin(x, y);
            break;
        }
        case LOCATION_UI:
        {
            if (Config::CanEdit())
                HandleMenuBuilderClick(x, y);
            break;
        }
        default:
            break;
    }*/
}
/*
void Controlls::HandleMouseRightClick(SDL_Event& e)
{

    uint32 x = e.button.x;
    uint32 y = graphics->GetScreenY() - e.button.y;
    switch (GetMouseLocation(x, y))
    {
        case LOCATION_WORLD:
        {
            if (Config::CanEdit())
            {
                HandleBuildTerrain(x, y);
                break;
            }

            HandleSendSelected(x, y);
            break;
        }
        case LOCATION_UI:
        {
            if (Config::CanEdit())
                HandleMenuBuilderClick(x, y);
            break;
        }
        default:
            break;
    }
}*/

bool Controlls::HandleKeyboardEvent(SDL_Keycode& key)
{
    SDL_StartTextInput();
    if (EditBox* focusedUIObject = ui->GetFocusedObject())
    {
        SDL_Keycode _key = key;
        uint8 flags = focusedUIObject->GetEditMode();
        bool isLetter = (key >= SDLK_a && key <= SDLK_z);
        bool isNumber = (key >= SDLK_KP_1 && key <= SDLK_KP_0) // 0 is last in this case
            || (key >= SDLK_0 && key <= SDLK_9);

        if (isNumber && key >= SDLK_KP_1)
        {
            if (key == SDLK_KP_0)
                _key = SDLK_0;
            else
                _key = key - SDLK_KP_1 + SDLK_1;
        }

        bool isAccepted = (isLetter && flags & EditBox::Flags::ACCEPT_LETTERS)
            || (isNumber && flags & EditBox::Flags::ACCEPT_NUMBERS)
            || (_key == SDLK_UNDERSCORE && flags & EditBox::Flags::ACCEPT_SYMBOLS);
        if (isAccepted)
        {
            char ascii = static_cast<uint8>(_key);
            auto kbd = SDL_GetKeyboardState(nullptr);
            bool upper = kbd[SDL_SCANCODE_LSHIFT] || kbd[SDL_SCANCODE_RSHIFT];
            if (kbd[SDL_SCANCODE_CAPSLOCK])
                upper = !upper;

            if (upper && isLetter)
                ascii -= 32;

            //@TODO: UIHandler move
            std::string newText = focusedUIObject->GetText() + ascii;
            focusedUIObject->SetText(newText);
            focusedUIObject->OnChange();
        }
        if (_key == SDLK_BACKSPACE)
        {
            std::string text = focusedUIObject->GetText();
            if (!text.empty())
                text.pop_back();

            focusedUIObject->SetText(text);
            focusedUIObject->OnChange();
        }
    }

    switch (key)
    {
        case SDLK_ESCAPE:
        {
            if (!Config::IsEnabled(Config::paused))
            {
                ui->MoveToMenu(UIHandler::MAIN_MENU);
                // @TODO: check if game is finished later when there will be some game objectives :D
                //@TODO: UIHandler move
                //mainMenu->SetCanContinue(true);
            }
            ui->SetFocusedUIObject(nullptr);
            break;
        }
        case SDLK_LEFT:
        {
            camera->SetX(camera->GetX() - ScrollSpeed);
            graphics->UpdateCamera();
            break;
        }
        case SDLK_RIGHT:
        {
            camera->SetX(camera->GetX() + ScrollSpeed);
            graphics->UpdateCamera();
            break;
        }
        case SDLK_UP:
        {
            camera->SetY(camera->GetY() + ScrollSpeed);
            graphics->UpdateCamera();
            break;
        }
        case SDLK_DOWN:
        {
            camera->SetY(camera->GetY() - ScrollSpeed);
            graphics->UpdateCamera();
            break;
        }
        case SDLK_KP_PLUS:
        {
            world->IncreaseWorldSize();
            graphics->HandleWorldResize();
            break;
        }
        case SDLK_KP_MINUS:
        {
            break;
        }
        case SDLK_KP_1:
        {
            player->UpdateBuilder(TERRAIN_PLACEHOLDER);
            break;
        }
        case SDLK_KP_2:
        {
            player->UpdateBuilder(TERRAIN_GRASS);
            break;
        }
        case SDLK_KP_3:
        {
            player->UpdateBuilder(TERRAIN_DIRT);
            break;
        }
        case SDLK_KP_4:
        {
            player->UpdateBuilder(TERRAIN_WATER);
            break;
        }
        default:
            break;
    }
    return false;
}

bool Controlls::Update(float /*diff*/)
{
    if (!Config::IsEnabled(Config::paused))
        player->RemoveDeadSelected();

    SDL_Event e;
    while (SDL_PollEvent(&e) != 0)
    {
        //User requests quit
        switch (e.type)
        {
            case SDL_QUIT:
            {
                quit = true;
                break;
            }
            case SDL_WINDOWEVENT:
            {
                switch (e.window.event)
                {
                    case SDL_WINDOWEVENT_RESIZED:
                    {
                        graphics->Resize();
                        //@TODO: UIHandler move
                        /*for (auto UIObj : UIObjects)
                            UIObj->OnWindowResized(graphics->GetScreenX(), graphics->GetScreenY());*/
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            case SDL_MOUSEMOTION:
            {
                if (e.motion.state & SDL_BUTTON_LMASK)
                    HandleMouseDrag(e, MouseType::LEFT_BUTTON);
                if (e.motion.state & SDL_BUTTON_MMASK)
                    HandleMouseDrag(e, MouseType::MIDDLE_BUTTON);
                if (e.motion.state & SDL_BUTTON_RMASK)
                    HandleMouseDrag(e, MouseType::RIGHT_BUTTON);
                break;
            }
            case SDL_MOUSEBUTTONDOWN:
            {
                switch (e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    case SDL_BUTTON_MIDDLE:
                    case SDL_BUTTON_RIGHT:
                        if (UIObject* obj = ui->GetVisibleParentObject())
                        {
                            Vec2<int32> pos(e.button.x, graphics->GetScreenY() - e.button.y);
                            obj = obj->GetTopObjectAtPos(pos);
                            SetClickStartedObject(obj);
                        }
                        else
                            SetClickStartedObject(nullptr);
                        break;
                    default:
                        break;
                }
                break;
            }
            case SDL_MOUSEBUTTONUP:
            {
                switch (e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                        HandleMouseClick(e, MouseType::LEFT_BUTTON);
                        break;
                    case SDL_BUTTON_MIDDLE:
                        HandleMouseClick(e, MouseType::MIDDLE_BUTTON);
                        break;
                    case SDL_BUTTON_RIGHT:
                        HandleMouseClick(e, MouseType::RIGHT_BUTTON);
                        break;
                    // @TODO: handle right click for world frame
                    // @TODO: create function for this
                    /*case SDL_BUTTON_LEFT:
                    {
                        if (!Config::IsEnabled(Config::paused) && isSelecting)
                        {
                            SetAreaSelectionEnd(e.button.x, graphics->GetScreenY() - e.button.y);
                            HandleSelectArea();
                        }
                        break;
                    }*/
                    default:
                        break;
                }
                SetClickStartedObject(nullptr);
                break;
            }
            case SDL_KEYDOWN:
            {
                quit = HandleKeyboardEvent(e.key.keysym.sym);
                break;
            }
            default:
                break;
        }
    }
    return quit || Config::IsEnabled(Config::reloading_graphics);
}

void Controlls::SetClickStartedObject(UIObject* obj)
{
    if (clickStartObj)
        clickStartObj->SetClickStarted(false);
    if (obj)
        obj->SetClickStarted(true);
    clickStartObj = obj;
}

Position* Controlls::GetSelectionArea()
{
    return selection;
}

#endif
