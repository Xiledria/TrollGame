#include "Config.h"
#include <cstring>
#ifndef STANDALONE_SERVER
#include <pugixml.hpp>

// strings
#define STRING_FPS_LIMIT    "FPS_Limit"
#define STRING_VSYNC        "VSync"
#define STRING_FULLSCREEN   "Fullscreen"
#define STRING_SHOW_FPS     "Show_FPS"
#define STRING_GRAPHICS_API "Graphics_API"
#define STRING_OPENGL_2_1   "OpenGL_2.1"
#define STRING_OPENGL_4_3   "OpenGL_4.3"

#endif

namespace Config
{
    FLAGTYPE _flags;
    uint16 fpsLimit;

    bool Toggle(ConfigFlags flags)
    {
        _flags ^= flags;
        return IsEnabled(flags);
    }

    ConfigFlags operator |(ConfigFlags a, ConfigFlags b)
    {
        return static_cast<ConfigFlags>(static_cast<FLAGTYPE>(a) | static_cast<FLAGTYPE>(b));
    }

    ConfigFlags operator &(ConfigFlags a, ConfigFlags b)
    {
        return static_cast<ConfigFlags>(static_cast<FLAGTYPE>(a) & static_cast<FLAGTYPE>(b));
    }

    bool IsAnyEnabled(ConfigFlags flags)
    {
        return ((_flags & flags) != 0);
    }

    bool IsEnabled(ConfigFlags flags)
    {
        return ((_flags & flags) == flags);
    }

    void Set(ConfigFlags flags)
    {
        _flags |= flags;
    }

    void Clear(ConfigFlags flags)
    {
        _flags &= ~flags;
    }

    bool CanEdit()
    {
        return IsEnabled(editable) && !IsEnabled(paused);
    }

    uint16 GetFPSLimit()
    {
        return fpsLimit;
    }

    void SetFPSLimit(uint16 fps)
    {
        fpsLimit = fps;
    }

    void Init()
    {
#ifdef STANDALONE_SERVER
        _flags = 0;
#else
        pugi::xml_document doc;
        doc.load_file("config.cfg");
        pugi::xml_node cfg = doc.child("config");
        fpsLimit = cfg.attribute(STRING_FPS_LIMIT).as_int(60);
        if (cfg.attribute(STRING_VSYNC).as_bool(true))
            Set(vsync);
#ifndef _DEBUG
        if (cfg.attribute(STRING_FULLSCREEN).as_bool(true))
#else
        if (cfg.attribute(STRING_FULLSCREEN).as_bool(false))
#endif
            Set(fullscreen);
        if (cfg.attribute(STRING_SHOW_FPS).as_bool(true))
            Set(showFPS);
        if (strcmp(cfg.attribute(STRING_GRAPHICS_API).as_string(STRING_OPENGL_2_1), STRING_OPENGL_2_1))
            Set(opengl_4_3);
        Clear(editable | paused | reloading_graphics);
#endif
    }
    void Save()
    {
#ifndef STANDALONE_SERVER
        pugi::xml_document doc;
        pugi::xml_node cfg = doc.append_child("config");
        cfg.append_attribute(STRING_FPS_LIMIT).set_value(fpsLimit);
        cfg.append_attribute(STRING_VSYNC).set_value(static_cast<int32>(IsEnabled(vsync)));
        cfg.append_attribute(STRING_FULLSCREEN).set_value(static_cast<int32>(IsEnabled(fullscreen)));
        cfg.append_attribute(STRING_SHOW_FPS).set_value(static_cast<int32>(IsEnabled(showFPS)));
        cfg.append_attribute(STRING_GRAPHICS_API).set_value(IsEnabled(opengl_4_3) ? STRING_OPENGL_4_3 : STRING_OPENGL_2_1);
        doc.save_file("config.cfg");
#endif
    }
}
