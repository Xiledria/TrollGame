#ifndef __GAME_H
#define __GAME_H

#include "SharedDefs.h"

class Game
{
public:
    Game();
    ~Game();
    uint32 Run();
};

extern Game* game;

#endif
