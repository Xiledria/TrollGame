#ifndef __CONFIG_H
#define __CONFIG_H

#include "SharedDefs.h"

namespace Config
{
    #define FLAGTYPE int32
    enum ConfigFlags : FLAGTYPE
    {
        // SAVEABLE
        vsync               = 0x01,
        fullscreen          = 0x02,
        showFPS             = 0x04,
        opengl_4_3          = 0x08,
        // NOT SAVEABLE
        editable            = 0x10,
        paused              = 0x20,
        reloading_graphics  = 0x40,
        world_initialized   = 0x80,
    };

    ConfigFlags operator |(ConfigFlags a, ConfigFlags b);
    ConfigFlags operator &(ConfigFlags a, ConfigFlags b);

    bool Toggle(ConfigFlags flags);
    void Set(ConfigFlags flags);
    void Clear(ConfigFlags flags);
    bool IsAnyEnabled(ConfigFlags flags);
    bool IsEnabled(ConfigFlags flags);
    bool CanEdit();
    uint16 GetFPSLimit();
    void SetFPSLimit(uint16 fps);

    void Init();
    void Save();
}

#endif
