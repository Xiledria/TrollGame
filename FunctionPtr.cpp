#ifndef STANDALONE_SERVER

#include "FunctionPtr.h"

NoParamFunction::NoParamFunction(std::function<void()> fn)
: _fn(fn)
{
}

void NoParamFunction::Execute()
{
    _fn();
}

OneParamFunction::OneParamFunction(std::function<void(int32)> fn, int32 param1)
: _fn(fn), _param1(param1)
{
}

void OneParamFunction::Execute()
{
    _fn(_param1);
}

MemberNoParamFunction::MemberNoParamFunction(std::function<void(UIObject*)> fn, UIObject* obj)
: _fn(fn), _obj(obj)
{
}

void MemberNoParamFunction::Execute()
{
    _fn(_obj);
}

MemberOneParamFunction::MemberOneParamFunction(std::function<void(UIObject*, int32)> fn, UIObject* obj, int32 param1)
: _fn(fn), _obj(obj), _param1(param1)
{
}

void MemberOneParamFunction::Execute()
{
    _fn(_obj, _param1);
}

MemberTwoParamFunction::MemberTwoParamFunction(std::function<void(UIObject*, int32, std::string)> fn, UIObject* obj, int32 param1, std::string param2)
: _fn(fn), _obj(obj), _param1(param1), _param2(param2)
{
}

void MemberTwoParamFunction::Execute()
{
    _fn(_obj, _param1, _param2);
}

MemberThreeParamFunction::MemberThreeParamFunction(std::function<void(UIObject*, int32, std::string, std::string)> fn,
UIObject* obj, int32 param1, std::string param2, std::string param3) : _fn(fn), _obj(obj), _param1(param1), _param2(param2), _param3(param3)
{
}

void MemberThreeParamFunction::Execute()
{
    _fn(_obj, _param1, _param2, _param3);
}

#endif
