#ifndef STANDALONE_SERVER
#ifndef __UI_HANDLER_H
#define __UI_HANDLER_H

#include <map>

#include "SharedDefs.h"
#include "UIObject.h"
#include "Vec2.h"

namespace pugi
{
    class xml_node;
}

class UIHandler
{
public:
    enum Menu
    {
        MAIN_MENU,
        //SINGLEPLAYER_MENU,
        //MULTIPLAYER_MENU,
        //EDIT_LEVEL_MENU,
        //NEW_WORLD_MENU,
        //CONFIG_MENU,
        INGAME_MENU = 6,
    };

    UIHandler();
    ~UIHandler();

    EditBox* GetFocusedObject();
    void MoveToMenu(Menu menu);
    void SetFocusedUIObject(EditBox* obj);
    UIObject* GetVisibleParentObject();
private:
    bool LoadBasicObjectAttributes(pugi::xml_node* node, float* color3f, Vec2<int32>& size, Vec2<float>& pctSize, Vec2<int32>& pos, Vec2<float>& pctPos, int32 menuId);
    void LoadChildrens(pugi::xml_node& parentNode, UIObject* parentObject, int32 menuId);
    void LoadFunctionHandlers(pugi::xml_node& node, UIObject* object, bool* clickable = nullptr);
    void LoadUI();
    UIObject* visibleUIParent;
    std::map<int32, UIObject*> UIParents;
    EditBox* focusedUIObject;
};
extern UIHandler* ui;

#endif
#endif
