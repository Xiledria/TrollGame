#include "Message.h"
#include <algorithm>
#include <iostream>

Message::Message(char* data, uint16 rcvbytes) : _id(static_cast<MessageType>(0)), _size(rcvbytes - sizeof(_id)),
_begin(0), _end(0), _buffer(new char[rcvbytes - sizeof(_id) + 1])
{
    memcpy((uint8*)&_id, data, sizeof(_id));
    memcpy(_buffer, data + sizeof(_id), rcvbytes - sizeof(_id));
}

Message::Message(MessageType id, uint16 size, SOCKET target) : _target(target), _id(id), _size(size), _begin(0), _end(0), _buffer(new char[size+1])
{
    _buffer[size] = 0;
}

Message::~Message()
{
    delete[] _buffer;
}

char* Message::GetString(uint16 len)
{
    char* tmp = _buffer+_begin;
    if (size() == 0)
        return nullptr;

    if (len)
    {
        std::string tmpstr = tmp;
        tmpstr.resize(len);
        _begin += len;
        tmp = new char[tmpstr.size() + 1];
        memcpy(tmp, tmpstr.c_str(), tmpstr.size());
        tmp[tmpstr.size()] = 0;
        return tmp;
    }
    else
        tmp[size()] = 0;
    return tmp;
}

void Message::Append(const char *value)
{
    uint32 len = uint32(strlen(value));
    memcpy(_buffer+_end,value,len);
    _end += len;
}

void Message::Append(const char *value, uint16 len)
{
    memcpy(_buffer+_end,value,len);
    _end += len;
}

void Message::operator<<(std::string value)
{
    *this << value.c_str();
}

void Message::operator<<(char* value)
{
    *this << (const char*)value;
}

void Message::operator<<(const char* value)
{
    uint32 len = uint32(strlen(value)+1);
    memcpy(_buffer+_end,value,len);
    _end += len;
}

uint16 Message::size(bool whole)
{
    return whole  ? _size : std::max(int32(_size-_begin),int32(0));
}
char* Message::ToPacket(uint16& length)
{
    length = _size + sizeof(_id) + GetBufferByteSize();
    char* packet = new char[length + 1];
    uint16 size = _size + sizeof(_id);
    memcpy(packet, &size, sizeof(size));
    memcpy(packet + GetBufferByteSize(), &_id, sizeof(_id));
    memcpy(packet + sizeof(_id) + GetBufferByteSize(), _buffer, _size);
    packet[length] = 0;
    return packet;
}

MessageType Message::GetId()
{
    return _id;
}

SOCKET Message::GetTarget()
{
    return _target;
}

uint8 Message::GetBufferByteSize()
{
    return sizeof(_size);
}
