#ifndef __VEC2_H
#define __VEC2_H

#include "SharedDefs.h"
#include <cmath>

template<typename TYPE>
class Vec2
{
public:
    Vec2()
    {
        _coords[0] = 0;
        _coords[1] = 0;
    }

    Vec2(TYPE x, TYPE y)
    {
        _coords[0] = x;
        _coords[1] = y;
    }

    ~Vec2()
    {

    }

    template<typename T>
    Vec2 operator-(Vec2<T> vec)
    {
        return Vec2(GetX() - vec.GetX(), GetY() - vec.GetY());
    }

    template<typename T>
    Vec2 operator+(Vec2<T> vec)
    {
        return Vec2(GetX() + vec.GetX(), GetY() + vec.GetY());
    }

    template<typename T>
    Vec2 operator+=(Vec2<T> vec)
    {
        SetCoords(GetX() + vec.GetX(), GetY() + vec.GetY());
        return *this;
    }

    template<typename T>
    Vec2 operator-=(Vec2<T> vec)
    {
        SetCoords(GetX() - vec.GetX(), GetY() - vec.GetY());
        return *this;
    }

    template<typename T>
    Vec2 operator*=(Vec2<T> val)
    {
        SetCoords(GetX() * val.GetX(), GetY() * val.GetY());
        return *this;
    }

    template<typename T>
    Vec2 operator*=(T val)
    {
        SetCoords(GetX() * val, GetY() * val);
        return *this;
    }

    template<typename T>
    Vec2 operator*(Vec2<T> val)
    {
        return Vec2(GetX() * val.GetX(), GetY() * val.GetY());
    }

    template<typename T>
    Vec2 operator*(T val)
    {
        return Vec2(GetX() * val, GetY() * val);
    }

    template<typename T>
    Vec2 operator/=(Vec2<T> val)
    {
        SetCoords(GetX() / val.GetX(), GetY() / val.GetY());
        return *this;
    }

    template<typename T>
    Vec2 operator/=(T val)
    {
        SetCoords(GetX() / val, GetY() / val);
        return *this;
    }

    template<typename T>
    Vec2 operator/(Vec2<T> val)
    {
        return Vec2(GetX() / val.GetX(), GetY() / val.GetY());
    }

    template<typename T>
    Vec2 operator/(T val)
    {
        return Vec2(GetX() / val, GetY() / val);
    }

    template<typename T>
    Vec2 operator=(Vec2<T> val)
    {
       SetCoords(val.GetX(), val.GetY());
       return *this;
    }


    bool operator>=(Vec2& val)
    {
        return GetX() >= val.GetX() && GetY() >= val.GetY();
    }

	bool operator<(Vec2& obj)
	{
		if (GetX() < obj.GetX())
			return true;

		if (GetX() > obj.GetX())
			return false;

		if (GetY() < obj.GetY())
			return true;

		if (GetY() > obj.GetY())
			return false;

		return this < &obj;
	}

    void SetX(TYPE x)
    {
        _coords[0] = x;
    }

    void SetY(TYPE y)
    {
        _coords[1] = y;
    }

    void SetCoords(TYPE x, TYPE y)
    {
        _coords[0] = x;
        _coords[1] = y;
    }

    void SetCoords(Vec2 newCoords)
    {
        _coords[0] = newCoords.GetX();
        _coords[1] = newCoords.GetY();
    }

    TYPE GetX()
    {
        return _coords[0];
    }

    TYPE GetY()
    {
        return _coords[1];
    }

    TYPE* GetCoords()
    {
        return _coords;
    }

    TYPE GetLength()
    {
        return static_cast<int32>(std::sqrt(GetX() * GetX() + GetY() * GetY()));
    }
private:
    TYPE _coords[2];
};

#endif
