#ifndef __SHARED_DEFS_H
#define __SHARED_DEFS_H

//@TODO: move this block to cmake
//#define DEBUG_MEMORY
//#define STANDALONE_SERVER
//#define _DEBUG

#ifdef _WIN32
#define NOMINMAX
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <Windows.h>
#ifndef DEBUG_MEMORY
#ifdef USE_JEMALLOC
#include <jemalloc.h>
#endif
#endif
#else
#ifdef USE_JEMALLOC
#define JEMALLOC_NO_DEMANGLE
#include <jemalloc/jemalloc.h>
#endif
#define closesocket close
#define SOCKET int32
#define INVALID_SOCKET -1
#define SOCKET_ERROR   -1
#endif
#include <cstdint>
#include <algorithm>
#include <vector>

typedef int8_t     int8;
typedef int16_t   int16;
typedef int32_t   int32;
typedef int64_t   int64;
typedef uint8_t   uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

#ifdef __linux__
typedef uint32 ft_int;
#elif _WIN32
typedef int32 ft_int;
#endif

#define PROJECT_NAME "Troll Game"

enum MoveFlags : uint8
{
    MOVEFLAG_NONE      = 0x0,
    MOVEFLAG_WALKING   = 0x1,
    MOVEFLAG_SWIMMING  = 0x2,
    MOVEFLAG_FLYING    = 0x4,
    MOVEFLAG_ALL       = 0x7,
};

enum MovePositions : uint8
{
    MOVE_HORIZONTAL,
    MOVE_UP = MOVE_HORIZONTAL,
    MOVE_VERTICAL,
    MOVE_DOWN = MOVE_VERTICAL,
    MOVE_LEFT,
    MOVE_MAX_LINES = MOVE_LEFT,
    MOVE_RIGHT,
    MOVE_STRAIGHT_MAX,
    MOVE_UP_LEFT = MOVE_STRAIGHT_MAX,
    MOVE_UP_RIGHT,
    MOVE_DOWN_LEFT,
    MOVE_DOWN_RIGHT,
    MOVE_ALL_MAX,
};

enum TerrainType : uint8
{
    TERRAIN_PLACEHOLDER,
    TERRAIN_GRASS,
    TERRAIN_DIRT,
    TERRAIN_WATER,
    TERRAIN_MAX,
};

enum class GraphicsInitializeErrors : uint8
{
    NONE,
    FAILED_SDL_INIT,
    UNSUPPORTED_GL_VERSION,
    FAILED_LOAD_FONT_LIB,
    FAILED_LOAD_FONT,
};

enum MouseType : uint8
{
    LEFT_BUTTON,
    MIDDLE_BUTTON,
    RIGHT_BUTTON,
    TOTAL_BUTTONS
};


#ifdef _DEBUG
#include <iostream>
#ifdef _DEBUG
#if !defined(DBG_NEW) && defined(DEBUG_MEMORY)
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif  // _DEBUG
#define DEBUG(x) x
#else
#define DEBUG(x)
#endif


#ifndef DEBUG_MEMORY
void* operator new(std::size_t sz);
void operator delete(void* ptr) noexcept;
#endif

MoveFlags operator|(MoveFlags a, MoveFlags b);
int32 irand(int32 start, int32 end);
float frand(float start, float end);
template <typename T>
T GetRandomItem(std::vector<T> items)
{
    auto itr = items.begin();
    if (items.size() > 1)
        std::advance(itr, static_cast<size_t>(irand(0, static_cast<int32>(items.size()) - 1)));
    return static_cast<T>(*itr);
}

#ifdef _WIN32
#define SDL_INCLUDE <SDL.h>
#define FREETYPE_INCLUDE <ft2build.h>
#else
#define SDL_INCLUDE <SDL2/SDL.h>
#define FREETYPE_INCLUDE <freetype2/ft2build.h>
#endif

#endif
