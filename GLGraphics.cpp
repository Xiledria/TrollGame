#ifndef STANDALONE_SERVER

#include <chrono>
#include "Config.h"

#include "GLGraphics.h"
#include "Shaders.h"
#include "Controlls.h"
#include "UIHandler.h"
#include "World.h"
#include "Player.h"
#include "ObjectStore.h"
#include "Unit.h"
#include "Missile.h" // @TODO: maybe missiles could be changed to be stored in Object container so we do not need to include missiles
#include "Terrain.h"
#include "Model.h"

using namespace std::chrono_literals;

GLGraphics::GLGraphics()
{
    fonts = nullptr;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) /* Initialize SDL's Video subsystem */
        SDLDie(const_cast<char*>("Unable to initialize SDL")); /* Or die on error */

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    

    /* Turn on double buffering with a 24bit Z buffer.
    * You may need to change this to 16 or 32 for your system */
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    /* Create our fullscreen window */
    window = SDL_CreateWindow(PROJECT_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        resolution.GetX(), resolution.GetY(), SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN_DESKTOP);
    graphics = this;
    UpdateFullscreen();
    if (!window) /* Die if creation failed */
        SDLDie(const_cast<char*>("Unable to create window"));

    /* Create our opengl context and attach it to our window */
    context = SDL_GL_CreateContext(window);
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        /* Problem: glewInit failed, something is seriously wrong. */
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }
    
    /* This makes our buffer swap syncronized with the monitor's vertical refresh */
    // @TODO: Enable me (make flag  for me?)
    SDL_GL_SetSwapInterval(1);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE); // cull face
    glCullFace(GL_BACK); // cull back

    glGenTextures(1, &mapTexture);
    RecreateMinimap();
    InitializeShaders();
    // initialize size of window for shaders and position of camera
    Resize();
}

GLGraphics::~GLGraphics()
{
    for (auto itr : programs)
        delete itr;
    delete fonts;

    glDeleteTextures(1, &mapTexture);
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
void GLGraphics::Run()
{
    bool quit = false;
    auto now = std::chrono::steady_clock::now();
    auto last = std::chrono::steady_clock::now();
    auto second = std::chrono::steady_clock::now();
    std::chrono::duration<float> diff = now - last;
    int32 fps = 0;
    int32 shownFPS = 0;
    //While application is running
    while (!quit)
    {
        glColor3f(1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear display window
        Graphics::Run();
        if (Config::IsEnabled(Config::showFPS))
            fonts->RenderText((std::to_string(shownFPS) + " FPS").c_str(), 15, resolution.GetY() - 46, false);
        SDL_GL_SwapWindow(window);
        diff = now - last;
        ++fps;
        if (second + 0.5s < std::chrono::steady_clock::now())
        {
            fps *= 2;
            shownFPS = fps;
            second = std::chrono::steady_clock::now();
            fps = 0;
        }
        last = now;
        // handle controlls
        now = std::chrono::steady_clock::now();
        quit = controlls->Update(diff.count());
    }
}

void GLGraphics::UpdateCamera()
{
    Graphics::UpdateCamera();
    for (auto program : programs)
        program->UpdateCameraPosition(-camera->GetX(), -camera->GetY());
}

/* A simple function that prints a message, the error code returned by SDL, and quits the application */
void GLGraphics::SDLDie(char *msg)
{
    printf("%s: %s\n", msg, SDL_GetError());
    SDL_Quit();
    exit(1);
}

GraphicsInitializeErrors GLGraphics::InitializeFonts()
{
    fonts = new FontsGLProgram();
    fonts->UpdateScreenResolution(resolution);
    return fonts->Initialize(24);
}

void GLGraphics::InitializeShaders()
{
    Unit* unitEntry1 = objectStore->GetUnit(1);
    // tiles
    programs.push_back(new TileGLProgram(world->GetGridSize()));
    {
        // selection color
        float color4f[] = { 1.0f, 1.0f, 1.0f, 0.5f };
        programs.push_back(new SquareGLProgram(color4f, unitEntry1->GetSize()));
        // generic square
        programs.push_back(new SquareGLProgram(color4f, unitEntry1->GetSize()));
    }
    // generic rectangle
    programs.push_back(new StaticRectangleGLProgram());
    float color4f[] = { 1.0f, 1.0f, 0.0f, 0.5f };
    // unit selector
    programs.push_back(new RectangleGLProgram(color4f, 5, 5));
    // team color (maybe own program for each team? or keep it this way? :D)
    programs.push_back(new TeamColorGLProgram(unitEntry1->GetSize() - 2));
    // health bar
    programs.push_back(new HealthBarGLProgram(unitEntry1->GetSize()));
    // minimap texture
    programs.push_back(new MinimapGLProgram(mapTexture, minimapSize));
    // minimap objects
    programs.push_back(new MinimapObjectGLProgram(unitEntry1->GetSize(), minimapSize, world->GetMaxX(), world->GetMaxY()));
    // minimap selection
    programs.push_back(new MinimapSelectionGLProgram(minimapSize, uiHeight, world->GetMaxX(), world->GetMaxY()));
    // @TODO: Prepare own shader for each model ID
    programs.push_back(new SquareGLProgram(unitEntry1->GetColor4f(), unitEntry1->GetSize()));
    
    const Model* missile = objectStore->GetModel(1000);
    programs.push_back(new SquareGLProgram(missile->GetColor4f(), missile->GetSize()));
}

GLuint GLGraphics::LoadShader(const char * source, GLuint type)
{
    GLuint id = glCreateShader(type);
    glShaderSource(id, 1, &source, nullptr);
    glCompileShader(id);
    DEBUG
    (
        char error[1000];
        glGetShaderInfoLog(id, 1000, nullptr, error);
        std::cout << "Compile shader status: " << std::endl << error << std::endl;
    );
    return id;
}

void GLGraphics::DrawCircle(int32 posx, int32 posy, int32 radius, const float* color4f)
{
    glColor4fv(color4f);
    uint8 num_segments = 20;
    float theta = 2 * 3.1415926f / float(num_segments);
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = static_cast<float>(radius);//we start at angle = 0
    float y = 0;

    glBegin(GL_LINE_LOOP);
    for (int ii = 0; ii < num_segments; ii++)
    {
        glVertex2f(x + posx, y + posy);//output vertex

        //apply the rotation matrix
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();
}

void GLGraphics::DrawDisc(int32 posx, int32 posy, int32 radius, const float* color4f)
{
    glColor4fv(color4f);
    uint8 num_segments = 20;
    float theta = 2 * 3.1415926f / float(num_segments);
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = static_cast<float>(radius);//we start at angle = 0
    float y = 0;

    glBegin(GL_TRIANGLE_FAN);
    for (int ii = 0; ii < num_segments; ii++)
    {
        glVertex2f(x + posx, y + posy);//output vertex

                                       //apply the rotation matrix
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();
}

void GLGraphics::DrawTile(uint32 x, uint32 y, const float* color3f)
{
    // UseShaderProgramAndBindArray(Shaders::TILE);
    programs[PROGRAM_TILE]->Activate();
    DrawTilePrepared(x, y, color3f);
}

void GLGraphics::DrawSquarePrepared(int32 x, int32 y, int32 radius, const float* color4f)
{
    glUniform2i(SquareGLProgram::SHADER_UNIFORM_POSITION, x, y);
    glUniform1i(SquareGLProgram::SHADER_UNIFORM_SCALE, radius);
    glUniform4fv(SquareGLProgram::SHADER_UNIFORM_COLOR, 1, color4f);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void GLGraphics::DrawSquare(int32 x, int32 y, int32 radius, const float* color4f)
{
    programs[PROGRAM_SQUARE]->Activate();
    DrawSquarePrepared(x, y, radius, color4f);
}

// does not call glProgramUse, requires shader program already used
void GLGraphics::DrawObject(Object* obj)
{
    // @TODO: GetPosition(), move posx posy to pos[2]
    glUniform2i(SquareGLProgram::SHADER_UNIFORM_POSITION, obj->GetX(), obj->GetY());
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
// does not call glProgramUse, requires shader program already used
void GLGraphics::DrawTeamColorPrepared(Unit* unit)
{
    //@TODO: remove this ugly hack! and make it more generic, lol
    if (player->GetId() == unit->GetOwnerId())
        glUniform3fv(TeamColorGLProgram::SHADER_UNIFORM_COLOR, 1, player->GetColor3f());
    else
    {
        float color3f[] = { 0.5f, 0.0f, 0.5f };
        glUniform3fv(TeamColorGLProgram::SHADER_UNIFORM_COLOR, 1, color3f);
    }
    glUniform2i(TeamColorGLProgram::SHADER_UNIFORM_POSITION, unit->GetX(), unit->GetY());
    glDrawArrays(GL_LINES, 0, 4);
}

void GLGraphics::DrawTeamColor(Unit* unit, Vec2<int32>& offset)
{
    programs[PROGRAM_TEAM_COLOR]->Activate();
    DrawTeamColorPrepared(unit);
}

// does not call glProgramUse, requires shader program already used
void GLGraphics::DrawTilePrepared(uint32 x, uint32 y, const float* color3f)
{
    glUniform2i(TileGLProgram::SHADER_UNIFORM_POSITION, x,  y);
    glUniform3fv(TileGLProgram::SHADER_UNIFORM_COLOR, 1, color3f);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

// does not update pctHp & does not activate program
void GLGraphics::DrawUnitHealthPrepared(int32 x, int32 y)
{
    glUniform2i(HealthBarGLProgram::SHADER_UNIFORM_POSITION, x, y);
    glDrawArrays(GL_LINES, 0, 4);
}

// @TODO: remove radius
void GLGraphics::DrawUnitHealth(int32 x, int32 y, int32 /*radius*/, float pctHp)
{
    HealthBarGLProgram* program = static_cast<HealthBarGLProgram*>(programs[PROGRAM_UNIT_HEALTH_BAR]);
    program->Activate();
    program->UpdateHealthPct(pctHp);
    DrawUnitHealthPrepared(x, y);
}

void GLGraphics::DrawUnitSelectionPrepared(int32 x, int32 y, int32 /*radius*/)
{
    glUniform2i(SquareGLProgram::SHADER_UNIFORM_POSITION, x, y);
    glDrawArrays(GL_LINE_LOOP, 0, 4);
}

void GLGraphics::DrawUnitSelection(int32 x, int32 y, int32 radius)
{
    programs[PROGRAM_UNIT_SELECTION]->Activate();
    DrawUnitSelectionPrepared(x, y, radius);
}

void GLGraphics::DrawActualSelecting()
{
    Position* area = controlls->GetSelectionArea();
    programs[PROGRAM_UNIT_SELECTOR]->Activate();
    glUniform2i(TileGLProgram::SHADER_UNIFORM_FRAME_POSITION, area[2].GetX(), area[2].GetY());
    Vec2<int32> size = Vec2<int32>(area[1]) - Vec2<int32>(area[0]);
    glUniform2i(RectangleGLProgram::SHADER_UNIFORM_SCALE, size.GetX(), size.GetY());
    glUniform2i(RectangleGLProgram::SHADER_UNIFORM_POSITION, area[0].GetX(), area[0].GetY());
    glDrawArrays(GL_LINE_LOOP, 0, 4);
}

void GLGraphics::DrawMinimapObjectPrepared(Object* obj)
{
    glUniform2i(MinimapObjectGLProgram::SHADER_UNIFORM_POSITION, obj->GetX(), obj->GetY());
    if (obj->GetObjectType() == TYPEID_UNIT)
    {
        //@TODO: remove this ugly hack! and make it more generic, lol
        if (player->GetId() == static_cast<Unit*>(obj)->GetOwnerId())
            glUniform3fv(MinimapObjectGLProgram::SHADER_UNIFORM_COLOR, 1, player->GetColor3f());
        else
        {
            float color3[] = { 0.5f, 0.0f, 0.5f };
            glUniform3fv(MinimapObjectGLProgram::SHADER_UNIFORM_COLOR, 1, color3);
        }
    }
    else
        glUniform3fv(MinimapObjectGLProgram::SHADER_UNIFORM_COLOR, 1, obj->GetColor4f());
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void GLGraphics::DrawMinimap(UIObject* object)
{
    //@TODO: change hardcoded values to real object position
    programs[PROGRAM_MINIMAP]->Activate();
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    uint32 index = 0;
    programs[PROGRAM_MINIMAP_OBJECT]->Activate();
    std::vector<Object*>& objects = world->_objects;
    while (Object* obj = objects[index++])
        DrawMinimapObjectPrepared(obj);

    programs[PROGRAM_MINIMAP_SELECTION]->Activate();
    glDrawArrays(GL_LINE_LOOP, 0, 4);
}

void GLGraphics::DrawWorld(uint32 startX, uint32 endX, uint32 startY, uint32 endY, UIObject* object)
{
    programs[PROGRAM_TILE]->Activate();
    auto offset = object->GetStart();
    glUniform2i(TileGLProgram::SHADER_UNIFORM_FRAME_POSITION, offset.GetX(), offset.GetY());
    std::vector<Terrain*>& terrain = world->GetTerrain();
    for (uint32 x = startX; x < endX; ++x)
    {
        for (uint32 y = startY; y < endY; ++y)
        {
            Terrain* t = terrain[x + world->GetGridMaxX() * y];
            // @TODO: fix me in shader
            DrawTilePrepared(x, y, t->GetColor3f());
        }
    }
}

void GLGraphics::DrawVisibleObjects(UIObject* object)
{
    Vec2<int32> start, end;
    start = *camera + object->GetCenter() - object->GetSize();
    end = *camera + object->GetCenter() + object->GetSize();
    std::vector<Object*> objects;
    Position pstart(start.GetX(), start.GetY()), pend(end.GetX(), end.GetY());
    world->GetObjectsInArea(&pstart, &pend, objects);
    auto offset = object->GetStart();

    programs[PROGRAM_UNIT_ENTRY_1]->Activate();
    glUniform2i(TileGLProgram::SHADER_UNIFORM_FRAME_POSITION, offset.GetX(), offset.GetY());
    for (auto object : objects)
        DrawObject(object);

    programs[PROGRAM_TEAM_COLOR]->Activate();
    glUniform2i(TileGLProgram::SHADER_UNIFORM_FRAME_POSITION, offset.GetX(), offset.GetY());
    for (auto object : objects)
        if (object->GetObjectType() == TYPEID_UNIT)
            DrawTeamColorPrepared(static_cast<Unit*>(object));
    
    programs[PROGRAM_UNIT_SELECTION]->Activate();
    glUniform2i(TileGLProgram::SHADER_UNIFORM_FRAME_POSITION, offset.GetX(), offset.GetY());
    for (auto i : player->GetSelected())
        DrawUnitSelectionPrepared(i->GetX(), i->GetY(), i->GetSize());

    HealthBarGLProgram* hpProgram = static_cast<HealthBarGLProgram*>(programs[PROGRAM_UNIT_HEALTH_BAR]);
    hpProgram->Activate();
    glUniform2i(TileGLProgram::SHADER_UNIFORM_FRAME_POSITION, offset.GetX(), offset.GetY());
    for (auto object : objects)
    {
        if (object->GetObjectType() == TYPEID_UNIT)
        {
            hpProgram->UpdateHealthPct(static_cast<Unit*>(object)->GetHealthPct());
            DrawUnitHealthPrepared(object->GetX(), object->GetY());
        }
    }

    programs[PROGRAM_MISSILE_MODEL]->Activate();
    glUniform2i(TileGLProgram::SHADER_UNIFORM_FRAME_POSITION, offset.GetX(), offset.GetY());
    std::vector<Missile*>& missiles = world->GetMissiles();
    uint32 index = 0;
    while (Missile* m = missiles[index++])
        DrawObject(m);
    
    DrawActualSelecting();
}

void GLGraphics::DrawUIObject(UIObject* obj)
{
    if (!obj->IsVisible())
        return;

    programs[PROGRAM_STATIC_RECTANGLE]->Activate();
    glUniform2i(StaticRectangleGLProgram::SHADER_UNIFORM_SCALE, obj->GetSize().GetX(), obj->GetSize().GetY());
    glUniform2i(StaticRectangleGLProgram::SHADER_UNIFORM_POSITION, obj->GetStart().GetX(), obj->GetStart().GetY());
    glUniform3fv(StaticRectangleGLProgram::SHADER_UNIFORM_COLOR, 1, obj->GetColor3f());
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    if (!obj->GetText().empty())
        fonts->RenderText(obj->GetText().c_str(), obj->GetCenter().GetX(), obj->GetCenter().GetY(), true);

    Graphics::DrawUIObject(obj);
}

void GLGraphics::DrawUI()
{
    Graphics::DrawUI();
}

void GLGraphics::DrawEditor()
{
    int32 camX = camera->GetX();
    int32 camY = camera->GetY();
    int32 gridSize = world->GetGridSize();
    uint32 i = 0;
    const float voidColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    const float grassColor[] = { 0.30f, 0.74f, 0.20f, 1.0f };
    const float dirtColor[] = { 0.38f, 0.20f, 0.07f, 1.0f };
    const float waterColor[] = { 0.31f, 0.47f, 0.63f, 1.0f };
    DrawSquare(camX + ++i * gridSize, camY + gridSize, gridSize / 2, voidColor);  // void
    DrawSquare(camX + ++i * gridSize, camY + gridSize, gridSize / 2, grassColor); // grass
    DrawSquare(camX + ++i * gridSize, camY + gridSize, gridSize / 2, dirtColor);  // dirt
    DrawSquare(camX + ++i * gridSize, camY + gridSize, gridSize / 2, waterColor); // water
}

void GLGraphics::Resize()
{
    int w,h;
    SDL_GetWindowSize(window, (int*)(&w), (int*)(&h));
    resolution.SetCoords(w, h);

    glViewport(0, 0, w, h);

    for (auto shader : programs)
        shader->UpdateScreenResolution(resolution);

    if (fonts)
        fonts->UpdateScreenResolution(resolution);

    Graphics::Resize();
}

void GLGraphics::HandleWorldResize()
{
    RecreateMinimap();
    // @TODO: maybe function in shaderprogram to handle this globally? :D
    glProgramUniform2i(programs[PROGRAM_MINIMAP_OBJECT]->GetId(), MinimapObjectGLProgram::SHADER_UNIFORM_WORLD_SIZE, world->GetMaxX(), world->GetMaxY());
    glProgramUniform2i(programs[PROGRAM_MINIMAP_SELECTION]->GetId(), MinimapSelectionGLProgram::SHADER_UNIFORM_WORLD_SIZE, world->GetMaxX(), world->GetMaxY());
}

void GLGraphics::RecreateMinimap()
{
    // prepare minimap texture
    uint32 maxX = world->GetGridMaxX();
    uint32 maxY = world->GetGridMaxY();
    float* minimapBitmap = new float[maxX * maxY * 3];
    std::vector<Terrain*>& terrain = world->GetTerrain();
    for (uint32 x = 0; x < maxX; ++x)
    {
        for (uint32 y = 0; y < maxY; ++y)
        {
            Terrain* t = terrain[x + maxX * y];
            memcpy(&minimapBitmap[(x + y * maxX) * 3], t->GetColor3f(), sizeof(float) * 3);
        }
    }

    glBindTexture(GL_TEXTURE_2D, mapTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, maxX, maxY, 0, GL_RGB, GL_FLOAT, minimapBitmap);
    delete[] minimapBitmap;
}

void GLGraphics::UpdateVSync()
{
    SDL_GL_SetSwapInterval(Config::vsync ? 1 : 0);
}

void GLGraphics::UpdateFullscreen()
{
    if (Config::IsEnabled(Config::fullscreen))
    {
        SDL_Rect rect;
        SDL_GetDisplayBounds(0, &rect);
        resolution.SetCoords(rect.w, rect.h);
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    }
    else
    {
        SDL_Rect rect;
        SDL_GetDisplayBounds(0, &rect);
        resolution.SetCoords(rect.w - 50, rect.h - 50);
        SDL_SetWindowFullscreen(window, 0);
        SDL_SetWindowSize(window, resolution.GetX(), resolution.GetY());
        SDL_SetWindowPosition(window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    }
    Resize();
}

#endif
