#ifndef STANDALONE_SERVER
#ifndef __GL_PROGRAM_H
#define __GL_PROGRAM_H

#include <GL/glew.h>
#include <vector>
#include "SharedDefs.h"
#include "Vec2.h"
#include FREETYPE_INCLUDE
#include FT_FREETYPE_H

// MUST BE SHARED BY ALL SHADERS!
enum ShaderAttribLocation
{
    SHADER_ATTRIB_POSITION,
};

#define USE_SHARED_ENUM SHADER_UNIFORM_SCREEN_SIZE, SHADER_UNIFORM_CAMERA, SHADER_UNIFORM_FRAME_POSITION
// MUST BE SHARED BY ALL SHADERS IF THEY USE CAMERA AND SCREEN SIZE
enum ShaderUniformLocation
{
    USE_SHARED_ENUM
};


class GLProgram
{
public:
    GLProgram();
    virtual ~GLProgram();
    void Activate();
    uint32 GetId();
    virtual void UpdateScreenResolution(Vec2<uint32> newRes);
    virtual void UpdateCameraPosition(int32 x, int32 y);
protected:
    GLuint LoadShader(const char * source, GLuint type);
    void PrepareBuffer(GLuint& buffer, GLsizeiptr size, const void* data, bool dynamic = false);
    void PrepareShader(bool dynamic = false);
    const char* vsSource;
    const char* fsSource;

    GLuint vs;
    GLuint fs;
    GLuint program;
    GLuint VBO;
    GLuint VAO;
    std::vector<GLfloat> positionData;
};

class SquareGLProgram : public GLProgram
{
public:
    SquareGLProgram(const float* color4f, int32 scale);
    enum Uniform
    {
        USE_SHARED_ENUM,
        SHADER_UNIFORM_POSITION,
        SHADER_UNIFORM_SCALE,
        SHADER_UNIFORM_COLOR,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
    };
};

class TeamColorGLProgram : public GLProgram
{
public:
    TeamColorGLProgram(int32 scale);
    enum Uniform
    {
        USE_SHARED_ENUM,
        SHADER_UNIFORM_POSITION,
        SHADER_UNIFORM_SCALE,
        SHADER_UNIFORM_COLOR,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
    };
};

class RectangleGLProgram : public GLProgram
{
public:
    RectangleGLProgram(const float* color4f, int32 scaleX, int32 scaleY);
    enum Uniform
    {
        USE_SHARED_ENUM,
        SHADER_UNIFORM_POSITION,
        SHADER_UNIFORM_SCALE,
        SHADER_UNIFORM_COLOR,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
    };
};

class StaticRectangleGLProgram : public GLProgram
{
public:
    StaticRectangleGLProgram();
    void UpdateCameraPosition(int32 /*x*/, int32 /*y*/) override { } // UI ignores camera position, still the same UI
    enum Uniform
    {
        SHADER_UNIFORM_RESOLUTION,
        SHADER_UNIFORM_POSITION,
        SHADER_UNIFORM_SCALE,
        SHADER_UNIFORM_COLOR,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
    };
};

class TileGLProgram : public GLProgram
{
public:
    TileGLProgram(uint32 gridSize);
    enum Uniform
    {
        USE_SHARED_ENUM,
        SHADER_UNIFORM_POSITION,
        SHADER_UNIFORM_SCALE,
        SHADER_UNIFORM_COLOR,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
    };
};

class HealthBarGLProgram : public GLProgram
{
public:
    HealthBarGLProgram(int32 scale);
    ~HealthBarGLProgram();
    void UpdateHealthPct(float pct);
    enum Uniform
    {
        USE_SHARED_ENUM,
        SHADER_UNIFORM_POSITION,
        SHADER_UNIFORM_SCALE,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
        SHADER_ATTRIB_COLOR,
    };

    GLuint colorVBO;
    std::vector<GLfloat> colorData;
};

class MinimapGLProgram : public GLProgram
{
public:
    MinimapGLProgram(GLuint texture, uint32 minimapSize);
    ~MinimapGLProgram();
    void UpdateCameraPosition(int32 /*x*/, int32 /*y*/) override { } // UI ignores camera position, still the same UI
    enum Uniform
    {
        SHADER_UNIFORM_SCREEN_SIZE,
        SHADER_UNIFORM_TEXTURE,
        SHADER_UNIFORM_SCALE,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
        SHADER_ATTRIB_TEXTURE_POSITION,
    };
    GLuint texVBO;
    std::vector<GLfloat> texCoords;
};

class MinimapObjectGLProgram : public GLProgram
{
public:
    MinimapObjectGLProgram(int32 scale, uint32 minimapSize, uint32 worldSizeX, uint32 worldSizeY);
    void UpdateCameraPosition(int32 /*x*/, int32 /*y*/) override { } // UI ignores camera position, still the same UI
    enum Uniform
    {
        SHADER_UNIFORM_RESOLUTION,
        SHADER_UNIFORM_POSITION,
        SHADER_UNIFORM_SCALE,
        SHADER_UNIFORM_MINIMAP_SIZE,
        SHADER_UNIFORM_WORLD_SIZE,
        SHADER_UNIFORM_COLOR,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
    };
};

class MinimapSelectionGLProgram : public GLProgram
{
public:
    MinimapSelectionGLProgram(uint32 minimapSize, uint32 uiHeight, uint32 worldSizeX, uint32 worldSizeY);
    void UpdateCameraPosition(int32 x, int32 y) override
    {
        glProgramUniform2i(program, SHADER_UNIFORM_CAMERA, -x, -y);
    }
    enum Uniform
    {
        SHADER_UNIFORM_RESOLUTION,
        SHADER_UNIFORM_CAMERA,
        SHADER_UNIFORM_MINIMAP_SIZE,
        SHADER_UNIFORM_WORLD_SIZE,
        SHADER_UNIFORM_UI_HEIGHT,
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
    };
};

class FontsGLProgram : public GLProgram
{
public:
    FontsGLProgram();
    ~FontsGLProgram();
    GraphicsInitializeErrors Initialize(int32 height);
    void UpdateCameraPosition(int32 /*x*/, int32 /*y*/) override { } // UI ignores camera position, still the same UI
    void RenderText(const char* text, int32 x, int32 y, bool centered);
#define TEXTURE_UNIT 1
    enum Uniform
    {
        SHADER_UNIFORM_RESOLUTION,
        SHADER_UNIFORM_TEXTURE,
        SHADER_UNIFORM_POSITION,
        // SHADER_UNIFORM_CAMERA, @TODO: dynamic fonts in world
    };
    enum Attrib
    {
        SHADER_ATTRIB_POSITION,
        SHADER_ATTRIB_TEX_COORDS,
    };
private:
    FT_Library ft;
    FT_Face face;
    // width for text texture
#define MAXWIDTH 1024
    GLuint textTexture;
    ft_int w;    // width of texture in pixels
    ft_int h;    // height of texture in pixels
    struct
    {
        float ax;    // advance.x
        float ay;    // advance.y

        float bw;    // bitmap.width;
        float bh;    // bitmap.height;

        float bl;    // bitmap_left;
        float bt;    // bitmap_top;

        float tx;    // x offset of glyph in texture coordinates
        float ty;    // y offset of glyph in texture coordinates
    } c[128];        // character information

    struct point
    {
        GLfloat x;
        GLfloat y;
        GLfloat s;
        GLfloat t;
    };
};

#endif
#endif
