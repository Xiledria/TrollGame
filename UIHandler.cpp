﻿#ifndef STANDALONE_SERVER
#include "UIHandler.h"
#include "UIObject.h"
#include "UIFunctions.h"
#include "Graphics.h"
#include "Config.h"
#include <pugixml.hpp>
#include <cstring>

//@TODO: initialiser list
UIHandler::UIHandler()
{
    LoadUI();
    MoveToMenu(MAIN_MENU);
    focusedUIObject = nullptr;
}

bool UIHandler::LoadBasicObjectAttributes(pugi::xml_node* node, float* color3f, Vec2<int32>& size, Vec2<float>& pctSize, Vec2<int32>& pos, Vec2<float>& pctPos, int32 menuId)
{
    pugi::xml_node xmlpos = node->child("position");
    pos.SetX(xmlpos.attribute("X").as_int(0));
    pos.SetY(xmlpos.attribute("Y").as_int(0));
    pctPos.SetX(xmlpos.attribute("pctX").as_float(0.0f));
    pctPos.SetY(xmlpos.attribute("pctY").as_float(0.0f));

    pugi::xml_node xmlsize = node->child("size");
    size.SetX(xmlsize.attribute("X").as_int(0));
    size.SetY(xmlsize.attribute("Y").as_int(0));
    pctSize.SetX(xmlsize.attribute("pctX").as_float(0.0f));
    pctSize.SetY(xmlsize.attribute("pctY").as_float(0.0f));

    if ((!size.GetX() && !pctSize.GetX()) || (!size.GetY() && !pctSize.GetY()))
    {
        DEBUG(std::cout << "Invalid size specified in UI.xml for menu ID " << menuId << ". Skipping!" << std::endl);
        return false;
    }

    pugi::xml_node color = node->child("color");
    color3f[0] = color.attribute("R").as_float(-1.0f);
    color3f[1] = color.attribute("G").as_float(-1.0f);
    color3f[2] = color.attribute("B").as_float(-1.0f);
    if (color3f[0] < 0.0f || color3f[1] < 0.0f || color3f[2] < 0.0f)
    {
        DEBUG(std::cout << "Invalid color specified in UI.xml for menu ID " << menuId << ". Skipping!" << std::endl);
        return false;
    }
    return true;
}

FunctionPtr* GetFunctionPtr(pugi::xml_node* node, UIObject* object, bool* isNodeEmpty = nullptr, const char* defaultFn = nullptr, bool defaultMember = false)
{
    if (!node->empty())
    {
        if (isNodeEmpty)
            *isNodeEmpty = false;
        bool memberFn = node->attribute("member").as_bool();
        std::string fnName = node->attribute("function").as_string();
        pugi::xml_attribute p1 = node->attribute("param1");
        if (!p1.empty())
        {
            int32 param1val = p1.as_int();
            pugi::xml_attribute p2 = node->attribute("param2");
            if (!p2.empty())
            {
                std::string param2val = p2.as_string();
                pugi::xml_attribute p3 = node->attribute("param3");
                if (!p3.empty())
                {
                    std::string param3val = p3.as_string();
                    auto fn = UIFunctions::GetMemberThreeParamFnByName(fnName);
                    return fn ? new MemberThreeParamFunction(fn, object, param1val, param2val, param3val) : new FunctionPtr();
                }
                auto fn = UIFunctions::GetMemberTwoParamFnByName(fnName);
                return fn ? new MemberTwoParamFunction(fn, object, param1val, param2val) : new FunctionPtr();
            }
            if (!memberFn)
            {
                auto fn = UIFunctions::GetOneParamFnByName(fnName);
                return fn ? new OneParamFunction(fn, param1val) : new FunctionPtr();
            }
            auto fn = UIFunctions::GetMemberOneParamFnByName(fnName);
            return fn ? new MemberOneParamFunction(fn, object, param1val) : new FunctionPtr();
        }
        if (memberFn)
        {
            auto fn = UIFunctions::GetMemberNoParamFnByName(fnName);
            if (fn)
            {
                return new MemberNoParamFunction(fn, object);
            }
            return fn ? new MemberNoParamFunction(fn, object) : new FunctionPtr();
        }
        auto fn = UIFunctions::GetNoParamFnByName(fnName);
        return fn ? new NoParamFunction(fn) : new FunctionPtr();
    }

    if (defaultFn)
    {
        if (defaultMember)
        {
            auto fn = UIFunctions::GetMemberNoParamFnByName(defaultFn);
            return fn ? new MemberNoParamFunction(fn, object) : new FunctionPtr();
        }
        else
        {
            auto fn = UIFunctions::GetNoParamFnByName(defaultFn);
            return fn ? new NoParamFunction(fn) : new FunctionPtr();
        }
    }

    return new FunctionPtr();
}

void UIHandler::LoadChildrens(pugi::xml_node& parentNode, UIObject* parentObject, int32 menuId)
{
    float color3f[3];
    Vec2<int32> size;
    Vec2<int32> pos;
    Vec2<float> pctSize;
    Vec2<float> pctPos;
    for (pugi::xml_node menuObject : parentNode.child("childs").children())
    {
        if (!LoadBasicObjectAttributes(&menuObject, color3f, size, pctSize, pos, pctPos, menuId))
            continue;

        bool clickable = false;
        std::string text = menuObject.attribute("text").as_string("");

        UIObject* obj;
        if (!strcmp(menuObject.name(), "Frame") || !strcmp(menuObject.name(), "Label"))
        {
            obj = new UIObject(pos, pctPos, size, color3f, text, pctSize);
        }
        else if (!strcmp(menuObject.name(), "EditBox"))
        {
            clickable = true;
            pugi::xml_node activeColor = menuObject.child("ActiveColor");
            float activeColor3f[] =
            {
                activeColor.attribute("R").as_float(-1.0f),
                activeColor.attribute("G").as_float(-1.0f),
                activeColor.attribute("B").as_float(-1.0f)
            };

            obj = new EditBox(pos, pctPos, size, color3f, activeColor3f, text, menuObject.attribute("EditMode").as_int(EditBox::Flags::ACCEPT_ALL), pctSize);
            pugi::xml_node OnVisCheck = menuObject.child("OnEdit");
            FunctionPtr* fn = GetFunctionPtr(&OnVisCheck, obj);
            reinterpret_cast<EditBox*>(obj)->SetOnChangeFunction(fn);
        }
        else
        {
            DEBUG(std::cout << "Invalid object type: \"" << menuObject.name() << "\" in menu id " << menuId << ". Skipping!" << std::endl);
            continue;
        }

        LoadFunctionHandlers(menuObject, obj, clickable ? nullptr : &clickable);
        parentObject->AddChild(obj, clickable);
        LoadChildrens(menuObject, obj, menuId);
    }
}

void UIHandler::LoadFunctionHandlers(pugi::xml_node& node, UIObject* object, bool* clickable)
{
    bool notClickable = true;
    pugi::xml_node child = node.child("OnLeftClick");
    FunctionPtr* fn = GetFunctionPtr(&child, object, &notClickable);
    object->SetOnClickFn(fn, MouseType::LEFT_BUTTON);

    child = node.child("OnLeftDrag");
    fn = GetFunctionPtr(&child, object, &notClickable);
    object->SetOnDragFn(fn, MouseType::LEFT_BUTTON);

    child = node.child("OnMiddleClick");
    fn = GetFunctionPtr(&child, object, &notClickable);
    object->SetOnClickFn(fn, MouseType::MIDDLE_BUTTON);

    child = node.child("OnMiddleDrag");
    fn = GetFunctionPtr(&child, object, &notClickable);
    object->SetOnDragFn(fn, MouseType::MIDDLE_BUTTON);

    child = node.child("OnRightClick");
    fn = GetFunctionPtr(&child, object, &notClickable);
    object->SetOnClickFn(fn, MouseType::RIGHT_BUTTON);

    child = node.child("OnRightDrag");
    fn = GetFunctionPtr(&child, object, &notClickable);
    object->SetOnDragFn(fn, MouseType::RIGHT_BUTTON);

    child = node.child("OnVisibilityCheck");
    fn = GetFunctionPtr(&child, object);
    object->SetOnVisibilityCheckFn(fn);

    child = node.child("OnDraw");
    fn = GetFunctionPtr(&child, object, nullptr, "DefaultRender", true);
    object->SetOnDrawFn(fn);

    if (clickable)
        *clickable = !notClickable;
}

void UIHandler::LoadUI()
{
    pugi::xml_document doc;
    doc.load_file("UI.xml");
    for (pugi::xml_node menu : doc.children())
    {
        if (strcmp(menu.name(), "menu"))
        {
            DEBUG(std::cout << "Invalid parent entry found - " << menu.name() << std::endl);
            continue;
        }

        int32 menuId = menu.attribute("id").as_int(-1);
        if (menuId != -1)
        {
            if (UIParents.find(menuId) != UIParents.end())
            {
                DEBUG(std::cout << "Menu with ID " << menuId << " is specified twice, skipping!" << std::endl);
                continue;
            }

            float color3f[3];
            Vec2<int32> size;
            Vec2<int32> pos;
            Vec2<float> pctSize;
            Vec2<float> pctPos;
            if (!LoadBasicObjectAttributes(&menu, color3f, size, pctSize, pos, pctPos, menuId))
                continue;

            UIObject* parentMenu = new UIObject(pos, pctPos, size, color3f, "", pctSize);
            UIParents[menuId] = parentMenu;

            LoadFunctionHandlers(menu, parentMenu);
            LoadChildrens(menu, parentMenu, menuId);
        }
        else
        {
            DEBUG(std::cout << "No Menu ID found!");
        }
    }
}

UIHandler::~UIHandler()
{

}

EditBox* UIHandler::GetFocusedObject()
{
    return focusedUIObject;
}

void UIHandler::MoveToMenu(Menu menu)
{
    if (UIParents.find(menu) == UIParents.end())
    {
        DEBUG(std::cout << "Invalid menu ID " << menu << " specified in MoveToMenu!" << std::endl);
        visibleUIParent = UIParents[0];
        return;
    }
    if (menu != INGAME_MENU)
        Config::Set(Config::paused);
    else
        Config::Clear(Config::paused);
    visibleUIParent = UIParents[menu];
    visibleUIParent->OnVisibilityCheck();
}

UIObject* UIHandler::GetVisibleParentObject()
{
    return visibleUIParent;
}

void UIHandler::SetFocusedUIObject(EditBox* obj)
{
    if (focusedUIObject)
        focusedUIObject->SetActive(false);
    focusedUIObject = obj;
    if (obj)
        obj->SetActive(true);
}

#endif
