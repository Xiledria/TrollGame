#include "Model.h"

#define SIZE_CORRECTION 0.002f

                                                                         // @TODO: fix size maybe? to correlate with size of terrain and remove / 2.0f
Model::Model(float* color4f, float size, uint32 gridSize, uint32 id) : _color4f{color4f[0], color4f[1], color4f[2], color4f[3] },
_size(static_cast<uint32>(size * static_cast<float>(gridSize) / 2.0f)), _id(id)
{
}

Model::~Model()
{
    // called automatically upon vector deletion
    // vertices.clear();
}

const int32 Model::GetSize() const
{
    return _size;
}

const uint32 Model::GetId() const
{
    return _id;
}

const float* Model::GetColor4f() const
{
    return _color4f;
}

const std::vector<float>& Model::GetVertices() const
{
    return _vertices;
}
