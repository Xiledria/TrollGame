#ifndef __MODEL_H
#define __MODEL_H

#include "SharedDefs.h"
#include <vector>

class Model
{
public:
    const int32 GetSize() const;
    const uint32 GetId() const;
    const float* GetColor4f() const;
    const std::vector<float>& GetVertices() const;
    Model(float* color4f, float size, uint32 gridSize, uint32 id);
    ~Model();
private:
    const float _color4f[4];
    const int32 _size;
    const uint32 _id;
    const std::vector<float> _vertices;
};

#endif
