#include "ObjectStore.h"
#include "Unit.h"
#include "Model.h"
#include "World.h"
#include "pugixml.hpp"
#include <string>

ObjectStore::ObjectStore()
{
    float model0[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
    int modelId = 0;
    uint32 grid = world->GetGridSize();
    _models[modelId] = new Model(model0, 0.0f, grid, modelId);
    ++modelId;
    float model1[4] = { 0.0f, 0.0f, 1.0f, 0.9f };
    _models[modelId] = new Model(model1, 1.0f, grid, modelId);

    float model1000[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
    _models[1000] = new Model(model1000, 0.1f, grid, 1000);
    LoadUnits();
}

ObjectStore::~ObjectStore()
{
    for (auto itr : _units)
        delete itr.second;
    // units.clear();
    for (auto itr : _models)
        delete itr.second;
    // models.clear();
}
/*
void ObjectStore::LoadUnits()
{
    std::string line;

    std::ifstream data("units.dat", std::ios::binary);
    if (data.is_open())
    {
#define READ(x) data.read(reinterpret_cast<char*>(&x), sizeof(x))
        uint32 unitsCount; // count of units
        READ(unitsCount);
        while (unitsCount--)
        {
            Unit* unit = new Unit;
            READ(unit->_moveSpeed);// = 200;
            READ(unit->_maxHealth);// = 100.0f;
            READ(unit->_health);// = 1.0f;
            READ(unit->_healthRegen);// = 5.0f;
            READ(unit->_attackRange);
            READ(unit->_attackSpeed);
            READ(unit->_maximalAttack);
            READ(unit->_minimalAttack);
            READ(unit->_armor);
            READ(unit->_missileModelId);
            READ(unit->_missileSpeed);
            uint32 modelId;
            READ(modelId);
            unit->_model = _models[modelId];
            unit->_type = TYPEID_UNIT;
            READ(unit->_moveFlags);
            READ(unit->_entry);// = 1;
            _units[unit->_entry] = unit;
        }
    }
    data.close();
}
*/

template <typename T>
void LoadValue(pugi::xml_node& node, const char* attribute, T& result)
{
	result = (T)node.attribute(attribute).as_int(-1);
}

template <typename T>
bool isEmpty(T& result)
{
	return result == -1;
}

bool isEmpty(std::string result)
{
    return result.empty();
}

template <typename T>
bool TryLoad(pugi::xml_node& node, const char* attribute, T& result)
{
	LoadValue(node, attribute, result);
	if (isEmpty(result))
	{
		DEBUG(std::cout << "Failed to load " << attribute << std::endl);
		return false;
	}
	return true;
}


void ObjectStore::LoadUnits()
{
    pugi::xml_document doc;
    doc.load_file("Unit.xml");
    for (pugi::xml_node node : doc.children())
    {
        if (strcmp(node.name(), "unit"))
        {
            DEBUG(std::cout << "Invalid parent entry found - " << node.name() << std::endl);
            continue;
        }

        int32 unitId = node.attribute("id").as_int(-1);
        if (unitId != -1)
        {
            if (_units.find(unitId) != _units.end())
            {
                DEBUG(std::cout << "Unit with ID " << unitId << " is specified twice, skipping!" << std::endl);
                continue;
            }
            /*
    unit->_moveSpeed = 200;
    unit->_maxHealth = 100.0f;
    unit->_health = 1.0f;
    unit->_healthRegen = 5.0f;
    unit->_model = _models[1];
    unit->_type = TYPEID_UNIT;
    unit->_moveFlags = MOVEFLAG_WALKING;
    unit->_entry = 1;
    unit->_attackRange = 100;
    unit->_searchRadius = 300;
    unit->_attackSpeed = 1.0f;
    unit->_maximalAttack = 20;
    unit->_minimalAttack = 12;
    unit->_missileModelId = 1000;
    unit->_missileSpeed = 800;
    unit->_armor = 5;
    */
            Unit* unit = new Unit;
            uint32 modelId = 0;
            if (TryLoad(node, "moveSpeed", unit->_moveSpeed) &&
                TryLoad(node, "maxHealth", unit->_maxHealth) &&
                TryLoad(node, "healthRegen", unit->_healthRegen) &&
                TryLoad(node, "modelId", modelId) &&
                TryLoad(node, "moveFlags", unit->_moveFlags) &&
                TryLoad(node, "attackRange", unit->_attackRange) &&
                TryLoad(node, "searchRadius", unit->_searchRadius) &&
                TryLoad(node, "attackSpeed", unit->_attackSpeed) &&
                TryLoad(node, "maxAttack", unit->_maximalAttack) &&
                TryLoad(node, "minAttack", unit->_minimalAttack) &&
                TryLoad(node, "missileModelId", unit->_missileModelId) &&
                TryLoad(node, "missileSpeed", unit->_missileSpeed) &&
                TryLoad(node, "armor", unit->_armor))
            {
                unit->_model = _models[modelId];
                unit->_entry = unitId;
                _units[unitId] = unit;
            }
            else
            {
                DEBUG(std::cout << "Failed to load Unit ID" << unitId << std::endl);
            }
        }
        else
        {
            DEBUG(std::cout << "No Unit ID found!");
        }
    }
}
const Model* ObjectStore::GetModel(uint32 modelID)
{
    return _models[modelID];
}

Unit* ObjectStore::GetUnit(uint32 entry)
{
    return _units[entry];
}
