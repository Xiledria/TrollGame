#ifndef __OBJECT_STORE_H
#define __OBJECT_STORE_H

#include "SharedDefs.h"
#include <unordered_map>
class Model;
class Unit;

class ObjectStore
{
public:
    ObjectStore();
    ~ObjectStore();
    void LoadUnits();
    const Model* GetModel(uint32 modelID);
    Unit* GetUnit(uint32 unitID);
private:
    enum SavedType
    {
        TYPE_NONE,
        TYPE_FLOAT,
        TYPE_INT,
    };
    std::unordered_map<uint32, Model*> _models;
    std::unordered_map<uint32, Unit*> _units;
};

extern ObjectStore* objectStore;

#endif
