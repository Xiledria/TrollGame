#include <fstream>
#include <cstring>
#include "World.h"
#include "Terrain.h"
#include "Unit.h"
#include "Missile.h"
#include "Searcher.h"
#include "Config.h"

using namespace std::chrono_literals;
World::World()
{
    Init();
    Config::Set(Config::paused);
    _terrainVec.resize(1, nullptr);
    _objectsMap.resize(1, nullptr);
    _objects.resize(2, nullptr);
    _missiles.resize(2, nullptr);
    _pathfinderThread = new std::thread([this](){ GeneratePaths(); });
}

void World::CreateNewWorld(std::string name, uint32 x, uint32 y)
{
    _worldName = name;

    std::vector<Terrain*> tvec;
    tvec.reserve(x * y);
    for (uint32 i = 0; i < x * y; ++i)
        tvec.push_back(new Terrain(TERRAIN_GRASS));

    std::swap(tvec, _terrainVec);
    std::vector<Object*> omap(x * y, nullptr);
    std::swap(omap, _objectsMap);
    std::vector<Object*> ovec(2, nullptr);
    std::swap(ovec, _objects);
    std::vector<Missile*> mvec(2, nullptr);
    std::swap(mvec, _missiles);
    _gridMaxX = x;
    _gridMaxY = y;
    Config::Set(Config::editable | Config::world_initialized);
}

// @TODO: if loading map after map is loaded, erase its content
void World::LoadWorld(std::string worldMap)
{
    _worldName = worldMap;
    Config::Set(Config::paused);
    Config::Clear(Config::editable);
    std::string line;
    std::ifstream myfile(worldMap.c_str(), std::ios::binary);
    if (myfile.is_open())
    {
        // after removing object, another one moves to objects[0]
        while (auto itr = _objects[0])
            RemoveObject(itr);

        for (auto itr : _terrainVec)
            delete itr;

        // after removing object, another one moves to missiles[0]
        while (auto itr = _missiles[0])
            RemoveMissile(itr);

#define READ(x) myfile.read(reinterpret_cast<char*>(&x), sizeof(x))
        uint32 maxX;
        READ(maxX);
        uint32 maxY;
        READ(maxY);

        std::vector<Terrain*> tvec(maxX * maxY, nullptr);
        std::swap(tvec, _terrainVec);
        std::vector<Object*> omap(maxX * maxY, nullptr);
        std::swap(omap, _objectsMap);
        std::vector<Object*> ovec(2, nullptr);
        std::swap(ovec, _objects);
        std::vector<Missile*> mvec(2, nullptr);
        std::swap(mvec, _missiles);
        _gridMaxX = maxX;
        _gridMaxY = maxY;
        for (uint32 i = 0; i < maxX * maxY; ++i)
        {
            TerrainType type;
            READ(type);
            _terrainVec[i] = new Terrain(type);
        }
        myfile.close();
    }

    // debug
    for (uint32 x = 420; x < 620; x += 40)
        for (uint32 y = 420; y < 620; y += 40)
        {
            Unit* unit = new Unit(1);
            unit->SetOwnerId(1);
            unit->SetCoords(x, y);
            AddObject(unit);
            RelocateObject(unit, x, y);
            unit->SetHealth(unit->GetMaxHealth());

            Unit* unit2 = new Unit(1);
            unit2->SetOwnerId(0);

            unit2->SetCoords(world->GetMaxX() - 20 - x, world->GetMaxY() - 20 - y);
            AddObject(unit2);
            RelocateObject(unit2, world->GetMaxX() - 20 - x, world->GetMaxY() - 20 - y);
            //unit2->MoveTo(unit);
            //unit->MoveTo(unit2);
            unit2->SetHealth(unit2->GetMaxHealth());
        }

    Config::Set(Config::world_initialized);
}

void World::Init()
{
    Config::Clear(Config::editable);
    _gridMaxX = 0;
    _gridMaxY = 0;
    _objectIdCounter = 0;
    _playerIdCounter = 0;
    _gridSize = 40;
    _objectsCount = 0;
    _missilesCount = 0;
    _deleting = false;
}

World::~World()
{
    _deleting = true;
    _pathfinderThread->join();
    delete _pathfinderThread;

    uint32 index = 0;
    while (auto itr = _objects[index++])
        delete itr;

    for (auto itr : _terrainVec)
        delete itr;

    index = 0;
    while (auto itr = _missiles[index++])
        delete itr;

    for (auto itr : _removedObjects)
    {
        delete itr->obj;
        delete itr;
    }
}

uint32 World::GetGridSize()
{
    return _gridSize;
}

uint32 World::GetGridMaxX()
{
    return _gridMaxX;
}
uint32 World::GetGridMaxY()
{
    return _gridMaxY;
}

uint32 World::GetMaxX()
{
    return _gridMaxX * _gridSize;
}

uint32 World::GetMaxY()
{
    return _gridMaxY * _gridSize;
}

int32 World::CalculateGridIndex(int32 pos)
{
    return pos / static_cast<int32>(_gridSize);
}

int32 World::NormalizeCoord(int32 pos)
{
    return CalculateGridIndex(pos) * static_cast<int32>(_gridSize);
}

void World::IncreaseWorldSize()
{
    if (!Config::CanEdit())
        return;

    uint32 maxX = _gridMaxX + 1;
    uint32 maxY = _gridMaxY + 1;
    uint32 size = maxX * maxY;
    std::vector<Terrain*> newTerrain(size, nullptr);
    std::vector<Object*> newObjects(size, nullptr);
    _objects.resize(size, nullptr);
    _missiles.resize(size * 100, nullptr);

    for (uint32 x = 0; x < _gridMaxX; ++x)
    {
        for (uint32 y = 0; y < _gridMaxY; ++y)
        {
            newTerrain[x + y * maxX] = _terrainVec[x + y * _gridMaxX];
            newObjects[x + y * maxX] = _objectsMap[x + y * _gridMaxX];
        }
    }

    for (uint32 x = 0; x < maxX; ++x)
        newTerrain[x + _gridMaxY * maxX] = new Terrain();

    for (uint32 y = 0; y < maxY - 1; ++y)
            newTerrain[maxX - 1 + y * maxX] = new Terrain();

    std::swap(newTerrain, _terrainVec);
    std::swap(_objectsMap, newObjects);
    _gridMaxX = maxX;
    _gridMaxY = maxY;
}

void World::SaveToFile()
{
    std::ofstream file(_worldName, std::ios::binary);
#define WRITE(x) file.write(reinterpret_cast<char*>(&x), sizeof(x))
    WRITE(_gridMaxX);
    WRITE(_gridMaxY);
    for (auto t : _terrainVec)
    {
        auto type = t->GetType();
        WRITE(type);
    }
    file.close();
}

uint32 World::GenerateObjectId()
{
    return ++_objectIdCounter;
}

uint8 World::GeneratePlayerId()
{
    return ++_playerIdCounter;
}

std::vector<Missile*>& World::GetMissiles()
{
    return _missiles;
}

std::vector<Terrain*>& World::GetTerrain()
{
    return _terrainVec;
}

bool World::IsTerrainWalkableBy(uint32 x, uint32 y, MoveFlags flags)
{
    uint32 index = x + _gridMaxX * y;
    if (index >= _terrainVec.size())
        return false;
    return (_terrainVec[index]->GetMoveFlags() & flags) != 0;
}

void World::GetObjectsInArea(Position* start, Position* end, std::vector<Object*>& objectsVector)
{
    int32 sx = start->GetX();
    int32 sy = start->GetY();
    int32 ex = end->GetX();
    int32 ey = end->GetY();
    // swap coordinates if end is lower than start
    if (ex < sx)
        std::swap(ex, sx);
    if (ey < sy)
        std::swap(ey, sy);

    uint32 startX = std::max(CalculateGridIndex(sx) - 2, 0);
    uint32 startY = std::max(CalculateGridIndex(sy) - 2, 0);
    uint32 endX = std::min(CalculateGridIndex(ex) + 2, static_cast<int32>(_gridMaxX));
    uint32 endY = std::min(CalculateGridIndex(ey) + 2, static_cast<int32>(_gridMaxY));
    objectsVector.reserve(objectsVector.size() + (endX - startX) *(endY - startY));
    for (uint32 x = startX; x < endX; ++x)
    {
        for (uint32 y = startY; y < endY; ++y)
        {
            if (Object* obj = _objectsMap[x + _gridMaxX * y])
            {
                int32 objX = obj->GetX();
                int32 objY = obj->GetY();
                int32 size = obj->GetSize();
                if (objX + size >= sx && objY + size >= sy)
                    if (objX - size <= ex && objY - size <= ey)
                        objectsVector.push_back(obj);
            }
        }
    }
}

void World::GetObjectsInArea(Position* pos, int32 distance, std::vector<Object*>& objectsVector)
{
    Searcher search = Searcher(pos, distance);
    GetObjectsInArea(objectsVector, search);
}

void World::GetObjectsInArea(std::vector<Object*>& objectsVector, Searcher& searcher)
{
    Position*& pos = searcher.owner;
    uint32 startX = std::max(CalculateGridIndex(pos->GetX() - searcher.distance) - 2, 0);
    uint32 startY = std::max(CalculateGridIndex(pos->GetY() - searcher.distance) - 2, 0);
    uint32 endX = std::min(CalculateGridIndex(pos->GetX() + searcher.distance) + 2, static_cast<int32>(_gridMaxX));
    uint32 endY = std::min(CalculateGridIndex(pos->GetY() + searcher.distance) + 2, static_cast<int32>(_gridMaxY));
    for (uint32 x = startX; x < endX; ++x)
    {
        for (uint32 y = startY; y < endY; ++y)
        {
            if (Object* obj = _objectsMap[x + _gridMaxX * y])
            {
                if (searcher(obj))
                    objectsVector.push_back(obj);
            }
        }
    }
}

void World::GetObjectsInArea(int32 posX, int32 posY, int32 distance, std::vector<Object*>& objectsVector)
{
    Position pos(posX, posY);
    GetObjectsInArea(&pos, distance, objectsVector);
}

std::vector<Object*>& World::GetObjects()
{
    return _objectsMap;
}

void World::AddObject(Object* obj)
{
    uint32 x = CalculateGridIndex(obj->GetX());
    uint32 y = CalculateGridIndex(obj->GetY());
    _objectsMap[x + _gridMaxX * y] = obj;

    if (2 * _objectsCount >= _objects.size())
    {
        auto objVec = _objects;
        objVec.resize(_objects.size() * 2, nullptr);
        uint32 i = 0;
        while (_objects[i])
            _objects[i++] = nullptr;
        std::swap(objVec, _objects);
        DEBUG(std::cout << "resizing objects to size " << _objects.size() << std::endl);
    }

    obj->_containerPos = _objectsCount;
    _objects[_objectsCount] = obj;
    ++_objectsCount;
}

void World::RemoveObject(Object* obj)
{
    uint32 x = CalculateGridIndex(obj->GetX());
    uint32 y = CalculateGridIndex(obj->GetY());
    _objectsMap[x + _gridMaxX * y] = nullptr;
    _removedObjects.push_back(new RemovedObject(obj));

    if (10 * (_objectsCount + 1) < _objects.size())
    {
        std::vector<Object*> objVec(_objects.size() / 2, nullptr);
        memcpy(&objVec[0], &_objects[0], _objectsCount * sizeof(Object*));
        uint32 i = 0;
        while (_objects[i])
            _objects[i++] = nullptr;
        std::swap(objVec, _objects);
        DEBUG(std::cout << "shrinking objects to size " << _objects.size() << std::endl);
    }

    _objects[obj->_containerPos] = nullptr;
    if (_objects[_objectsCount - 1] != _objects[obj->_containerPos])
    {
        std::swap(_objects[obj->_containerPos], _objects[_objectsCount - 1]);
        _objects[obj->_containerPos]->_containerPos = obj->_containerPos;
    }
    --_objectsCount;
}

void World::AddMissile(Missile* missile)
{
    if (2 * _missilesCount >= _missiles.size())
    {
        auto mvec = _missiles;
        mvec.resize(_missiles.size() * 2, nullptr);
        uint32 i = 0;
        while (_missiles[i])
            _missiles[i++] = nullptr;
        std::swap(mvec, _missiles);
        DEBUG(std::cout << "resizing missiles to size " << _missiles.size() << std::endl);
    }

    missile->_containerPos = _missilesCount;
    _missiles[_missilesCount] = missile;
    ++_missilesCount;
}

void World::RemoveMissile(Missile* missile)
{
    _removedObjects.push_back(new RemovedObject(missile));
    _missiles[missile->_containerPos] = nullptr;
    if (_missiles[_missilesCount - 1] != _missiles[missile->_containerPos])
    {
        std::swap(_missiles[missile->_containerPos], _missiles[_missilesCount - 1]);
        _missiles[missile->_containerPos]->_containerPos = missile->_containerPos;
    }
    --_missilesCount;

    if (10 * (_missilesCount + 1) < _missiles.size())
    {
        std::vector<Missile*> mvec(_missiles.size() / 2, nullptr);
        memcpy(&mvec[0], &_missiles[0], _missilesCount * sizeof(Missile*));
        uint32 i = 0;
        while (_missiles[i])
            _missiles[i++] = nullptr;
        std::swap(mvec, _missiles);
        DEBUG(std::cout << "shrinking missiles to size " << _missiles.size() << std::endl);
    }
}

bool World::RelocateObject(Object* obj, int32 newX, int32 newY)
{
    uint32 oldXContainer = CalculateGridIndex(obj->GetX());
    uint32 oldYContainer = CalculateGridIndex(obj->GetY());
    uint32 newXContainer = CalculateGridIndex(newX);
    uint32 newYContainer = CalculateGridIndex(newY);
    if (oldXContainer != newXContainer || oldYContainer != newYContainer)
    {
        if (_objectsMap[newXContainer + _gridMaxX * newYContainer] != nullptr)
            return false;

        if (_objectsMap[oldXContainer + _gridMaxX * oldYContainer] == obj)
            _objectsMap[oldXContainer + _gridMaxX * oldYContainer] = nullptr;
        _objectsMap[newXContainer + _gridMaxX * newYContainer] = obj;
    }
    obj->SetCoords(newX, newY);
    return true;
}

void World::AddTerrain(Terrain* terrain, uint32 x, uint32 y)
{
    if (x + 1 > _gridMaxX)
        _gridMaxX = x + 1;
    if (y + 1 > _gridMaxY)
        _gridMaxY = y + 1;
    if (_terrainVec[x + _gridMaxX * y] != nullptr)
        delete _terrainVec[x + _gridMaxX * y];
    _terrainVec[x + _gridMaxX * y] = terrain;
}

void World::GeneratePaths()
{
    auto last = std::chrono::high_resolution_clock::now();
    auto now = std::chrono::high_resolution_clock::now();
    auto diff = last - now;

    while (!_deleting) // when graphics is deleted, stop updating
    {
#define UPDATE_TIME 10ms
        now = std::chrono::high_resolution_clock::now();
        diff = now - last;
        if (diff < 2 * UPDATE_TIME)
            std::this_thread::sleep_for(UPDATE_TIME - std::max(0ns, diff - UPDATE_TIME));
        std::chrono::duration<float> fdiff = now - last;
        if (!Config::IsEnabled(Config::paused))
        {
            #pragma omp parallel for schedule(dynamic, 200)
            for (int32 i = 0; i < _objectsCount; ++i)
            {
                Object* obj = _objects[i];
                if (!obj)
                    continue;

                if (obj->GetObjectType() == TYPEID_UNIT)
                    static_cast<Unit*>(obj)->UpdatePathfinder(fdiff.count());
            }
        }
        last = now;
    }
}

void World::Update(float diff)
{
    if (Config::IsAnyEnabled(Config::paused | Config::editable))
        return;

    #pragma omp parallel sections
    {
        #pragma omp section
        {
            #pragma omp parallel for
            for (uint32 i = 0; i < _missilesCount; ++i)
                _missiles[i]->UpdateParallel(diff);
        }

        #pragma omp section
        {
            #pragma omp parallel for
            for (uint32 i = 0; i < _objectsCount; ++i)
                _objects[i]->UpdateParallel(diff);
        }
        #pragma omp section
        {
            // delete no longer used objects
            std::list<RemovedObject*>::iterator itr = _removedObjects.begin();
            while (itr != _removedObjects.end())
            {
                if (((*itr)->timer -= diff) < 0.0f)
                {
                    RemovedObject* ro = (*itr);
                    delete ro->obj;
                    _removedObjects.erase(itr++);
                    delete ro;
                }
                else
                    ++itr;
            }
        }
    }

    for (uint32 i = 0; i < _missilesCount; ++i)
        _missiles[i]->Update(diff);

    for (uint32 i = 0; i < _objectsCount; ++i)
        _objects[i]->Update(diff);
}
