#ifndef STANDALONE_SERVER
#ifndef __GRAPHICS_INITIALIZE_H
#define __GRAPHICS_INITIALIZE_H

#include "SharedDefs.h"

class Graphics;

class GraphicsInitialize
{
public:
    static Graphics* CreateGraphicsContext();
private:
    static Graphics* CreateGraphicsContextInternal(GraphicsInitializeErrors& error);
};

#endif
#endif
