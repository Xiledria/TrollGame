#ifndef STANDALONE_SERVER

#include "Config.h"

#include "Graphics.h"
#include "World.h"
#include "Player.h"
#include "Unit.h"
#include "UIObject.h"
#include "UIHandler.h"
#include "Missile.h"
#include <chrono>
using namespace std::chrono_literals;

#ifdef _WIN32
#pragma warning(disable:4996) // disable deprecated fopen error
#endif



Graphics::Graphics()
{
    this->uiHeight = 200;
    this->minimapSize = uiHeight - 20;
    lastUpdateTime = std::chrono::high_resolution_clock::now();
}

Graphics::~Graphics()
{

}

GraphicsInitializeErrors Graphics::InitializeFonts()
{
    return GraphicsInitializeErrors::NONE;
}

Vec2<uint32>& Graphics::GetResolution()
{
    return resolution;
}

uint32 Graphics::GetScreenX()
{
    return resolution.GetX();
}

uint32 Graphics::GetScreenY()
{
    return resolution.GetY();
}

uint32 Graphics::GetMinimapSize()
{
    return minimapSize;
}

uint32 Graphics::GetUIHeight()
{
    return uiHeight;
}

Vec2<int32>* Graphics::GetCamera()
{
    return camera;
}

char* Graphics::FileToBuffer(char *file)
{
    FILE *fptr;
    long length;
    char *buf;

    fptr = fopen(file, "rb"); /* Open file for reading */
    if (!fptr) /* Return NULL on failure */
        return NULL;
    fseek(fptr, 0, SEEK_END); /* Seek to the end of the file */
    length = ftell(fptr); /* Find out how many bytes into the file we are */
    buf = static_cast<char*>(malloc(length + 1)); /* Allocate a buffer for the entire length of the file and a null terminator */
    fseek(fptr, 0, SEEK_SET); /* Go back to the beginning of the file */
    fread(buf, length, 1, fptr); /* Read the contents of the file in to the buffer */
    fclose(fptr); /* Close the file */
    buf[length] = 0; /* Null terminator */

    return buf; /* Return the buffer */
}

void Graphics::DrawCircle(int32 /*posx*/, int32 /*posy*/, int32 /*radius*/, const float* /*color4f*/)
{

}
void Graphics::DrawDisc(int32 /*posx*/, int32 /*posy*/, int32 /*radius*/, const float* /*color4f*/)
{

}

void Graphics::DrawTile(uint32 /*x*/, uint32 /*y*/, const float* /*color3f*/)
{   

}

void Graphics::DrawSquare(int32 /*x*/, int32 /*y*/, int32 /*radius*/, const float* /*color4f*/)
{

}

void Graphics::DrawUnitHealth(int32 /*x*/, int32 /*y*/, int32 /*radius*/, float /*pctHp*/)
{

}

void Graphics::DrawUnitSelection(int32 /*x*/, int32 /*y*/, int32 /*radius*/)
{

}

void Graphics::DrawActualSelecting()
{

}

void Graphics::DrawTeamColor(Unit* /*unit*/, Vec2<int32>& /*offset*/)
{
    
}

//@TODO: change uiHeight to int32? casting it all the times to int32 seems to be kind of bad solution
void Graphics::UpdateCamera()
{
    if (camera->GetX() <= 0)
        camera->SetX(0);
    else
    {
        int32 maxX = static_cast<int32>(world->GetMaxX() - resolution.GetX());
        if (maxX > 0)
        {
            if (camera->GetX() > maxX)
                camera->SetX(maxX);
        }
        else
            camera->SetX(0);
    }

    if (camera->GetY() <= 0)
        camera->SetY(0);
    else
    {
        int32 maxY = static_cast<int32>(world->GetMaxY() - resolution.GetY());
        if (maxY > 0)
        {
            if (camera->GetY() > maxY)
                camera->SetY(maxY);
        }
        else
            camera->SetY(0);
    }
}

void Graphics::DrawUIObject(UIObject* obj)
{
    // clickable child objects
    {
        std::vector<UIObject*>& childs = obj->GetChilds();
        for (auto child : childs)
            child->Draw();
    }
    // nonclickable child objects
    {
        std::vector<UIObject*>& childs = obj->GetChilds(false);
        for (auto child : childs)
            child->Draw();
    }
}

void Graphics::DrawUI()
{
    if (UIObject* parent = ui->GetVisibleParentObject())
        parent->Draw();
}

void Graphics::DrawVisibleObjects(UIObject* object)
{
    Vec2<int32> start, end, offset;
    offset = *camera;
    start = offset - object->GetSize() + object->GetCenter();
    end = offset + object->GetSize() + object->GetCenter();
    std::vector<Object*> objects;
    Position pstart(start.GetX(), start.GetY()), pend(end.GetX(), end.GetY());
    world->GetObjectsInArea(&pstart, &pend, objects);
    offset = offset - object->GetStart();
    for (auto object : objects)
        DrawSquare(object->GetX() - offset.GetX(), object->GetY() - offset.GetY(), object->GetSize(), object->GetColor4f());
    
    for (auto object : objects)
        if (object->GetObjectType() == TYPEID_UNIT)
            DrawTeamColor(static_cast<Unit*>(object), offset);

    for (auto i : player->GetSelected())
        DrawUnitSelection(i->GetX() - offset.GetX(), i->GetY() - offset.GetY(), i->GetSize());

    for (auto object : objects)
    {
        if (object->GetObjectType() == TYPEID_UNIT)
        {
            Unit* unit = static_cast<Unit*>(object);
            DrawUnitHealth(object->GetX() - offset.GetX(), object->GetY() - offset.GetY(), object->GetSize(), unit->GetHealthPct());
        }
    }

    uint32 index = 0;
    std::vector<Missile*>& missiles = world->GetMissiles();
    while (Missile* m = missiles[index++])
        DrawSquare(m->GetX() - offset.GetX(), m->GetY() - offset.GetY(), m->GetSize(), m->GetColor4f());

    DrawActualSelecting();
}


void Graphics::DrawWorld(uint32 /*startX*/, uint32 /*endX*/, uint32 /*startY*/, uint32 /*endY*/, UIObject* object)
{

}

void Graphics::DrawEditor()
{

}

void Graphics::DrawMinimap(UIObject* object)
{

}

void Graphics::Resize()
{
    UpdateCamera();
    if (ui)
    if (UIObject* parent = ui->GetVisibleParentObject())
        parent->OnWindowResized();
}

void Graphics::HandleWorldResize()
{

}

void Graphics::RecreateMinimap()
{

}

void Graphics::UpdateVSync()
{

}

void Graphics::UpdateFullscreen()
{

}

void Graphics::DrawWorld(UIObject* object)
{
    uint32 worldSizeX = world->GetGridMaxX();
    uint32 worldSizeY = world->GetGridMaxY();
    int32 startX = world->CalculateGridIndex(camera->GetX());
    int32 startY = world->CalculateGridIndex(camera->GetY());
    if (startX < 0)
        startX = 0;
    if (startY < 0)
        startY = 0;
    uint32 endX = startX + world->CalculateGridIndex(object->GetSize().GetX()) + 2;
    uint32 endY = startY + world->CalculateGridIndex(object->GetSize().GetY()) + 2;
    if (endX > worldSizeX)
        endX = worldSizeX;
    if (endY > worldSizeY)
        endY = worldSizeY;
    // draw world
    DrawWorld(startX, endX, startY, endY, object);
    // draw objects on world that are visible
    DrawVisibleObjects(object);
}

void Graphics::Run()
{
    DrawUI();

    uint16 fps = Config::GetFPSLimit();
    if (fps)
    {
        std::chrono::nanoseconds updateTime = std::chrono::nanoseconds(1000000000 / fps);
        auto now = std::chrono::high_resolution_clock::now();
        auto diff = lastUpdateTime - now;
        now = std::chrono::high_resolution_clock::now();
        diff = now - lastUpdateTime;
        if (diff < 2 * updateTime)
            std::this_thread::sleep_for(updateTime - std::max(0ns, diff - updateTime));
        lastUpdateTime = now;
    }
}

#endif
