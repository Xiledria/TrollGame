#include "Position.h"
#include "Object.h"

Position::Position() : Vec2()
{

}

Position::Position(int32 X, int32 Y) : Vec2(X, Y)
{

}

Position::~Position()
{

}

int32 Position::GetDistance(int32 X, int32 Y)
{
    return (Vec2(X, Y) - *this).GetLength();
}

int32 Position::GetDistance(Position* target)
{
    return (*target - *this).GetLength();
}

int32 Position::GetDistance(Position* target, int32& distX, int32& distY)
{
    Vec2 dist = (*target - *this);
    distX = dist.GetX();
    distY = dist.GetY();
    return dist.GetLength();
}

int32 Position::GetDistance(Object* target, bool size)
{
    if (size)
        return std::max((*target - *this).GetLength() - target->GetSize(), 0);
    return GetDistance(target);
}

int32 Position::GetDistance(Object* target, int32& distX, int32& distY, bool size)
{
    if (size)
    {
        Vec2 dist = (*target - *this);
        distX = dist.GetX();
        distY = dist.GetY();
        return std::max(dist.GetLength() - target->GetSize(), 0);
    }
    return GetDistance(target, distX, distY);
}
