#ifndef STANDALONE_SERVER
#ifdef BUILD_VULKAN_GRAPHICS
#include "VKGraphics.h"
#include <vector>

VKGraphics::VKGraphics(Game* game) : Graphics(game)
{
    // Filling out application description:
    // sType is mandatory
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    // pNext is mandatory
    applicationInfo.pNext = NULL;
    // The name of our application
    applicationInfo.pApplicationName = "Tutorial 1";
    // The name of the engine (e.g: Game engine name)
    applicationInfo.pEngineName = NULL;
    // The version of the engine
    applicationInfo.engineVersion = 1;
    // The version of Vulkan we're using for this application
    applicationInfo.apiVersion = VK_API_VERSION;

    // Filling out instance description:
    // sType is mandatory
    instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    // pNext is mandatory
    instanceInfo.pNext = NULL;
    // flags is mandatory
    instanceInfo.flags = 0;
    // The application info structure is then passed through the instance
    instanceInfo.pApplicationInfo = &applicationInfo;
    // Don't enable and layer
    instanceInfo.enabledLayerCount = 0;
    instanceInfo.ppEnabledLayerNames = NULL;
    // Don't enable any extensions
    instanceInfo.enabledExtensionCount = 0;
    instanceInfo.ppEnabledExtensionNames = NULL;

    // Now create the desired instance
    VkResult result = vkCreateInstance(&instanceInfo, NULL, &instance);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "Failed to create instance: %d\n", result);
        abort();
    }

    // Query how many devices are present in the system
    uint32_t deviceCount = 0;
    result = vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "Failed to query the number of physical devices present: %d\n", result);
        abort();
    }

    // There has to be at least one device present
    if (deviceCount == 0) {
        fprintf(stderr, "Couldn't detect any device present with Vulkan support: %d\n", result);
        abort();
    }

    // Get the physical devices
    std::vector<VkPhysicalDevice> physicalDevices(deviceCount);

    // Enumerate all physical devices
    VkPhysicalDeviceProperties deviceProperties;
    for (uint32 i = 0; i < deviceCount; i++) {
        result = vkEnumeratePhysicalDevices(instance, &deviceCount, &physicalDevices[i]);
        if (result != VK_SUCCESS) {
            fprintf(stderr, "Faied to enumerate physical devices present: %d\n", result);
            abort();
        }
        memset(&deviceProperties, 0, sizeof deviceProperties);
        vkGetPhysicalDeviceProperties(physicalDevices[i], &deviceProperties);
        printf("Driver Version: %d\n", deviceProperties.driverVersion);
        printf("Device Name:    %s\n", deviceProperties.deviceName);
        printf("Device Type:    %d\n", deviceProperties.deviceType);
        printf("API Version:    %d.%d.%d\n",
            // See note below regarding this:
            (deviceProperties.apiVersion >> 22) & 0x3FF,
            (deviceProperties.apiVersion >> 12) & 0x3FF,
            (deviceProperties.apiVersion & 0x3FF));

        uint32 queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevices[i], &queueFamilyCount, NULL);
        std::vector<VkQueueFamilyProperties> familyProperties(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevices[i], &queueFamilyCount, &familyProperties[0]);

        // Print the families
        for (uint32 j = 0; j < queueFamilyCount; j++) {
            printf("Count of Queues: %d\n", familyProperties[j].queueCount);
            printf("Supported operationg on this queue:\n");
            if (familyProperties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT)
                printf("\t\t Graphics\n");
            if (familyProperties[j].queueFlags & VK_QUEUE_COMPUTE_BIT)
                printf("\t\t Compute\n");
            if (familyProperties[j].queueFlags & VK_QUEUE_TRANSFER_BIT)
                printf("\t\t Transfer\n");
            if (familyProperties[j].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT)
                printf("\t\t Sparse Binding\n");
        }

        VkDeviceCreateInfo deviceInfo;
        // Mandatory fields
        deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceInfo.pNext = NULL;
        deviceInfo.flags = 0;

        // We won't bother with extensions or layers
        deviceInfo.enabledLayerCount = 0;
        deviceInfo.ppEnabledLayerNames = NULL;
        deviceInfo.enabledExtensionCount = 0;
        deviceInfo.ppEnabledExtensionNames = NULL;

        // Here's where we initialize our queues
        VkDeviceQueueCreateInfo deviceQueueInfo;
        deviceQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueInfo.pNext = NULL;
        deviceQueueInfo.flags = 0;
        // Use the first queue family in the family list
        deviceQueueInfo.queueFamilyIndex = 0;

        // Create only one queue
        float queuePriorities[] = { 1.0f };
        deviceQueueInfo.queueCount = 1;
        deviceQueueInfo.pQueuePriorities = queuePriorities;
        // Set queue(s) into the device
        deviceInfo.queueCreateInfoCount = 1;
        deviceInfo.pQueueCreateInfos = &deviceQueueInfo;
        VkDevice device;
        result = vkCreateDevice(physicalDevices[i], &deviceInfo, NULL, &device);
        if (result != VK_SUCCESS) {
            fprintf(stderr, "Failed creating logical device: %d\n", result);
            abort();
        }
    }
}

VKGraphics::~VKGraphics()
{
    // Never forget to free resources
    vkDestroyInstance(instance, NULL);
}

void VKGraphics::Run()
{

}

#endif
#endif
