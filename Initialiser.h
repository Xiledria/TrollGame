#ifndef __INITIALISER_H
#define __INITIALISER_H

namespace Initialiser
{
    bool InitializeGraphics(bool initial = true);
    bool Init();
    void Exit();
};

#endif
