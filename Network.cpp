#include "Network.h"
#include "Message.h"

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#define fd_set FD_SET
#else
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <cstring>
#endif

//@TODO: check for optimalisations


Network::Network()
{
    host = 0;
    serverInfo = nullptr;
}

Network::~Network()
{
    if (serverInfo)
        free(serverInfo);
}

// initializes socket to accept incomming connections
void Network::InitializeServer(uint16 port)
{
    addrinfo hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

    getaddrinfo(NULL, std::to_string(port).c_str(), &hints, &serverInfo);

    // make a socket, bind it, and listen on it:

    host = socket(serverInfo->ai_family, serverInfo->ai_socktype, serverInfo->ai_protocol);
    bind(host, serverInfo->ai_addr, serverInfo->ai_addrlen);
    listen(host, 20);
    thread[CLIENT_ACCEPT_THREAD] = new std::thread(&Network::AcceptNewConnections, this);
}

// blocking function waiting for new connections
void Network::AcceptNewConnections()
{
    while (connected)
    {
        struct sockaddr_storage their_addr;
        socklen_t addr_size;
        SOCKET client;
        // now accept an incoming connection:

        addr_size = sizeof(their_addr);
        client = accept(host, (struct sockaddr *)&their_addr, &addr_size);
        if (client == SOCKET_ERROR)
            continue;

        ClientInfo info(client);
        clients.insert(std::pair<SOCKET, ClientInfo>(client, info));
        // @TODO: use something like poll() or select()
        // (well.. but threads are fine for few connections anyways..)
        info.rcvThread = new std::thread(&Network::HandleRcvConnections, this, client);
    }
}

// connects to specific ip address on specified port
void Network::ConnectToServer(std::string address, std::string port)
{
#ifdef _WIN32
    WSADATA wsa_data;
    // Initialize Winsock
    int res = WSAStartup(MAKEWORD(2, 2), &wsa_data);
    if (res != 0)
    {
        DEBUG(std::cout << "WSAStartup failed with error: " << res << std::endl);
        return;
    }
#endif

    struct addrinfo hints;
    SOCKET sockfd;

    // first, load up address structs with getaddrinfo():

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    getaddrinfo(address.c_str(), port.c_str(), &hints, &serverInfo);

    // make a socket:
    sockfd = socket(serverInfo->ai_family, serverInfo->ai_socktype, serverInfo->ai_protocol);

    // connect!
    connect(sockfd, serverInfo->ai_addr, serverInfo->ai_addrlen);

#ifndef STANDALONE_SERVER
    isServer = false;
#endif
    thread[SND_THREAD] = new std::thread(&Network::HandleSndConnections, this);
    thread[RCV_THREAD] = new std::thread(&Network::HandleRcvConnections, this, host);
}

void Network::GetMessages(int32 rcvbytes, char* rcvbuffer)
{
    uint16 offset = 0; // offsetted position in rcvbuffer
    while (rcvbytes > 1)
    {
        uint16 messageBytes; // bytes for single message
        memcpy((uint8*)&messageBytes, rcvbuffer + offset, sizeof(messageBytes));
        offset += Message::GetBufferByteSize();
        char* messageBuffer = new char[messageBytes + 1];
        memcpy(messageBuffer, rcvbuffer + offset, messageBytes);
        messageBuffer[messageBytes] = 0;
        Message* message = new Message(messageBuffer, messageBytes);
        AddRcvMessage(message);
        offset += messageBytes;
        rcvbytes -= messageBytes + Message::GetBufferByteSize();
        delete[] messageBuffer;
    }
}

void Network::HandleRcvConnections(SOCKET rcvsocket)
{
#ifndef STANDALONE_SERVER
    // #ifdef STANDALONE_SERVER
    // only resend and calculate everything, no need to display effects in graphics
    // #else
    // if (isServer) resend everything and recalculate, also display, client is hosting game
    // else
    // only client - just use packet and maybe reply if required
    // #endif
    int32 rcvbytes;
    char* rcvbuffer = new char[65535];
    if (!isServer)
    {
        while (connected)
        {
            rcvbytes = recv(rcvsocket, rcvbuffer, 65535/*max bytes*/, 0);
            if (rcvbytes == -1 || rcvbytes == 0)
            {
                DEBUG(std::cout << "Disconnected from server" << std::endl);
                connected = false;
            }
            else
                GetMessages(rcvbytes, rcvbuffer);
        }
        delete[] rcvbuffer;
    }
    else
#endif
    {

    }
}

void Network::HandleSndConnections()
{
#ifndef STANDALONE_SERVER
    if (!isServer)
    {
        while (connected)
        {
            std::vector<Message*> messages;
            GetSndMessages(messages);
            for (auto msg : messages)
            {
                if (SOCKET target = msg->GetTarget())
                    DirectSendMessage(msg, target);
                else
                    BroadcastMessage(msg);
            }
            // no need to clear scope local vector
            // messages.clear();
        }
    }
    else
#endif
    {

    }
}

// sends message immediatedly and deletes message - should be used within Network code
void Network::DirectSendMessage(Message* message, SOCKET target)
{
    uint16 len;
    const char* buffer = message->ToPacket(len);
    send(target, buffer, len, 0);
    delete buffer;
    delete message;
}

// broadcasts messageg to all connected clients (ONLY FOR SERVER) and deletes message
void Network::BroadcastMessage(Message *message)
{
    uint16 len;
    const char* buffer = message->ToPacket(len);
    //@TODO: send to all sockets connected
    // send(target, buffer, len, 0);
    delete buffer;
    delete message;
}

///lock sender only once per game update loop
//network thread can be delayed by this, but we cannot afford to delay game loop, which is slower than network
void Network::LockSender()
{
    sndMutex.lock();
}

// must be called ONLY between LockSender() and UnlockSender() calls, thread unsafe
void Network::AddSndMessage(Message* message)
{
    sndMessages.push_back(message);
}

/// thread safe, vector is locked for every message, added overhead is only for network thread
// we cannot afford to let game loop sleep waiting for messages too long
void Network::AddRcvMessage(Message* message)
{
    std::lock_guard<std::mutex> lock(rcvMutex);
    rcvMessages.push_back(message);
}

// unlock sender only once per game update loop
void Network::UnlockSender()
{
    sndMutex.unlock();
}

// must be called only from network thread
void Network::GetSndMessages(std::vector<Message*>& messages)
{
    std::lock_guard<std::mutex> lock(sndMutex);
    sndMessages.swap(messages);
}

// gets vector of not handled messages
void Network::GetRcvMessages(std::vector<Message*>& messages)
{
    std::lock_guard<std::mutex> lock(rcvMutex);
    usedRcvMessages.clear();
    rcvMessages.swap(messages);
}
