#include "ObjectStore.h"
#include "World.h"
#include "Object.h"
#include "Model.h"

                                                                                          // @TODO: supply entry from somewhere
Object::Object(uint32 modelId) : _type(TYPEID_OBJECT), _objectId(world->GenerateObjectId()), _entry(0), _model(objectStore->GetModel(modelId))
{
}

Object::Object() : _type(TYPEID_OBJECT), _objectId(0), _entry(0), _model(nullptr)
{
}

Object::~Object()
{

}

const Model* Object::GetModel()
{
    return _model;
}

const ObjectType Object::GetObjectType()
{
    return _type;
}

const float* Object::GetColor4f()
{
    return _model->GetColor4f();
}

const int32 Object::GetSize()
{
    return _model->GetSize();
}

const uint32 Object::GetId()
{
    return _objectId;
}

bool Object::IsMoving()
{
    return false;
}

int32 Object::GetDistance(int32 X, int32 Y)
{
    return Position::GetDistance(X, Y);
}

int32 Object::GetDistance(Position* target)
{
    return Position::GetDistance(target);
}

int32 Object::GetDistance(Position* target, int32& distX, int32& distY)
{
    return Position::GetDistance(target, distX, distY);
}

int32 Object::GetDistance(Object* target, bool size)
{
    if (size)
    {
        int32 totalSize = target->GetSize() + this->GetSize();
        int32 distX = target->GetX() - GetX();
        if (distX > 0)
            distX -= std::min(std::abs(distX), totalSize);
        else
            distX += std::min(std::abs(distX), totalSize);
        int32 distY = target->GetY() - GetY();
        if (distY > 0)
            distY -= std::min(std::abs(distY), totalSize);
        else
            distY += std::min(std::abs(distY), totalSize);
        return static_cast<int32>(std::sqrt(distX * distX + distY * distY));
    }

    return Position::GetDistance(target);
}

int32 Object::GetDistance(Object* target, int32& distX, int32& distY, bool size)
{
    if (size)
    {
        int32 totalSize = target->GetSize() + this->GetSize();
        distX = target->GetX() - GetX();
        if (distX > 0)
            distX -= std::min(std::abs(distX), totalSize);
        else
            distX += std::min(std::abs(distX), totalSize);
        distY = target->GetY() - GetY();
        if (distY > 0)
            distY -= std::min(std::abs(distY), totalSize);
        else
            distY += std::min(std::abs(distY), totalSize);
        return static_cast<int32>(std::sqrt(distX * distX + distY * distY));
    }
    return Position::GetDistance(target, distX, distY);
}
