#ifndef __PLAYER_H
#define __PLAYER_H

#include "SharedDefs.h"
#include <list>
#include <string>

class Unit;
class Terrain;
enum TerrainType : uint8;

class Player
{
public:
    Player();
    ~Player();
    void AddSelected(Unit* target);
    void ClearSelected();
    std::list<Unit*>& GetSelected();

    void RemoveDeadSelected();
    void UpdateCheat(char* c);
    void UpdateBuilder(TerrainType type);
    Terrain* GetBuilder();
    uint8 GetId();
    const float* GetColor3f();
private:
    float _color3f[3];
    std::list<Unit*> _selected;
    std::string _cheat;
    Terrain* _builder;
    uint8 _id;
};

extern Player* player;

#endif
