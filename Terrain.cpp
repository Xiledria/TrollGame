#include "Terrain.h"

Terrain::Terrain(TerrainType terrainType)
{
    SetType(terrainType);
}

Terrain::Terrain(Terrain* copy)
{
    *this = *copy;
}

Terrain::~Terrain()
{

}

void Terrain::SetType(TerrainType terrainType)
{
    switch (terrainType)
    {
    case TERRAIN_GRASS:
        SetColor(0.30f, 0.74f, 0.20f);
        _moveFlags = MoveFlags(MOVEFLAG_WALKING | MOVEFLAG_FLYING);
        break;
    case TERRAIN_WATER:
        SetColor(0.31f, 0.47f, 0.63f);
        _moveFlags = MoveFlags(MOVEFLAG_SWIMMING | MOVEFLAG_FLYING);
        break;
    case TERRAIN_DIRT:
        SetColor(0.38f, 0.20f, 0.07f);
        _moveFlags = MoveFlags(MOVEFLAG_WALKING | MOVEFLAG_FLYING);
        break;
    case TERRAIN_PLACEHOLDER:
        SetColor(0.0f, 0.0f, 0.0f);
        _moveFlags = MoveFlags(MOVEFLAG_NONE);
    default:
        terrainType = TERRAIN_PLACEHOLDER;
        break;
    }
    _type = terrainType;
}

TerrainType Terrain::GetType()
{
    return _type;
}

MoveFlags Terrain::GetMoveFlags()
{
    return _moveFlags;
}
const float* Terrain::GetColor3f()
{
    return _color3f;
}

void Terrain::SetColor(float r, float g, float b)
{
    _color3f[0] = r;
    _color3f[1] = g;
    _color3f[2] = b;
}
