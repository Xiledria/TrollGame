#ifndef STANDALONE_SERVER

#include "Config.h"

#include "ImmediateGLGraphics.h"
#include "Controlls.h"
#include "UIHandler.h"
#include "Fonts.h"
#include "World.h"
#include "Player.h"
#include "Unit.h"
#include "Terrain.h"

using namespace std::chrono_literals;

ImmediateGLGraphics::ImmediateGLGraphics()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) /* Initialize SDL's Video subsystem */
        SDLDie(const_cast<char*>("Unable to initialize SDL")); /* Or die on error */

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);


    /* Turn on double buffering with a 24bit Z buffer.
    * You may need to change this to 16 or 32 for your system */
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    /* Create our fullscreen window */
    window = SDL_CreateWindow(PROJECT_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        resolution.GetX(), resolution.GetY(), SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN_DESKTOP);
    graphics = this;
    UpdateFullscreen();
    if (!window) /* Die if creation failed */
        SDLDie(const_cast<char*>("Unable to create window"));

    /* Create our opengl context and attach it to our window */
    context = SDL_GL_CreateContext(window);
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        /* Problem: glewInit failed, something is seriously wrong. */
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }

    /* This makes our buffer swap syncronized with the monitor's vertical refresh */
    SDL_GL_SetSwapInterval(1);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE); // cull face
    glCullFace(GL_BACK); // cull back
    glEnableClientState(GL_VERTEX_ARRAY);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, resolution.GetX(), 0, resolution.GetY());   // only the window is changing, not the camera
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(static_cast<GLfloat>(-camera->GetX()), static_cast<GLfloat>(-camera->GetY()), 0.0f);
    glGenTextures(1, &mapTexture);
    RecreateMinimap();
    Resize();
}

ImmediateGLGraphics::~ImmediateGLGraphics()
{
    delete fonts;
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void ImmediateGLGraphics::Run()
{
    bool quit = false;
    auto now = std::chrono::steady_clock::now();
    auto last = std::chrono::steady_clock::now();
    auto second = std::chrono::steady_clock::now();
    std::chrono::duration<float> diff = now - last;
    int32 fps = 0;
    int32 shownFPS = 0;
    //While application is running
    while (!quit)
    {
        glColor3f(1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear display window
        Graphics::Run();
        if (Config::IsEnabled(Config::showFPS))
            fonts->RenderText((std::to_string(shownFPS) + " FPS").c_str(), 15, resolution.GetY() - 46, false);
        SDL_GL_SwapWindow(window);
        diff = now - last;
        ++fps;
        if (second + 0.5s < std::chrono::steady_clock::now())
        {
            fps *= 2;
            shownFPS = fps;
            second = std::chrono::steady_clock::now();
            fps = 0;
        }
        last = now;
        // handle controlls
        now = std::chrono::steady_clock::now();
        quit = controlls->Update(diff.count());
    }
}

void ImmediateGLGraphics::UpdateCamera()
{
    Graphics::UpdateCamera();
    glLoadIdentity();
    glTranslatef(static_cast<GLfloat>(-camera->GetX()), static_cast<GLfloat>(-camera->GetY()), 0.0f);
}

/* A simple function that prints a message, the error code returned by SDL, and quits the application */
void ImmediateGLGraphics::SDLDie(char *msg)
{
    printf("%s: %s\n", msg, SDL_GetError());
    SDL_Quit();
    exit(1);
}

GraphicsInitializeErrors ImmediateGLGraphics::InitializeFonts()
{
    fonts = new Fonts();
    return fonts->Initialize(24);
}

void ImmediateGLGraphics::DrawCircle(int32 posx, int32 posy, int32 radius, const float* color4f)
{
    glColor4fv(color4f);
    uint8 num_segments = 20;
    float theta = 2 * 3.1415926f / float(num_segments);
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = static_cast<float>(radius);//we start at angle = 0
    float y = 0;

    glBegin(GL_LINE_LOOP);
    for (int ii = 0; ii < num_segments; ii++)
    {
        glVertex2f(x + posx, y + posy);//output vertex

                                       //apply the rotation matrix
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();
}

void ImmediateGLGraphics::DrawDisc(int32 posx, int32 posy, int32 radius, const float* color4f)
{
    glColor4fv(color4f);
    uint8 num_segments = 20;
    float theta = 2 * 3.1415926f / float(num_segments);
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = static_cast<float>(radius);//we start at angle = 0
    float y = 0;

    glBegin(GL_TRIANGLE_FAN);
    for (int ii = 0; ii < num_segments; ii++)
    {
        glVertex2f(x + posx, y + posy);//output vertex

                                       //apply the rotation matrix
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();
}

void ImmediateGLGraphics::DrawTile(uint32 x, uint32 y, const float* color3f)
{
    int32 X = x * world->GetGridSize();
    int32 Y = y * world->GetGridSize();
    glColor3fv(color3f);
    glRecti(X, Y, X + world->GetGridSize(), Y + world->GetGridSize());
}

void ImmediateGLGraphics::DrawSquare(int32 x, int32 y, int32 radius, const float* color4f)
{
    glColor4fv(color4f);
    glRecti(x - radius, y - radius, x + radius, y + radius);
}

void ImmediateGLGraphics::DrawUnitHealth(int32 x, int32 y, int32 radius, float pctHp)
{
    int32 startX = x - radius;
    glBegin(GL_LINES);
    glColor3f(0, 1, 0);
    glVertex2i(startX, y + radius);
    glVertex2f(startX + 2 * radius * pctHp, static_cast<GLfloat>(y + radius));
    glColor3f(1, 0, 0);
    glVertex2f(startX + 2 * radius * pctHp, static_cast<GLfloat>(y + radius));
    glVertex2i(x + radius, y + radius);
    glEnd();
}

void ImmediateGLGraphics::DrawUnitSelection(int32 x, int32 y, int32 radius)
{
    glBegin(GL_LINE_LOOP);
    glColor4f(1, 1, 1, 0.5f);
    glVertex2i(x - radius, y + radius);
    glVertex2i(x + radius, y + radius);
    glVertex2i(x + radius, y - radius);
    glVertex2i(x - radius, y - radius);
    glEnd();
}

void ImmediateGLGraphics::DrawActualSelecting()
{
    Position* area = controlls->GetSelectionArea();

    auto startPos = area[0] + area[2];
    auto endPos = area[1] + area[2];
    glBegin(GL_LINE_LOOP);
    glColor4f(1.0f, 1.0f, 0.0f, 0.5f);
    glVertex2iv(startPos.GetCoords());
    glVertex2i(startPos.GetX(), endPos.GetY());
    glVertex2iv(endPos.GetCoords());
    glVertex2i(endPos.GetX(), startPos.GetY());
    glEnd();
}

void ImmediateGLGraphics::DrawTeamColor(Unit* unit, Vec2<int32>& offset)
{
    //@TODO: remove this ugly hack! and make it more generic, lol
    if (player->GetId() == unit->GetOwnerId())
        glColor3fv(player->GetColor3f());
    else
    {
        float color3f[] = { 0.5f, 0.0f, 0.5f };
        glColor3fv(color3f);
    }
    int32 radius = unit->GetSize() - 2;
    Vec2<int32> rad = Vec2<int32>(radius, radius);
    Vec2<int32> start = *unit - offset + rad;
    Vec2<int32> end = *unit - offset - rad;
    glBegin(GL_LINES);
    glVertex2i(end.GetX(), start.GetY());
    glVertex2i(start.GetX(), end.GetY());
    glVertex2i(start.GetX(), start.GetY());
    glVertex2i(end.GetX(), end.GetY());
    glEnd();
}

void ImmediateGLGraphics::DrawMinimap(UIObject* object)
{
    Vec2<float> start, end;
    start = object->GetCenter() - (object->GetSize() * 0.5f);
    end = object->GetCenter() + (object->GetSize() * 0.5f);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, mapTexture);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2i(start.GetX(), start.GetY());
    glTexCoord2f(1, 0);
    glVertex2i(end.GetX(), start.GetY());
    glTexCoord2f(1, 1);
    glVertex2i(end.GetX(), end.GetY());
    glTexCoord2f(0, 1);
    glVertex2i(start.GetX(), end.GetY());
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 0.0f);
    // @TODO: use gameboard size instead of this
    Vec2<int32> cstart = *camera;
    // @TODO: use gameboard size instead of this
    Vec2<int32> cend = *camera + resolution;

    Vec2<float> mult;
    mult = Vec2<int32>(world->GetGridMaxX(), world->GetGridMaxY()) * world->GetGridSize() / object->GetSize();
    Vec2<float> sstart, send;
    sstart = start + (cstart / mult);
    send = start + (cend / mult);
    // minimap selection - shows where player actualy is
    glBegin(GL_LINE_LOOP);
    glVertex2f(sstart.GetX(), sstart.GetY());
    glVertex2f(send.GetX(), sstart.GetY());
    glVertex2f(send.GetX(), send.GetY());
    glVertex2f(sstart.GetX(), send.GetY());
    glEnd();

    glBegin(GL_QUADS); // @TODO: move me elsewhere, add team colors on minimap instead of this
    uint32 index = 0;
    auto& objects = world->_objects;
    while (auto* itr = objects[index++])
    {
        if (itr->GetObjectType() == TYPEID_UNIT)
        {
            //@TODO: remove this ugly hack! and make it more generic, lol
            if (player->GetId() == static_cast<Unit*>(itr)->GetOwnerId())
                glColor3fv(player->GetColor3f());
            else
            {
                float color3f[] = { 0.5f, 0.0f, 0.5f };
                glColor3fv(color3f);
            }
        }
        else
            glColor4fv(itr->GetColor4f());
        Object* obj = itr;
        int32* coordPtr = obj->GetCoords();
        Vec2<int32> coord(coordPtr[0], coordPtr[1]);
        Vec2<int32> mstart, mend, size;
        // @TODO: objects with different X and Y
        size = Vec2<int32>(obj->GetSize(), obj->GetSize());
        mstart = (coord + size) / mult + start;
        mend = (coord - size) / mult + start;
        glVertex2f(mstart.GetX(), mstart.GetY());
        glVertex2f(mend.GetX(), mstart.GetY());
        glVertex2f(mend.GetX(), mend.GetY());
        glVertex2f(mstart.GetX(), mend.GetY());
    }
    glEnd();
}
#include <iostream>
void ImmediateGLGraphics::DrawWorld(uint32 startX, uint32 endX, uint32 startY, uint32 endY, UIObject* object)
{
    std::vector<Terrain*>& terrain = world->GetTerrain();
    Vec2<int32> offset = *camera - object->GetStart() - (Vec2<int32>(world->GetGridSize(), world->GetGridSize()) / 2);
    for (uint32 x = startX; x < endX; ++x)
    {
        for (uint32 y = startY; y < endY; ++y)
        {
            Terrain* t = terrain[x + world->GetGridMaxX() * y];
            float color4f[4] = { t->GetColor3f()[0], t->GetColor3f()[1], t->GetColor3f()[2], 1.0f };
            DrawSquare(world->GetGridSize() * x - offset.GetX(), world->GetGridSize() * y - offset.GetY(), world->GetGridSize() / 2, color4f);
        }
    }
}

void ImmediateGLGraphics::DrawUIObject(UIObject* obj)
{
    if (!obj->IsVisible())
        return;
    glColor3fv(obj->GetColor3f());
    Vec2<int32> end = obj->GetStart() + obj->GetSize();
    Vec2<int32> start = obj->GetStart();
    glBegin(GL_QUADS);
    glVertex2i(start.GetX(), start.GetY());
    glVertex2i(end.GetX(), start.GetY());
    glVertex2i(end.GetX(), end.GetY());
    glVertex2i(start.GetX(), end.GetY());
    glEnd();
    if (!obj->GetText().empty())
        fonts->RenderText(obj->GetText().c_str(), obj->GetCenter().GetX(), obj->GetCenter().GetY(), true);

    Graphics::DrawUIObject(obj);
}

void ImmediateGLGraphics::DrawUI()
{
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(0, 0, 0);
    Graphics::DrawUI();
    glPopMatrix();
}

void ImmediateGLGraphics::DrawEditor()
{
    int32 gridSize = world->GetGridSize();
    uint32 i = 0;
    const float voidColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    const float grassColor[] = { 0.30f, 0.74f, 0.20f, 1.0f };
    const float dirtColor[] = { 0.38f, 0.20f, 0.07f, 1.0f };
    const float waterColor[] = { 0.31f, 0.47f, 0.63f, 1.0f };
    DrawSquare(++i * gridSize, gridSize, gridSize / 2, voidColor);   // void
    DrawSquare(++i * gridSize, gridSize, gridSize / 2, grassColor);  // grass
    DrawSquare(++i * gridSize, gridSize, gridSize / 2, dirtColor);   // dirt
    DrawSquare(++i * gridSize, gridSize, gridSize / 2, waterColor);  // water
}

void ImmediateGLGraphics::Resize()
{
    int w,h;
    SDL_GetWindowSize(window, (int*)(&w), (int*)(&h));
    resolution.SetCoords(w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluOrtho2D(0, w, 0, h);   // only the window is changing, not the camera
    glMatrixMode(GL_MODELVIEW);
    Graphics::Resize();
}

void ImmediateGLGraphics::HandleWorldResize()
{
    RecreateMinimap();
}

void ImmediateGLGraphics::RecreateMinimap()
{
    // prepare minimap texture
    uint32 maxX = world->GetGridMaxX();
    uint32 maxY = world->GetGridMaxY();
    float* minimapBitmap = new float[maxX * maxY * 3];
    std::vector<Terrain*>& terrain = world->GetTerrain();
    for (uint32 x = 0; x < maxX; ++x)
    {
        for (uint32 y = 0; y < maxY; ++y)
        {
            Terrain* t = terrain[x + maxX * y];
            memcpy(&minimapBitmap[(x + y * maxX) * 3], t->GetColor3f(), sizeof(float) * 3);
        }
    }

    glBindTexture(GL_TEXTURE_2D, mapTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, maxX, maxY, 0, GL_RGB, GL_FLOAT, minimapBitmap);
    delete[] minimapBitmap;
}

void ImmediateGLGraphics::UpdateVSync()
{
    SDL_GL_SetSwapInterval(Config::vsync ? 1 : 0);
}

void ImmediateGLGraphics::UpdateFullscreen()
{
    if (Config::IsEnabled(Config::fullscreen))
    {
        SDL_Rect rect;
        SDL_GetDisplayBounds(0, &rect);
        resolution.SetCoords(rect.w, rect.h);
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    }
    else
    {
        SDL_Rect rect;
        SDL_GetDisplayBounds(0, &rect);
        resolution.SetCoords(rect.w - 50, rect.h - 50);
        SDL_SetWindowFullscreen(window, 0);
        SDL_SetWindowSize(window, resolution.GetX(), resolution.GetY());
        SDL_SetWindowPosition(window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    }
    Resize();
}

#endif
