#ifndef STANDALONE_SERVER
#include "UIFunctions.h"
#include "UIObject.h"
#include "UIHandler.h"
#include "Config.h"
// @TODO: remove me as soon as moving to menus get moved to UIHandler instead of controlls
#include "Controlls.h"
#include "Graphics.h"
#include "Player.h"
#include "World.h"
#include <map>
#include <filesystem>

void GetFilesInDirectory(std::vector<std::string> &out, const std::string &directory)
{
    std::filesystem::path dir = directory;
    std::filesystem::directory_iterator enditr;
    for (std::filesystem::directory_iterator itr(dir); itr != enditr; ++itr)
        out.push_back(itr->path().filename().string());
}

void ContinueGame()
{
    Config::Clear(Config::paused);
    ui->MoveToMenu(UIHandler::INGAME_MENU);
}

void SaveWorld()
{
    world->SaveToFile();
}

void ExitGame()
{
    controlls->Quit();
}

void EmptyFn()
{

}

void MoveToMenu(int32 menuId)
{
    ui->MoveToMenu(UIHandler::Menu(menuId));
}

void RenderMinimap(UIObject* obj)
{
    graphics->DrawMinimap(obj);
}

void RenderWorld(UIObject* obj)
{
    graphics->DrawWorld(obj);
}

void HandleSendSelected(UIObject* obj)
{
    controlls->HandleSendSelected(obj);
}

void HandleWorldClick(UIObject* obj)
{
    controlls->HandleWorldClick(obj);
}

void HandleWorldDrag(UIObject* obj)
{
    controlls->HandleWorldDrag(obj);
}

void MoveInWorld(UIObject* obj)
{
    Vec2<float> mult = Vec2<float>(world->GetMaxX(), world->GetMaxY()) / obj->GetSize();
    *camera = mult * obj->GetRelativeClickPos() - (graphics->GetResolution() / 2);
    graphics->UpdateCamera();
}

void SetFPS(UIObject* obj)
{
    Config::SetFPSLimit(atoi(obj->GetText().c_str()));
}

void ShowFPSText(UIObject* obj)
{
    std::string text = std::to_string(Config::GetFPSLimit());
    obj->SetText(text);
}

void CreateNewWorld(UIObject* obj)
{
    std::vector<UIObject*>& childs = obj->GetParent()->GetChilds();
    std::string xStr = childs[0]->GetText();
    std::string yStr = childs[1]->GetText();
    std::string name = childs[2]->GetText();
    if (xStr.empty() || yStr.empty() || name.empty())
        return;
    uint32 x = atoi(xStr.c_str());
    uint32 y = atoi(yStr.c_str());
    name = std::string("maps/" + name + ".dat");
    world->CreateNewWorld(name, x, y);
    ContinueGame();
    graphics->HandleWorldResize();
    graphics->UpdateCamera();
    player->ClearSelected();
}

void DefaultRender(UIObject* obj)
{
    graphics->DrawUIObject(obj);
}

void HideOnConfigFlags(UIObject* obj, int32 flag)
{
    obj->SetVisible(!Config::IsEnabled(Config::ConfigFlags(flag)));
}

void ShowOnConfigFlags(UIObject* obj, int32 flag)
{
    obj->SetVisible(Config::IsEnabled(Config::ConfigFlags(flag)));
}

void LoadWorld(UIObject* obj, int32 editable)
{
    world->LoadWorld(std::string("maps/") + obj->GetText().c_str());
    graphics->HandleWorldResize();
    graphics->UpdateCamera();
    player->ClearSelected();
    ContinueGame();
    if (editable)
        Config::Set(Config::editable);
}

enum MapModes
{
    MODE_SINGLEPLAYER,
    MODE_MULTIPLAYER,
    MODE_EDIT,
};

void LoadMaps(UIObject* obj, int32 mode)
{
    float red[] = { 1.0f, 0.0f, 0.0f };
    float blue[] = { 0.0f, 0.0f, 1.0f };
    const Vec2<int32> size(200, 30);
    obj->ClearChilds();
    std::vector<std::string> files;
    GetFilesInDirectory(files, "maps");
    Vec2<float> pctPos(0.25f, 0.8f);
    Vec2<int32> pos(0, 0);
    obj->AddChild(new UIObject(pos, Vec2<float>(0.5f, 0.9f), size, blue, mode == MODE_EDIT ? "EDITOR MENU" :
                                                                               mode == MODE_SINGLEPLAYER ?
                                                                               "SINGLEPLAYER MENU" : "MULTIPLAYER MENU"), false);
    UIObject* child;
    if (auto fn = UIFunctions::GetMemberOneParamFnByName("LoadWorld"))
    {
        for (auto file : files)
        {
            if (mode == MODE_EDIT)
            {
                child = new UIObject(pos, pctPos, size, red, file);
                obj->AddChild(child);
                child->SetOnClickFn(new MemberOneParamFunction(fn, child, 1), MouseType::LEFT_BUTTON);
            }
            else
            {
                child = new UIObject(pos, pctPos, size, red, file);
                obj->AddChild(child);
                child->SetOnClickFn(new MemberOneParamFunction(fn, child, 0), MouseType::LEFT_BUTTON);
            }
            if (pctPos.GetX() == 0.25f)
                pctPos.SetX(0.75f);
            else
            {
                pctPos.SetX(0.25f);
                pctPos.SetY(pctPos.GetY() - 0.1f);
            }
        }
    }
    if (auto fn = UIFunctions::GetOneParamFnByName("MoveToMenu"))
    {
        if (mode == MODE_EDIT)
        {
            child = new UIObject(pos, pctPos, size, red, "Create new World");
            child->SetOnClickFn(new OneParamFunction(fn, 4), MouseType::LEFT_BUTTON);
            obj->AddChild(child);
        }
        child = new UIObject(pos, Vec2<float>(0.5f, 0.1f), size, red, "<== Main Menu");
        child->SetOnClickFn(new OneParamFunction(fn, 0), MouseType::LEFT_BUTTON);
        obj->AddChild(child);
    }
}

void ToggleFlag1s(UIObject* obj, int32 flag, std::string text)
{
    text += Config::Toggle(Config::ConfigFlags(flag)) ? "ON" : "OFF";
    obj->SetText(text);
}

void ShowFlag1s(UIObject* obj, int32 flag, std::string text)
{
    text += Config::IsEnabled(Config::ConfigFlags(flag)) ? "ON" : "OFF";
    obj->SetText(text);
}

void ToggleFlag2s(UIObject* obj, int32 flag, std::string text_on, std::string text_off)
{
    obj->SetText(Config::Toggle(Config::ConfigFlags(flag)) ? text_on : text_off);
}

void ShowFlag2s(UIObject* obj, int32 flag, std::string text_on, std::string text_off)
{
    obj->SetText(Config::IsEnabled(Config::ConfigFlags(flag)) ? text_on : text_off);
}

std::map<std::string, std::function<void()>> NoParamFnMap
{
    { "ContinueGame",   ContinueGame },
    { "SaveWorld",      SaveWorld },
    { "ExitGame",       ExitGame },
    { "EmptyFn",        EmptyFn },
};

std::map<std::string, std::function<void(int32)>> OneParamFnMap
{
    { "MoveToMenu",     MoveToMenu },
};

std::map<std::string, std::function<void(UIObject*)>> MemberNoParamFnMap
{
    { "SetFPS",             SetFPS },
    { "ShowFPSText",        ShowFPSText },
    { "CreateNewWorld",     CreateNewWorld },
    { "DefaultRender",      DefaultRender },
    { "RenderMinimap",      RenderMinimap },
	{ "RenderWorld",        RenderWorld },
	{ "MoveInWorld",        MoveInWorld },
	{ "HandleWorldClick",   HandleWorldClick },
	{ "HandleWorldDrag",    HandleWorldDrag },
    { "HandleSendSelected", HandleSendSelected },
};

std::map<std::string, std::function<void(UIObject*, int32)>> MemberOneParamFnMap
{
    { "HideOnConfigFlags", HideOnConfigFlags },
    { "ShowOnConfigFlags", ShowOnConfigFlags },
    { "LoadMaps",          LoadMaps },
    { "LoadWorld",         LoadWorld },
};

std::map<std::string, std::function<void(UIObject*, int32, std::string)>> MemberTwoParamFnMap
{
    { "ToggleFlag", ToggleFlag1s },
    { "ShowFlag",   ShowFlag1s },
};

std::map<std::string, std::function<void(UIObject*, int32, std::string, std::string)>> MemberThreeParamFnMap
{
    { "ToggleFlag", ToggleFlag2s },
    { "ShowFlag",   ShowFlag2s },
};

namespace UIFunctions
{
    std::function<void()>& GetNoParamFnByName(std::string name)
    {
        auto itr = NoParamFnMap.find(name);
        if (itr != NoParamFnMap.end())
            return itr->second;
        return NoParamFnMap[""];
    }

    std::function<void(int32)>& GetOneParamFnByName(std::string name)
    {
        auto itr = OneParamFnMap.find(name);
        if (itr != OneParamFnMap.end())
            return itr->second;
        return OneParamFnMap[""];
    }

    std::function<void(UIObject*)>& GetMemberNoParamFnByName(std::string name)
    {
        auto itr = MemberNoParamFnMap.find(name);
        if (itr != MemberNoParamFnMap.end())
            return itr->second;
        return MemberNoParamFnMap[""];
    }

    std::function<void(UIObject*, int32)>& GetMemberOneParamFnByName(std::string name)
    {
        auto itr = MemberOneParamFnMap.find(name);
        if (itr != MemberOneParamFnMap.end())
            return itr->second;
        return MemberOneParamFnMap[""];
    }

    std::function<void(UIObject*, int32, std::string)>& GetMemberTwoParamFnByName(std::string name)
    {
        auto itr = MemberTwoParamFnMap.find(name);
        if (itr != MemberTwoParamFnMap.end())
            return itr->second;
        return MemberTwoParamFnMap[""];
    }

    std::function<void(UIObject*, int32, std::string, std::string)>& GetMemberThreeParamFnByName(std::string name)
    {
        auto itr = MemberThreeParamFnMap.find(name);
        if (itr != MemberThreeParamFnMap.end())
            return itr->second;
        return MemberThreeParamFnMap[""];
    }
}

#endif
