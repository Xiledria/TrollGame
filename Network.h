#ifndef __NETWORK_H
#define __NETWORK_H

#include "SharedDefs.h"
#include <string>
#include <unordered_map>
#include <thread>
#include <vector>
#include <mutex>
#ifndef _WIN32
#include <netdb.h>
#endif

class Message;
//@TODO: endianess for mac
//@TODO: check for optimalisations
//#include <SDL_endian.h>
class Network
{
public:
    Network();
    ~Network();


    // initializes socket to accept incomming connections
    void InitializeServer(uint16 port);

    // connects to specific ip address on specified port
    void ConnectToServer(std::string address, std::string port);

    ///lock sender only once per game update loop
    //network thread can be delayed by this, but we cannot afford to delay game loop, which is slower than network
    void LockSender();

    // must be called ONLY between LockSender() and UnlockSender() calls, thread unsafe
    void AddSndMessage(Message* message);

    // unlock sender only once per game update loop
    void UnlockSender();

    // gets vector of not handled messages
    void GetRcvMessages(std::vector<Message*>& messages);

private:
    // blocking function waiting for new connections
    void AcceptNewConnections();

    /// thread safe, vector is locked for every message, added overhead is only for network thread
    // we cannot afford to let game loop sleep waiting for messages too long
    void AddRcvMessage(Message* message);

    // must be called only from network thread
    void GetSndMessages(std::vector<Message*>& messages);

    // sends message immediatedly and deletes message - should be used within Network code
    void DirectSendMessage(Message* message, SOCKET target);

    // broadcasts messageg to all connected clients (ONLY FOR SERVER) and deletes message
    void BroadcastMessage(Message* message);

    void GetMessages(int32 rcvbytes, char* rcvbuffer);
    // used for receiving messages
    void HandleRcvConnections(SOCKET rcvsocket);
    // used for sending messages
    void HandleSndConnections();
    struct ClientInfo
    {
        // @TODO: IP address and nickname
        // int getpeername(int sockfd, struct sockaddr *addr, int *addrlen);  gets ip address
        ClientInfo(SOCKET socket)
        {
            this->socket = socket;
        }

        SOCKET  socket;
        std::string ip;
        std::string nickname;
        std::thread* rcvThread;
    };
    std::unordered_map<SOCKET, ClientInfo> clients;
    // socket used for creating server or connecting to host
    SOCKET host;
    addrinfo* serverInfo;
#ifndef STANDALONE_SERVER
    bool isServer;
#endif
    bool connected;
    enum threads
    {
        RCV_THREAD,
        SND_THREAD,
        CLIENT_ACCEPT_THREAD,
        MAX_THREADS
    };

    std::thread* thread[MAX_THREADS];
    std::vector<Message*> rcvMessages;     // received messages
    std::vector<Message*> sndMessages;     // messages in queue ready for being sent
    //@TODO: maybe use local deque variable to which we are going to swap our buffer instead of this? :D
    std::vector<Message*> usedRcvMessages; // received messages that are currently being handled
    std::mutex rcvMutex;
    std::mutex sndMutex;
};

extern Network* network;

#endif
