#include "SharedDefs.h"
#include <random>
std::default_random_engine generator;

MoveFlags operator|(MoveFlags a, MoveFlags b)
{
    return static_cast<MoveFlags>(static_cast<uint8>(a) | static_cast<uint8>(b));
}

int32 irand(int32 start, int32 end)
{
    std::uniform_int_distribution<int32> distribution(start, end);
    return distribution(generator);
}

float frand(float start, float end)
{
    std::uniform_real_distribution<float> distribution(start, end);
    return distribution(generator);
}

#ifndef DEBUG_MEMORY
#ifdef USE_JEMALLOC
void* operator new(std::size_t sz)
{
    return je_malloc(sz);
}
void operator delete(void* ptr) noexcept
{
    je_free(ptr);
}
#endif
#endif
//@TODO: pick random element from container fn
