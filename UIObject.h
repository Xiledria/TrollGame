#ifndef STANDALONE_SERVER
#ifndef __UI_OBJECT_H
#define __UI_OBJECT_H

#include "SharedDefs.h"
#include "Vec2.h"
#include <string>
#include <vector>
#include "FunctionPtr.h"
#include <functional>
//@TODO: _names, initialisere list
class UIObject
{
public:
    UIObject(Vec2<int32> pos, Vec2<float> pctPos, Vec2<int32> size, float color3f[3], std::string newText, Vec2<float> pctSize = { 0.0f, 0.0f });
    virtual ~UIObject();
    void ClearChilds();
    void SetOnDrawFn(FunctionPtr* fn);
    void SetOnDragFn(FunctionPtr* fn, MouseType button);
    void SetOnClickFn(FunctionPtr* fn, MouseType button);
    void SetOnVisibilityCheckFn(FunctionPtr* fn);
    void SetText(std::string& text);
    void SetVisible(bool visible);
    bool IsVisible();
    void OnWindowResized();
    UIObject* GetTopObjectAtPos(Vec2<int32>& pos);
    bool IsInside(Vec2<int32>& pos);
    void HandleDrag(Vec2<int32>& pos, MouseType button);
    void HandleClick(Vec2<int32>& pos, MouseType button);
    bool operator<(UIObject& obj);
    Vec2<int32>& GetStart();
    Vec2<int32>& GetSize();
    Vec2<int32>& GetCenter();
    Vec2<int32> GetClickPos();
    Vec2<int32> GetRelativeClickPos();
    virtual float* GetColor3f();
    std::string GetText();
    std::vector<UIObject*>& GetChilds(bool clickable = true);
    UIObject* GetParent();
    void AddChild(UIObject* child, bool clickable = true);
    void OnPosChanged(); // called from parent to all childs
    void SetClickStarted(bool started);
    virtual void OnDrag(Vec2<int32>& pos, MouseType button);
    virtual void OnClick(Vec2<int32>& pos, MouseType button);
    virtual void OnVisibilityCheck();
    virtual void Draw();
protected:
    void Init(Vec2<int32> pos, Vec2<float> pctPos, Vec2<int32> size, Vec2<float> pctSize, float color3f[3], std::string newText);
    Vec2<int32> start;          // for controlls - it is faster to recalculate on screen resize than on every frame
    Vec2<int32> end;            // for controlls - it is faster to recalculate on screen resize than on every frame
    Vec2<int32> pos;
    Vec2<float> pctPos;
    Vec2<int32> center;         // for graphics - it is faster to recalculate on screen resize than on every frame
    Vec2<int32> currentSize;    // for graphics - it is faster to recalculate on screen resize than on every frame
    Vec2<int32> size;      // constant size of object
    Vec2<float> pctSize;   // dynamic size depending on screen resolution
    Vec2<int32> clickPos;
    MouseType clickType;
    bool clickStarted;
    std::string text;
    float m_color3f[3];
    // active elements
    std::vector<UIObject*> childs;
    // decoration elements only
    std::vector<UIObject*> nonClickableChilds;
    UIObject* parent;
    bool visible;
    FunctionPtr* onDrawFn;
    FunctionPtr* onDragFn[TOTAL_BUTTONS];
    FunctionPtr* onClickFn[TOTAL_BUTTONS];
    FunctionPtr* onVisibilityCheckFn;
private:
    void SetParent(UIObject* parent);
};

class EditBox : public UIObject
{
public:
    EditBox(Vec2<int32> pos, Vec2<float> pctPos, Vec2<int32> size, float color3f[], float activeColor3f[], std::string newText, uint8 editMode, Vec2<float> pctSize = { 0.0f, 0.0f });
    ~EditBox();
    void SetOnChangeFunction(FunctionPtr* function);
    void OnClick(Vec2<int32>& pos, MouseType button) override;
    void OnChange();
    float* GetColor3f() override;
    void SetActive(bool act);
    void SetActiveColor(float color3f[3]);
    enum Flags
    {
        ACCEPT_LETTERS = 0x01,
        ACCEPT_NUMBERS = 0x02,
        ACCEPT_SYMBOLS = 0x04, // @TODO: now allows only underscore
        ACCEPT_ALL = 0x07,
    };
    uint8 GetEditMode();
private:
    void Init(float activeColor3f[], uint8 editMode);
    bool active;
    float activeColor3f[3];
    uint8 editMode;
    FunctionPtr* onChangeFn;
};

#endif
#endif
