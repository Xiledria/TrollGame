#ifndef __UNIT_H
#define __UNIT_H

#include "SharedDefs.h"
#include "Object.h"

class Pathfinder;
class ObjectStore;

class Unit : public Object
{
    friend class ObjectStore;
public:
    Unit(uint32 entry);
    ~Unit();
    // called from World::GeneratePaths()
    void UpdatePathfinder(float diff);
    // called from Game::Update() - this is thread safe code able to parallelize
    void UpdateParallel(float diff) override;
    // called from Game::Update()
    void Update(float diff) override;
    // returns current health
    float GetHealth();
    // sets current health (capped at maxhealth)
    void SetHealth(float health);
    // returns maximal health
    float GetMaxHealth();
    // returns percentage of actual health
    float GetHealthPct();
    // returns ID of player owner
    uint8 GetOwnerId();
    // Set ID of player owner
    void SetOwnerId(uint8 id);
    // Sends unit to specified coordinates
    void MoveTo(int32 x, int32 y);
    // Set this Unit to follow another Unit
    // NEVER EVER pass pos = nullptr from anywhere but Unit class
    void MoveTo(Position* pos);
    // copies coordinates from specified position and moves there
    void MoveTo(Position& pos);
    // return position where Unit is heading
    bool GetMoveDest(Position& pos);
    // returns true if unit is moving, false otherwise
    bool IsMoving() override;
    // returns true if unit is alive, false otherwise
    bool IsAlive();
    // return movement types
    MoveFlags GetMoveFlags();
    // deals damage to this unit
    void HandleDamageTaken(int32 damage);

private:
    // updates actual unit position if is moving, called only internally from ::Update()
    void UpdateMovement(float diff);
    void UpdateAttack(float diff);
    void AttackTarget(Unit* target);
protected:
    Unit();
    // setable flags from file
    uint16 _minimalAttack;
    uint16 _maximalAttack;
    uint16 _missileSpeed;
    uint32 _missileModelId;
    float _attackSpeed;
    uint16 _attackRange;
    uint16 _searchRadius;
    float _attackTimer;
    float _healthRegen;
    float _maxHealth;
    float _health;
    uint16 _armor;
    uint16 _moveSpeed;
    MoveFlags _moveFlags;
    // end of setable flags from file
    Pathfinder* _pathfinder;
    bool _moving;
    uint8 _ownerId; // ID of player owning this unit
    bool _alive;
    Vec2<int32> _movementOffset;
    Unit* _target;
};

#endif
