#ifndef STANDALONE_SERVER
#ifndef __FONTS_H
#define __FONTS_H

#include "SharedDefs.h"
#include <GL/glew.h>
#include FREETYPE_INCLUDE
#include FT_FREETYPE_H
class Fonts
{
public:
    Fonts();
    GraphicsInitializeErrors Initialize(int32 height);
    ~Fonts();
    void RenderText(const char* text, int32 posx, int32 posy, bool centered);
private:
    FT_Library ft;
    FT_Face face;
    // width for text texture
#define MAXWIDTH 1024
    GLuint textTexture;
    ft_int w;    // width of texture in pixels
    ft_int h;    // height of texture in pixels
    struct
    {
        float ax;    // advance.x
        float ay;    // advance.y

        float bw;    // bitmap.width;
        float bh;    // bitmap.height;

        float bl;    // bitmap_left;
        float bt;    // bitmap_top;

        float tx;    // x offset of glyph in texture coordinates
        float ty;    // y offset of glyph in texture coordinates
    } c[128];        // character information

    struct point
    {
        GLfloat x;
        GLfloat y;
        GLfloat s;
        GLfloat t;
    };
};

#endif
#endif
