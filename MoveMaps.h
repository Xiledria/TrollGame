#ifndef __MOVE_MAPS_H
#define __MOVE_MAPS_H

#include "SharedDefs.h"
#include <vector>

class Terrain;
class Position;
class Object;

class MoveMaps
{
public:
    MoveMaps(MoveFlags flags, Object* owner);
    ~MoveMaps();
    virtual bool CheckTile(unsigned xitr, unsigned yitr) const;
    bool operator()(unsigned x, unsigned y) const;
protected:
    std::vector<Terrain*>& _terrain;
    std::vector<Object*>& _objects;
    uint32 _maxX;
    uint32 _maxY;
    MoveFlags _moveFlags;
    Object* _owner;
    uint32 _sizeplus;
    uint32 _sizeminus;
};

class MoveMapsFollow : public MoveMaps
{
public:
    MoveMapsFollow(MoveFlags flags, Object* owner, Position* target);
    bool CheckTile(unsigned xitr, unsigned yitr) const override;
protected:
    Position* _target;
};

class MoveMapsIgnoringMobs : public MoveMaps
{
public:
    MoveMapsIgnoringMobs(MoveFlags flags, Object* owner);
    bool CheckTile(unsigned xitr, unsigned yitr) const override;
};

#endif
