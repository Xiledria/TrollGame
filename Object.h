#ifndef __OBJECT_H
#define __OBJECT_H
#include "Position.h"

class Model;

enum ObjectType
{
    TYPEID_OBJECT,
    TYPEID_UNIT,
};

class Object : public Position
{
public:
    Object(uint32 modelId);
    virtual ~Object();
    virtual void UpdateParallel(float diff) = 0;
    virtual void Update(float diff) = 0;
    const Model* GetModel();
    const ObjectType GetObjectType();
    const float* GetColor4f();
    const int32 GetSize();
    const uint32 GetId();
    
    virtual bool IsMoving();
    
    // coords
    int32 GetDistance(int32 x, int32 y) override;
    // positions
    int32 GetDistance(Position* target) override;
    int32 GetDistance(Position* target, int32& distX, int32& distY) override;
    // objects
    int32 GetDistance(Object* target, bool size) override;
    int32 GetDistance(Object* target, int32& distX, int32& distY, bool size) override;

    // @TODO: think about better solution - this is used in world as position in vector to remove it correctly
    // also make GetContainerPos() and move to protected, lol
    uint32 _containerPos;
protected:
    Object();
    ObjectType _type;
    uint32 _objectId;
    uint32 _entry;
    const Model* _model;
};

#endif

