#ifndef __MESSAGE_H
#define __MESSAGE_H

#include "SharedDefs.h"
#include <string>
#include <cstring>

enum class MessageType : int16
{

};

class Message
{
public:
    static uint8 GetBufferByteSize();
public:
    Message(char* data, uint16 rcvbytes);
    /*SIZE NEEDS TO BE SET >= OF REAL SIZE, OTHERWISE IT MIGHT CAUSE PROBLEMS*/
    Message(MessageType id, uint16 size, SOCKET target);
    ~Message();

    char* ToPacket(uint16& length);
    uint16 size(bool whole = false);
    char* GetString(uint16 len = 0);
    template<typename T>
    void operator>>(T& value)
    {
        if (_size < _begin+sizeof(T))
            return;
        memcpy(&value,_buffer+_begin,sizeof(T));
        _begin += sizeof(T);
    }
    //char array is handled differently than
    //other params that are pushed into buffer
    void Append(const char* value);
    void Append(const char* value, uint16 len);
    void operator<<(std::string value);
    void operator<<(const char* value);
    void operator<<(char* value);
    template<typename T>
    void operator<<(T value)
    {
        memcpy(_buffer+_end,&value,sizeof(T));
        _end += sizeof(T);
    }
    SOCKET GetTarget();
    MessageType GetId();
protected:
    SOCKET _target;
    MessageType _id;
    uint16 _size;
    uint16 _begin;
    uint16 _end;
    char* _buffer;
};

#endif
