#include "Searcher.h"
#include "Unit.h"
#include "Object.h"

bool Searcher::operator()(Object* obj)
{
    return owner->GetDistance(obj, true) <= distance;
}

bool UnitSearcher::operator()(Object* obj)
{
    
    return obj->GetObjectType() == TYPEID_UNIT && owner->GetDistance(obj, true) <= distance;
}

bool HostileUnitSearcher::operator()(Object* obj)
{
    bool checkFaction = obj->GetObjectType() == TYPEID_UNIT && owner->GetDistance(obj, true) <= distance;
    if (!checkFaction)
        return false;
    return static_cast<Unit*>(obj)->GetOwnerId() != static_cast<Unit*>(owner)->GetOwnerId();
}
