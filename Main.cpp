#include "SharedDefs.h"
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <string>
#include <thread>
#ifdef _MSC_VER
#ifndef _DEBUG
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup") // hide console on Windows if not debugging
#else
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#endif
#include "Initialiser.h"
#include "Game.h"
/* Our program's entry point */
int main(int /*argc*/, char * /*argv*/[])
{
#ifndef _WIN32
    kmp_set_defaults("KMP_BLOCKTIME=0");
#endif
#if defined(_MSC_VER) && defined(DEBUG_MEMORY)
    DEBUG(_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF));
    DEBUG(_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG));
#endif
    if (!Initialiser::Init())
        return EXIT_FAILURE;
    int ret = game->Run();
    Initialiser::Exit();
    return ret;
}
