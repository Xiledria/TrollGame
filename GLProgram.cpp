#ifndef STANDALONE_SERVER

#include "GLProgram.h"

GLProgram::GLProgram()
{

}

GLProgram::~GLProgram()
{
    glUseProgram(program);
    glDeleteBuffers(1, &VBO);
    glDetachShader(program, vs);
    glDetachShader(program, fs);
    glDeleteShader(vs);
    glDeleteShader(fs);
    glDeleteProgram(program);
}

void GLProgram::Activate()
{
    glUseProgram(program);
    glBindVertexArray(VAO);
}

uint32 GLProgram::GetId()
{
    return program;
}

void GLProgram::UpdateScreenResolution(Vec2<uint32> newRes)
{
    glProgramUniform2f(program, SHADER_UNIFORM_SCREEN_SIZE, static_cast<float>(newRes.GetX() / 2), static_cast<float>(newRes.GetY() / 2));
}

void GLProgram::UpdateCameraPosition(int32 x, int32 y)
{
    glProgramUniform2i(program, SHADER_UNIFORM_CAMERA, x, y);
}

GLuint GLProgram::LoadShader(const char * source, GLuint type)
{
    GLuint id = glCreateShader(type);
    glShaderSource(id, 1, &source, nullptr);
    glCompileShader(id);
    DEBUG
    (
        char error[1000];
        glGetShaderInfoLog(id, 1000, nullptr, error);
        std::cout << "Compile shader status: " << std::endl << error << std::endl;
    );
    return id;
}

void GLProgram::PrepareBuffer(GLuint& buffer, GLsizeiptr size, const void* data, bool dynamic /* = false */)
{
    //Create buffer objects
    glGenBuffers(1, &buffer);
    //Populate the color buffer
    DEBUG(std::cout << "generated buffer " << buffer << std::endl);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, size, data, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
}

// shared code with all shaders
// should be called after initialisation of shader program source and positionData
void GLProgram::PrepareShader(bool dynamic /* = false */)
{
    vs = LoadShader(vsSource, GL_VERTEX_SHADER);
    fs = LoadShader(fsSource, GL_FRAGMENT_SHADER);
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glUseProgram(program);

    PrepareBuffer(VBO, positionData.size() * sizeof(GLfloat), &positionData[0], dynamic);

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    //Enable vertex attribute arrays
    glEnableVertexAttribArray(SHADER_ATTRIB_POSITION); //vertexPosition
                                                       //map index 0 to position buffer (location = 0) in vec2


    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(SHADER_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
}

SquareGLProgram::SquareGLProgram(const float* color4f, int32 scale)
{
    positionData =
    {
        -1.0f,  -1.0f,
         1.0f,  -1.0f,
         1.0f,   1.0f,
        -1.0f,   1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 camera;
layout (location = 2) uniform ivec2 frame;
layout (location = 3) uniform ivec2 position;
layout (location = 4) uniform int scale;
layout (location = 5) uniform vec4 incolor;
out vec4 color;
void main()
{
    color = incolor;
    vec2 pos = (camera + frame + VertexPosition * scale + position) / screen - 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430

in vec4 color;
out vec4 FragColor;
void main()
{
    FragColor = color;
})";

    PrepareShader();
    glUniform1i(SHADER_UNIFORM_SCALE, scale);
    glUniform4fv(SHADER_UNIFORM_COLOR, 1, color4f);
}

TeamColorGLProgram::TeamColorGLProgram(int32 scale)
{
    positionData =
    {
       -1.0f, -1.0f,
        1.0f,  1.0f,
       -1.0f,  1.0f,
        1.0f, -1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 camera;
layout (location = 2) uniform ivec2 frame;
layout (location = 3) uniform ivec2 position;
layout (location = 4) uniform int scale;
layout (location = 5) uniform vec3 incolor;
out vec3 color;
void main()
{
    color = incolor;
    vec2 pos = (camera + frame + VertexPosition * scale + position) / screen - 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430

in vec3 color;
out vec4 FragColor;
void main()
{
    FragColor = vec4(color, 1.0f);
})";

    PrepareShader();
    glUniform1i(SHADER_UNIFORM_SCALE, scale);
}


RectangleGLProgram::RectangleGLProgram(const float* color4f, int32 scaleX, int32 scaleY)
{
    positionData =
    {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 camera;
layout (location = 2) uniform ivec2 frame;
layout (location = 3) uniform ivec2 position;
layout (location = 4) uniform ivec2 scale;
layout (location = 5) uniform vec4 incolor;
out vec4 color;
void main()
{
    color = incolor;
    vec2 pos = (camera + frame + VertexPosition * scale + position) / screen - 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430

in vec4 color;
out vec4 FragColor;
void main()
{
    FragColor = color;
})";

    PrepareShader();
    glUniform2i(SHADER_UNIFORM_SCALE, scaleX, scaleY);
    glUniform4fv(SHADER_UNIFORM_COLOR, 1, color4f);
}


StaticRectangleGLProgram::StaticRectangleGLProgram()
{
    positionData =
    {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 position;
layout (location = 2) uniform ivec2 scale;
layout (location = 3) uniform vec3 incolor;
out vec4 color;
void main()
{
    color = vec4(incolor, 1.0f);
    vec2 pos = (VertexPosition * scale + position) / screen - 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430

in vec4 color;
out vec4 FragColor;
void main()
{
    FragColor = color;
})";

    PrepareShader();
}


TileGLProgram::TileGLProgram(uint32 gridSize)
{
    positionData =
    {
        -1.0f,  -1.0f,
        1.0f,  -1.0f,
        1.0f,   1.0f,
        -1.0f,   1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 camera;
layout (location = 2) uniform ivec2 frame;
layout (location = 3) uniform ivec2 position;
layout (location = 4) uniform int scale;
layout (location = 5) uniform vec3 incolor;
out vec3 color;
void main()
{
    color = incolor;
    vec2 pos = (camera + frame + VertexPosition * scale + (position + vec2(1.0f, 1.0f)) * scale) / screen - 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430

in vec3 color;
out vec4 FragColor;
void main()
{
    FragColor = vec4(color, 1.0f);
})";
    PrepareShader();
    glUniform1i(SHADER_UNIFORM_SCALE, gridSize);
}

HealthBarGLProgram::~HealthBarGLProgram()
{
    glDeleteBuffers(1, &colorVBO);
}

HealthBarGLProgram::HealthBarGLProgram(int32 scale)
{
    positionData =
    {
       -1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
    };

    colorData =
    {
        0.0f, 1.0f,
        0.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 0.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 1) in vec2 incolor;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 camera;
layout (location = 2) uniform ivec2 frame;
layout (location = 3) uniform ivec2 position;
layout (location = 4) uniform int scale;
flat out vec2 color;
void main()
{
    color = incolor;
    vec2 pos = (camera + frame + VertexPosition * scale + position) / screen - 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430

flat in vec2 color;
out vec4 FragColor;
void main()
{
    FragColor = vec4(color, 0.0f, 1.0f);
})";
    PrepareShader(true);
    PrepareBuffer(colorVBO, colorData.size() * sizeof(GLfloat), &colorData[0]);
    //Enable vertex attribute arrays
    glEnableVertexAttribArray(SHADER_ATTRIB_COLOR); //vertexPosition
                                                    //map index 0 to position buffer (location = 0) in vec2
    glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
    glVertexAttribPointer(SHADER_ATTRIB_COLOR, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    glUniform1i(SHADER_UNIFORM_SCALE, scale);
}

void HealthBarGLProgram::UpdateHealthPct(float pct)
{
    positionData[2] = 2.0f * pct - 1.0f;
    positionData[4] = 2.0f * pct - 1.0f;
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2, sizeof(GLfloat) * 3, &positionData[2]);
    //glBufferData(GL_ARRAY_BUFFER, positionData.size(), &positionData[0], GL_STATIC_DRAW);
}

MinimapGLProgram::MinimapGLProgram(GLuint texture, uint32 minimapSize)
{
    positionData =
    {
        .0f,  .0f,
        1.0f,  .0f,
        1.0f,   1.0f,
        .0f,   1.0f
    };

    texCoords =
    {
        0.0f,  0.0f,
        1.0f,  0.0f,
        1.0f,  1.0f,
        0.0f,  1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 1) in vec2 vertTexCoord;
layout (location = 2) out vec2 fragTexCoord;
layout (location = 0) uniform vec2 screen;
layout (location = 2) uniform int minimapSize;

void main()
{
    // Pass the tex coord straight through to the fragment shader
    fragTexCoord = vertTexCoord;

    vec2 pos = VertexPosition * minimapSize;
    pos.x += 2 * screen.x - minimapSize;
    pos /= screen;
    pos -= 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430
layout (location = 1) uniform sampler2D tex;
layout (location = 2) in vec2 fragTexCoord;
out vec4 finalColor;

void main()
{
    finalColor = texture2D(tex, fragTexCoord);
})";
    PrepareShader();
    glUniform1i(SHADER_UNIFORM_SCALE, minimapSize);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(MinimapGLProgram::SHADER_UNIFORM_TEXTURE, 0);

    //Enable vertex attribute arrays
    glEnableVertexAttribArray(SHADER_ATTRIB_TEXTURE_POSITION);

    PrepareBuffer(texVBO, texCoords.size() * sizeof(GLfloat), &texCoords[0]);

    glBindBuffer(GL_ARRAY_BUFFER, texVBO);
    glVertexAttribPointer(SHADER_ATTRIB_TEXTURE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
}

MinimapGLProgram::~MinimapGLProgram()
{
    glDeleteBuffers(1, &texVBO);
}

MinimapObjectGLProgram::MinimapObjectGLProgram(int32 scale, uint32 minimapSize, uint32 worldSizeX, uint32 worldSizeY)
{
    positionData =
    {
        -1.0f,  -1.0f,
        1.0f,  -1.0f,
        1.0f,   1.0f,
        -1.0f,   1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 position;
layout (location = 2) uniform int scale;
layout (location = 3) uniform int minimapSize;
layout (location = 4) uniform ivec2 world;
layout (location = 5) uniform vec3 incolor;
out vec3 color;
void main()
{
    color = incolor;
    vec2 mult = vec2(minimapSize, minimapSize) / vec2(world);
    vec2 offset = mult * position;
    vec2 pos = VertexPosition * mult * float(scale) + offset;
    pos.x += 2 * screen.x - minimapSize;
    pos /= screen;
    pos -= 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430

in vec3 color;
out vec4 FragColor;
void main()
{
    FragColor = vec4(color, 1.0f);
})";

    PrepareShader();
    glUniform1i(SHADER_UNIFORM_SCALE, scale);
    glUniform1i(SHADER_UNIFORM_MINIMAP_SIZE, minimapSize);
    glUniform2i(SHADER_UNIFORM_WORLD_SIZE, worldSizeX, worldSizeY);
}

MinimapSelectionGLProgram::MinimapSelectionGLProgram(uint32 minimapSize, uint32 uiHeight, uint32 worldSizeX, uint32 worldSizeY)
{
    positionData =
    {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    vsSource = R"(
#version 430
layout (location = 0) in vec2 VertexPosition;
layout (location = 0) uniform vec2 screen;
layout (location = 1) uniform ivec2 position;
layout (location = 2) uniform float minimapSize;
layout (location = 3) uniform ivec2 world;
layout (location = 4) uniform float uiHeight;
void main()
{
    vec2 mult = vec2(minimapSize, minimapSize) / vec2(world);
    vec2 _position = position;
    vec2 _screen = 2 * screen;
    _screen.y -= uiHeight;
    _position.y += uiHeight;
    vec2 offset = mult * _position;
    vec2 pos = VertexPosition * mult * _screen + offset;
    pos.x += 2 * screen.x - minimapSize;
    pos /= screen;
    pos -= 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
})";
    fsSource = R"(
#version 430
out vec4 FragColor;
void main()
{
    FragColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);
})";

    PrepareShader();
    glUniform1f(SHADER_UNIFORM_MINIMAP_SIZE, static_cast<GLfloat>(minimapSize));
    glUniform2i(SHADER_UNIFORM_WORLD_SIZE, worldSizeX, worldSizeY);
    glUniform1f(SHADER_UNIFORM_UI_HEIGHT, static_cast<GLfloat>(uiHeight));
}

FontsGLProgram::FontsGLProgram()
{
    vsSource = R"(
#version 430
layout (location = 0) in vec4 coord;
layout (location = 1) out vec2 texpos;
layout (location = 0) uniform vec2 screen;
layout (location = 2) uniform ivec2 position;
void main()
{
    vec2 pos = (position + coord.xy) / screen - 1.0f;
    gl_Position = vec4(pos, 0.0f, 1.0f);
    texpos = coord.zw;
})";

    fsSource = R"(
#version 430
layout (location = 1) uniform sampler2D tex;
layout (location = 1) in vec2 texpos;
out vec4 finalColor;
void main()
{
    finalColor = vec4(1.0f, 1.0f, 0.0f, texture2D(tex, texpos).r);
})";

    positionData =
    {
        1,
    };

    PrepareShader(true);
}

FontsGLProgram::~FontsGLProgram()
{
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
    glDeleteTextures(1, &textTexture);
}

GraphicsInitializeErrors FontsGLProgram::Initialize(int32 height)
{
    FT_Error fterror = FT_Init_FreeType(&ft);
    if (fterror)
        return GraphicsInitializeErrors::FAILED_LOAD_FONT_LIB;
    else
    {
        fterror = FT_New_Face(ft, "font.ttf", 0, &face);
        if (fterror)
            return  GraphicsInitializeErrors::FAILED_LOAD_FONT;
        else // success
            FT_Set_Pixel_Sizes(face, 0, height);
    }

    FT_GlyphSlot g = face->glyph;

    ft_int roww = 0;
    ft_int rowh = 0;
    w = 0;
    h = 0;

    memset(c, 0, sizeof c);

    // Find minimum size for a texture holding all visible ASCII characters
    for (int i = 32; i < 128; i++)
    {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER))
            continue;
        if (roww + g->bitmap.width + 1 >= MAXWIDTH)
        {
            w = std::max(w, roww);
            h += rowh;
            roww = 0;
            rowh = 0;
        }
        roww += g->bitmap.width + 1;
        rowh = std::max(rowh, g->bitmap.rows);
    }

    w = std::max(w, roww);
    h += rowh;

    // Create a texture that will be used to hold all ASCII glyphs
    glActiveTexture(GL_TEXTURE0 + TEXTURE_UNIT);
    glGenTextures(1, &textTexture);
    glBindTexture(GL_TEXTURE_2D, textTexture);
    glUniform1i(SHADER_UNIFORM_TEXTURE, TEXTURE_UNIT);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, 0);

    // We require 1 byte alignment when uploading texture data
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Clamping to edges is important to prevent artifacts when scaling
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Linear filtering usually looks best for text
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Paste all glyph bitmaps into the texture, remembering the offset
    int ox = 0;
    int oy = 0;

    rowh = 0;

    for (int i = 32; i < 128; i++)
    {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER))
            continue;

        if (ox + g->bitmap.width + 1 >= MAXWIDTH)
        {
            oy += rowh;
            rowh = 0;
            ox = 0;
        }

        glTexSubImage2D(GL_TEXTURE_2D, 0, ox, oy, g->bitmap.width, g->bitmap.rows, GL_RED, GL_UNSIGNED_BYTE, g->bitmap.buffer);
        c[i].ax = static_cast<float>(g->advance.x >> 6);
        c[i].ay = static_cast<float>(g->advance.y >> 6);

        c[i].bw = static_cast<float>(g->bitmap.width);
        c[i].bh = static_cast<float>(g->bitmap.rows);

        c[i].bl = static_cast<float>(g->bitmap_left);
        c[i].bt = static_cast<float>(g->bitmap_top);

        c[i].tx = ox / (float)w;
        c[i].ty = oy / (float)h;

        rowh = std::max(rowh, g->bitmap.rows);
        ox += g->bitmap.width + 1;
    }

    return GraphicsInitializeErrors::NONE;
}

void FontsGLProgram::RenderText(const char* text, int32 posx, int32 posy, bool centered)
{
    float x, y;
    if (centered)
    {
        x = 0.0f;
        y = 0.0f;
    }
    else
    {
        x = static_cast<float>(posx);
        y = static_cast<float>(posy);
    }

    Activate();
    const uint8* p;

    // Use the texture containing the atlas
    glBindTexture(GL_TEXTURE_2D, textTexture);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(SHADER_ATTRIB_POSITION, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    size_t strLen = strlen(text);
    point* coords = new point[6 * strLen];
    int ch = 0;

    // Loop through all characters
    for (p = (const uint8_t *)text; *p; p++)
    {
        /* Calculate the vertex and texture coordinates */
        float x2 = x + this->c[*p].bl;
        float y2 = -y - this->c[*p].bt;
        float gw = this->c[*p].bw;
        float gh = this->c[*p].bh;

        /* Advance the cursor to the start of the next character */
        x += this->c[*p].ax;
        y += this->c[*p].ay;

        /* Skip glyphs that have no pixels */
        if (!gw || !gh)
        {
            --strLen;
            continue;
        }
        coords[ch++] =
        {
            x2, -y2 - gh, c[*p].tx, c[*p].ty + c[*p].bh / h
        };
        coords[ch++] =
        {
            x2 + gw, -y2, c[*p].tx + c[*p].bw / w, c[*p].ty
        };
        coords[ch++] =
        {
            x2, -y2, c[*p].tx, c[*p].ty
        };
        coords[ch++] =
        {
            x2 + gw, -y2, c[*p].tx + c[*p].bw / w, c[*p].ty
        };
        coords[ch++] =
        {
            x2, -y2 - gh, c[*p].tx, c[*p].ty + c[*p].bh / h
        };
        coords[ch++] =
        {
            x2 + gw, -y2 - gh, c[*p].tx + c[*p].bw / w, c[*p].ty + c[*p].bh / h
        };
    }
    if (centered)
    {
        posx -= static_cast<int32>(x / 2);
        posy -= h / 4 - 2;
        glUniform2i(SHADER_UNIFORM_POSITION, posx, posy);
    }
    else
        glUniform2i(SHADER_UNIFORM_POSITION, 0, 0);
    // Draw all the character on the screen in one go
    glBufferData(GL_ARRAY_BUFFER, sizeof(point) * 6 * strLen, coords, GL_DYNAMIC_DRAW);
    glDrawArrays(GL_TRIANGLES, 0, ch);
    delete[] coords;
}

#endif
