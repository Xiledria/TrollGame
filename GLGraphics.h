#ifndef STANDALONE_SERVER
#ifndef __GL_GRAPHICS_H
#define __GL_GRAPHICS_H

#include "SharedDefs.h"
#include "Graphics.h"
#include <GL/glew.h>
#include SDL_INCLUDE
#include <vector>

#define GLERROR() DEBUG({GLenum err = glGetError(); if (err != GL_NO_ERROR) std::cout << "GL ERROR: " << err << " " << __LINE__ << std::endl;})
class Object;
class Unit;
class GLProgram;
class FontsGLProgram;
class UIObject;

class GLGraphics : public Graphics
{
public:
    GLGraphics();
    ~GLGraphics() override;
    void Run() override;
    void UpdateCamera() override;
    void Resize() override;
    void HandleWorldResize() override;
    void RecreateMinimap() override;
    GraphicsInitializeErrors InitializeFonts() override;
    void UpdateVSync() override;
    void UpdateFullscreen() override;
    void DrawUIObject(UIObject* object) override;
    void DrawMinimap(UIObject* object) override;
private:
    void InitializeShaders();
    GLuint LoadShader(const char* source, GLuint type /*vertex / fragment / geometry / rasterisation*/);
    void DrawCircle(int32 posx, int32 posy, int32 radius, const float* color4f) override;
    void DrawDisc(int32 posx, int32 posy, int32 radius, const float* color4f) override;
    void DrawTile(uint32 x, uint32 y, const float* color3f) override;
    // does not call glProgramUse, requires shader program already used
    void DrawSquarePrepared(int32 x, int32 y, int32 radius, const float* color4f);
    void DrawSquare(int32 x, int32 y, int32 radius, const float* color4f) override;
    // does not call glProgramUse, requires shader program already used
    void DrawObject(Object* obj);
    // does not call glProgramUse, requires shader program already used
    void DrawTeamColorPrepared(Unit* obj);
    void DrawTeamColor(Unit* obj, Vec2<int32>& offset) override;
    // does not call glProgramUse, requires shader program already used
    void DrawTilePrepared(uint32 x, uint32 y, const float* color3f);
    void DrawVisibleObjects(UIObject* object) override;
    void DrawUnitHealthPrepared(int32 x, int32 y);
    void DrawUnitHealth(int32 x, int32 y, int32 radius, float pctHp) override;
    // does not call glProgramUse, requires shader program already used
    void DrawUnitSelectionPrepared(int32 x, int32 y, int32 radius);
    void DrawUnitSelection(int32 x, int32 y, int32 radius) override;

    void DrawActualSelecting() override;
    // does not call glProgramUse, requires shader program already used
    void DrawMinimapObjectPrepared(Object* obj);
    void DrawUI() override;
    void DrawWorld(uint32 startX, uint32 endX, uint32 startY, uint32 endY, UIObject* object) override;
    void DrawEditor() override;
    void SDLDie(char* msg);


    SDL_Window* window;
    SDL_GLContext context;
    GLuint mapTexture;
    enum ProgramId
    {
        PROGRAM_TILE,
        PROGRAM_UNIT_SELECTION, // @TODO: use different selections for different unit sizes, maybe fixed sizes ? small - medium - large? :D
        PROGRAM_SQUARE,
        PROGRAM_STATIC_RECTANGLE,
        PROGRAM_UNIT_SELECTOR, // when owner is selecting units in world - area for selected units
        PROGRAM_TEAM_COLOR,     // @TODO: use different sizes for different unit sizes, maybe fixed sizes ? small - medium - large? :D
        PROGRAM_UNIT_HEALTH_BAR,
        PROGRAM_MINIMAP,
        PROGRAM_MINIMAP_OBJECT,
        PROGRAM_MINIMAP_SELECTION,

        PROGRAM_UNIT_MODELS_OFFSET,
        PROGRAM_UNIT_ENTRY_1 = PROGRAM_UNIT_MODELS_OFFSET,
        PROGRAM_MISSILE_MODEL,
    };
    FontsGLProgram* fonts;
    std::vector<GLProgram*> programs;
};

#endif
#endif
