#ifndef STANDALONE_SERVER
#include "Fonts.h"

Fonts::Fonts()
{
    
}


GraphicsInitializeErrors Fonts::Initialize(int32 height)
{
    FT_Error fterror = FT_Init_FreeType(&ft);
    if (fterror)
        return GraphicsInitializeErrors::FAILED_LOAD_FONT_LIB;
    else
    {
        fterror = FT_New_Face(ft, "font.ttf", 0, &face);
        if (fterror)
            return  GraphicsInitializeErrors::FAILED_LOAD_FONT;
        else // success
            FT_Set_Pixel_Sizes(face, 0, height);
    }

    FT_GlyphSlot g = face->glyph;

    ft_int roww = 0;
    ft_int rowh = 0;
    w = 0;
    h = 0;

    memset(c, 0, sizeof c);

    // Find minimum size for a texture holding all visible ASCII characters
    for (int i = 32; i < 128; i++)
    {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER))
            continue;
        if (roww + g->bitmap.width + 1 >= MAXWIDTH)
        {
            w = std::max(w, roww);
            h += rowh;
            roww = 0;
            rowh = 0;
        }
        roww += g->bitmap.width + 1;
        rowh = std::max(rowh, g->bitmap.rows);
    }

    w = std::max(w, roww);
    h += rowh;

    // Create a texture that will be used to hold all ASCII glyphs
    glGenTextures(1, &textTexture);
    glBindTexture(GL_TEXTURE_2D, textTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, w, h, 0, GL_ALPHA, GL_UNSIGNED_BYTE, 0);

    // We require 1 byte alignment when uploading texture data
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Clamping to edges is important to prevent artifacts when scaling
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Linear filtering usually looks best for text
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Paste all glyph bitmaps into the texture, remembering the offset
    int ox = 0;
    int oy = 0;

    rowh = 0;

    for (int i = 32; i < 128; i++)
    {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER))
            continue;

        if (ox + g->bitmap.width + 1 >= MAXWIDTH)
        {
            oy += rowh;
            rowh = 0;
            ox = 0;
        }

        glTexSubImage2D(GL_TEXTURE_2D, 0, ox, oy, g->bitmap.width, g->bitmap.rows, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);
        c[i].ax = static_cast<float>(g->advance.x >> 6);
        c[i].ay = static_cast<float>(g->advance.y >> 6);

        c[i].bw = static_cast<float>(g->bitmap.width);
        c[i].bh = static_cast<float>(g->bitmap.rows);

        c[i].bl = static_cast<float>(g->bitmap_left);
        c[i].bt = static_cast<float>(g->bitmap_top);

        c[i].tx = ox / (float)w;
        c[i].ty = oy / (float)h;

        rowh = std::max(rowh, g->bitmap.rows);
        ox += g->bitmap.width + 1;
    }

    return GraphicsInitializeErrors::NONE;
}


Fonts::~Fonts()
{
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
    glDeleteTextures(1, &textTexture);
}

void Fonts::RenderText(const char* text, int32 posx, int32 posy, bool centered)
{
    float x, y;
    if (centered)
    {
        x = 0.0f;
        y = 0.0f;
    }
    else
    {
        x = static_cast<float>(posx);
        y = static_cast<float>(posy);
    }
    const uint8* p;
    size_t strLen = strlen(text);
    point* coords = new point[6 * strLen];
    int ch = 0;

    // Loop through all characters
    for (p = (const uint8_t *)text; *p; p++)
    {
        /* Calculate the vertex and texture coordinates */
        float x2 = x + this->c[*p].bl;
        float y2 = -y - this->c[*p].bt;
        float gw = this->c[*p].bw;
        float gh = this->c[*p].bh;

        /* Advance the cursor to the start of the next character */
        x += this->c[*p].ax;
        y += this->c[*p].ay;

        /* Skip glyphs that have no pixels */
        if (!gw || !gh)
        {
            --strLen;
            continue;
        }

        coords[ch++] =
        {
            x2, -y2 - gh, c[*p].tx, c[*p].ty + c[*p].bh / h
        };
        coords[ch++] =
        {
            x2 + gw, -y2, c[*p].tx + c[*p].bw / w, c[*p].ty
        };
        coords[ch++] =
        {
            x2, -y2, c[*p].tx, c[*p].ty
        };
        coords[ch++] =
        {
            x2 + gw, -y2, c[*p].tx + c[*p].bw / w, c[*p].ty
        };
        coords[ch++] =
        {
            x2, -y2 - gh, c[*p].tx, c[*p].ty + c[*p].bh / h
        };
        coords[ch++] =
        {
            x2 + gw, -y2 - gh, c[*p].tx + c[*p].bw / w, c[*p].ty + c[*p].bh / h
        };
    }
    if (centered)
    {
        posx -= static_cast<int32>(x / 2);
        posy -= h / 4 - 2;
    }
    else
    {
        posx = 0;
        posy = 0;
    }
    // Use the texture containing the atlas
    glBindTexture(GL_TEXTURE_2D, textTexture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glEnable(GL_TEXTURE_2D);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(0, 0, 0);
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f, 1.0f, 0.0f);
    for (uint32 i = 0; i < 6 * strLen; ++i)
    {
        glTexCoord2f(coords[i].s, coords[i].t);
        glVertex2f(coords[i].x + posx, coords[i].y + posy);
    }
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    delete[] coords;
}

#endif
