#ifndef __PATHFINDER_H
#define __PATHFINDER_H

#include "SharedDefs.h"
#include <mutex>
#include <vector>

class Position;
class Unit;

class Pathfinder
{
public:
    Pathfinder(Unit* owner);
    ~Pathfinder();
    void SetMoveDest(Position* dest);
    void SetMoveDest(int32 x, int32 y);
    bool GetMoveDest(Position& pos);
    void GeneratePath();
    void OnWaypointReached();
    void Update(float diff);
    bool GetNextPath(Position& pos);
    void SetCanMove(bool canMove);
private:
    void Reset();

    Unit* _owner;
    Position* _moveDest;
    std::vector<Position*> _waypoints;
    float _updateTimer;
    float _cannotMoveTimer;
    bool _canMove;
    bool _isReferenceDest;
    std::mutex _destLock;
    std::mutex _wpLock;
};

#endif
