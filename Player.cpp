#include "Player.h"
#include "World.h"
#include "Unit.h"
#include "Terrain.h"

//@TODO: initialiser list
Player::Player()
{
    _builder = new Terrain();
    _id = world->GeneratePlayerId();
    _color3f[0] = 0.0f;
    _color3f[1] = 1.0f;
    _color3f[2] = 0.0f;
}

Player::~Player()
{
    // _selected.clear();
    delete _builder;
}

void Player::AddSelected(Unit* target)
{
    _selected.push_back(target);
}

void Player::ClearSelected()
{
    _selected.clear();
}

std::list<Unit*>& Player::GetSelected()
{
    return _selected;
}

void Player::RemoveDeadSelected()
{
    std::list<Unit*>::iterator itr = _selected.begin();
    while (itr != _selected.end())
    {
        if (!(*itr)->IsAlive())
            _selected.erase(itr++);
        else
            ++itr;
    }
}

void Player::UpdateCheat(char* c)
{
    _cheat += c;
}

void Player::UpdateBuilder(TerrainType type)
{
    _builder->SetType(type);
}

Terrain* Player::GetBuilder()
{
    return _builder;
}

uint8 Player::GetId()
{
    return _id;
}

const float* Player::GetColor3f()
{
    return _color3f;
}
