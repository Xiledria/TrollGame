#ifndef STANDALONE_SERVER
#ifndef __UI_FUNCTIONS_H
#define __UI_FUNCTIONS_H

#include <functional>
#include "SharedDefs.h"

class UIObject;

namespace UIFunctions
{
    std::function<void()>& GetNoParamFnByName(std::string name);
    std::function<void(int32)>& GetOneParamFnByName(std::string name);
    std::function<void(UIObject*)>& GetMemberNoParamFnByName(std::string name);
    std::function<void(UIObject*, int32)>& GetMemberOneParamFnByName(std::string name);
    std::function<void(UIObject*, int32, std::string)>& GetMemberTwoParamFnByName(std::string name);
    std::function<void(UIObject*, int32, std::string, std::string)>& GetMemberThreeParamFnByName(std::string name);
}

#endif
#endif
